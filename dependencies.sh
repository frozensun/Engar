#!/bin/bash

sudo apt-get install libglu1-mesa-dev libgtk2.0-dev libhdf5-dev cmake cmake-qt-gui cmake-extras libopenblas-dev libtbb-dev libfreetype6-dev libopenexr-dev libjpeg-dev libpng-dev libtiff-dev libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libxvidcore-dev libx264-dev libgtk-3-dev