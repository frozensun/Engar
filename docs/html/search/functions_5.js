var searchData=
[
  ['faceengine',['FaceEngine',['../classengar_1_1_face_engine.html#a7079dd8f17fd005d5ddf2fdf4fb2e5e4',1,'engar::FaceEngine']]],
  ['fastdetector',['FastDetector',['../classengar_1_1_fast_detector.html#addb54312ecb3f40287921574570453a0',1,'engar::FastDetector']]],
  ['ferinferrer',['FERInferrer',['../classengar_1_1_f_e_r_inferrer.html#a5ddd7c80f23b863f0ce45ddacd133c81',1,'engar::FERInferrer']]],
  ['find',['find',['../classengar_1_1_homography_helper.html#a6b2162343998aa901ff33b3edfdb3d9b',1,'engar::HomographyHelper::find(vector&lt; cv::Point2f &gt; &amp;initialCorners, vector&lt; cv::KeyPoint &gt; &amp;keypoints_object, cv::Ptr&lt; KeypointMatches &gt; keypointMatches, vector&lt; cv::Point2f &gt; &amp;, vector&lt; cv::KeyPoint &gt; &amp;)'],['../classengar_1_1_homography_helper.html#a2ef15afb791dce39c965f775d9fa91c7',1,'engar::HomographyHelper::find(const vector&lt; cv::Point2f &gt; &amp;objectPoints, const vector&lt; cv::Point2f &gt; &amp;scenePoints, const vector&lt; cv::Point2f &gt; &amp;initialCorners, vector&lt; cv::Point2f &gt; &amp;estimatedCorners, vector&lt; cv::Point2f &gt; &amp;inliers, cv::Mat &amp;matrix)'],['../struct_range_1_1basic__range.html#aed588ee507e154d6692479ca465970bf',1,'Range::basic_range::find()']]],
  ['findfaces',['findFaces',['../classengar_1_1_face_engine.html#a7eb9b86726094b3da0d5574694f19c00',1,'engar::FaceEngine']]],
  ['flannbasedmatcherhack',['FlannBasedMatcherHack',['../classengar_1_1_flann_based_matcher_hack.html#a29c236012f997a3ecf20a1c5c8b6e9bb',1,'engar::FlannBasedMatcherHack']]],
  ['flannmatcher',['FLANNMatcher',['../classengar_1_1_f_l_a_n_n_matcher.html#a67ad48d9e5bc8425c6b1f61620288509',1,'engar::FLANNMatcher']]]
];
