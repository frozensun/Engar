var searchData=
[
  ['recognitionattemptframe',['RecognitionAttemptFrame',['../classengar_1_1_tracked_face.html#a923b1bc60ad7788ee9d6f04d3ea77d4b',1,'engar::TrackedFace']]],
  ['recognizeperiod',['recognizePeriod',['../classengar_1_1_face_engine.html#afba91da85a4c10839e4d04ee726933e3',1,'engar::FaceEngine']]],
  ['recognizer',['recognizer',['../classengar_1_1_face_engine.html#a3a151b9a58e6221a749ffea05c3b282c',1,'engar::FaceEngine']]],
  ['red',['Red',['../classengar_1_1_basic_colors.html#a93465d83268d34abaca8c673362cf0af',1,'engar::BasicColors::Red()'],['../namespaceengar.html#a9d5696573bd38e9893ef50f5acfd6c09',1,'engar::red()']]],
  ['resultlocations',['resultLocations',['../classengar_1_1_caffe_net_abstract_inferrer.html#ae41cba80a8588470addbf056fda55ec6',1,'engar::CaffeNetAbstractInferrer']]],
  ['results',['results',['../namespaceengar.html#a9da8830aea8c7b1e48b6b6fbee6873a4',1,'engar']]],
  ['rotations',['rotations',['../classengar_1_1_l_k_tracker.html#a7646129a79acf454b5d7b70d9a36f9ea',1,'engar::LKTracker']]],
  ['rvec',['rvec',['../namespaceengar.html#a50ec4900aa9871dae8aecfe195a6a32f',1,'engar']]]
];
