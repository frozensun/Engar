var searchData=
[
  ['magenta',['Magenta',['../classengar_1_1_basic_colors.html#a8d13eb3e2dd9f9cee14308d71b08c834',1,'engar::BasicColors']]],
  ['matcher',['matcher',['../classengar_1_1_ar_detector.html#a979f525388bb2e012c3ee71ad893f36b',1,'engar::ArDetector::matcher()'],['../classengar_1_1_dlib_face_recognizer.html#a88119556a47228e8680eeb0e4bb91d5c',1,'engar::DlibFaceRecognizer::matcher()'],['../class_tracker.html#aebea861655e7079419584d88cabe8780',1,'Tracker::matcher()']]],
  ['matches',['matches',['../classengar_1_1_face_recognizer.html#acfca47e19bc577263878a2a9a6fd1ba4',1,'engar::FaceRecognizer']]],
  ['matchtime',['matchTime',['../classengar_1_1_ar_detector.html#ac7a76b1dfb2e9f55ea0b993c567b47d1',1,'engar::ArDetector::matchTime()'],['../classengar_1_1_face_recognizer.html#af0188c9ce1d05c42feab1f84dcc24021',1,'engar::FaceRecognizer::matchTime()']]],
  ['matrix',['matrix',['../classengar_1_1_homography_helper.html#a97fe9a07884b399ef3d774dbcec8678e',1,'engar::HomographyHelper']]],
  ['maxframetime',['maxFrameTime',['../classengar_1_1_image_based_engine.html#ac3606bd3abd5e355b9626181240d77fa',1,'engar::ImageBasedEngine']]],
  ['maxtrackingtime',['maxTrackingTime',['../classengar_1_1_image_based_engine.html#af11d24a3963173e21eecb431f0d5a63e',1,'engar::ImageBasedEngine']]],
  ['maxx',['MaxX',['../classengar_1_1_ar_detection_result.html#aeac8b3106c6db2a6733aa0336afef8c2',1,'engar::ArDetectionResult']]],
  ['maxy',['MaxY',['../classengar_1_1_ar_detection_result.html#a0a891572f553ad4242b36bcf5e5dcb7b',1,'engar::ArDetectionResult']]],
  ['meancolor',['meanColor',['../classengar_1_1_caffe_net_abstract_inferrer.html#a54cd5b35b32502cddc5f480707599f9a',1,'engar::CaffeNetAbstractInferrer']]],
  ['mintrackingtime',['minTrackingTime',['../classengar_1_1_image_based_engine.html#a95fb724a45093a1c677b18872585b42b',1,'engar::ImageBasedEngine']]],
  ['minx',['MinX',['../classengar_1_1_ar_detection_result.html#a930d4b5d9c9178cca453e3180a520786',1,'engar::ArDetectionResult']]],
  ['miny',['MinY',['../classengar_1_1_ar_detection_result.html#a932874ffc7db83c0c022bf79b56178f9',1,'engar::ArDetectionResult']]]
];
