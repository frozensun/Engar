var searchData=
[
  ['ngtmatcher',['NGTMatcher',['../classengar_1_1_n_g_t_matcher.html#a35f7c4fc97a03d10a3485df23b616758',1,'engar::NGTMatcher::NGTMatcher(int dimension, bool buildTree=true, float searchEpsilon=0.1, float searchRadius=FLT_MAX)'],['../classengar_1_1_n_g_t_matcher.html#adae36f5094e8f38ca2df617901e87570',1,'engar::NGTMatcher::NGTMatcher(NGT::Property &amp;property, float searchEpsilon=0.1, float searchRadius=FLT_MAX)']]],
  ['nmsmatcher',['NMSMatcher',['../classengar_1_1_n_m_s_matcher.html#ae681852ef57709d7907dd6018993e44c',1,'engar::NMSMatcher']]]
];
