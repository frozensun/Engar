var searchData=
[
  ['scenekeypoints',['SceneKeypoints',['../class_keypoint_matches.html#ac366193bfa15b8317f888ebd8a1ad0e8',1,'KeypointMatches']]],
  ['searchepsilon',['searchEpsilon',['../classengar_1_1_f_l_a_n_n_matcher.html#a5846a841ffa291474ecdd5e866f3aabb',1,'engar::FLANNMatcher::searchEpsilon()'],['../classengar_1_1_n_g_t_matcher.html#a1516b32fb39f6ef4f40a290c122458ca',1,'engar::NGTMatcher::searchEpsilon()']]],
  ['searchparams',['searchParams',['../classengar_1_1_fast_detector.html#a8c0bc3b3d6c43e20d5456aab92c4afcb',1,'engar::FastDetector::searchParams()'],['../classengar_1_1_f_l_a_n_n_matcher.html#a619d03a50da1bbdc6bae3cad1ba2e3d1',1,'engar::FLANNMatcher::searchParams()'],['../classengar_1_1_k_graph_matcher.html#ae20fdc0d49df8b275bd134c8793e0cd7',1,'engar::KGraphMatcher::SearchParams()']]],
  ['searchprecision',['SearchPrecision',['../classengar_1_1_annoy_matcher.html#a9803a74dfa2dd8c84f782f51deb5a2d0',1,'engar::AnnoyMatcher']]],
  ['searchradius',['searchRadius',['../classengar_1_1_f_l_a_n_n_matcher.html#acc7458618c8cdfd5259e45d7320e7a39',1,'engar::FLANNMatcher::searchRadius()'],['../classengar_1_1_n_g_t_matcher.html#ad9e865e04a1a2aac26480b49d072abe5',1,'engar::NGTMatcher::searchRadius()']]],
  ['sp',['sp',['../classengar_1_1_dlib_face_recognizer.html#acb7bcbd948d1a1c5d04b7fc56ce4dd6e',1,'engar::DlibFaceRecognizer::sp()'],['../classengar_1_1_face_engine.html#a898c2c5c391da21023321c05182ffbd6',1,'engar::FaceEngine::sp()']]],
  ['space',['space',['../classengar_1_1_n_m_s_matcher.html#aaaacb6e70f5f438a1f3bf349d0581c58',1,'engar::NMSMatcher']]],
  ['state',['state',['../classengar_1_1_image_based_engine.html#a806e2118a3f81f8fb8561d37511394d6',1,'engar::ImageBasedEngine']]],
  ['stillfound',['stillFound',['../classengar_1_1_image_based_engine.html#a95cf0820ba1664a69fd6c202242c34cf',1,'engar::ImageBasedEngine']]],
  ['stop',['stop',['../classengar_1_1_integrated_a_r_engine.html#a9d66671de6cf9dbcbd7154472b7e0855',1,'engar::IntegratedAREngine']]]
];
