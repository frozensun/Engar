var searchData=
[
  ['range',['range',['../namespace_range.html#a0c36f0ac4e20111ec1e199b567340c70',1,'Range']]],
  ['reference',['reference',['../struct_range_1_1basic__range_1_1const__iterator__impl.html#a25fd8173c047a76ce3de7fb0b8153ffa',1,'Range::basic_range::const_iterator_impl::reference()'],['../struct_range_1_1basic__range_1_1const__reverse__iterator__impl.html#a15b1282f6c65feece0e76937062f2867',1,'Range::basic_range::const_reverse_iterator_impl::reference()'],['../struct_range_1_1basic__range.html#ab3527b0629ed5c4e336e489e4546dc64',1,'Range::basic_range::reference()']]],
  ['residual',['residual',['../classengar_1_1_dlib_face_recognizer.html#a275a803adf587ceba16f4c0a2e8260d5',1,'engar::DlibFaceRecognizer']]],
  ['residual_5fdown',['residual_down',['../classengar_1_1_dlib_face_recognizer.html#a670d7287d56638f7c11f7cafb8c98775',1,'engar::DlibFaceRecognizer']]],
  ['reverse_5fiterator',['reverse_iterator',['../struct_range_1_1basic__range.html#af64952c4cd9fb6007b50e3656b90038b',1,'Range::basic_range']]]
];
