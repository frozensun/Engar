var searchData=
[
  ['basic_5frange',['basic_range',['../struct_range_1_1basic__range.html',1,'Range::basic_range&lt; IntegerType &gt;'],['../struct_range_1_1basic__range.html#ab24e138f35e2962609f603355e59b5c0',1,'Range::basic_range::basic_range()'],['../struct_range_1_1basic__range.html#a871b092956d91534429e9e1b37429a97',1,'Range::basic_range::basic_range(value_type first_element, value_type last_element, value_type step)'],['../struct_range_1_1basic__range.html#a9e8c0ce95ada2c224974ed6ef91b8e1e',1,'Range::basic_range::basic_range(value_type first_element, value_type last_element)'],['../struct_range_1_1basic__range.html#a8788d69c9aae80d80aab338dee720325',1,'Range::basic_range::basic_range(value_type last_element)'],['../struct_range_1_1basic__range.html#aecb6a4e5396e126200d464ecefbfc82d',1,'Range::basic_range::basic_range(const basic_range&lt; IntegerType &gt; &amp;r)']]],
  ['basiccolors',['BasicColors',['../classengar_1_1_basic_colors.html',1,'engar::BasicColors'],['../classengar_1_1_basic_colors.html#ac8a2eb645b9454f4e7e87e505504dd41',1,'engar::BasicColors::BasicColors()']]],
  ['bbox',['Bbox',['../classengar_1_1_tracked_face.html#a6ab059d01746fa93f4280c22b2d89434',1,'engar::TrackedFace']]],
  ['begin',['begin',['../struct_range_1_1basic__range.html#a6c137466337af4dd98ab552ae8fbef38',1,'Range::basic_range']]],
  ['binboostdetector',['BinBoostDetector',['../classengar_1_1_bin_boost_detector.html',1,'engar::BinBoostDetector'],['../classengar_1_1_bin_boost_detector.html#a994b91877412a2dde06ad706d27dc229',1,'engar::BinBoostDetector::BinBoostDetector()']]],
  ['bithamming',['BitHamming',['../structkgraph_1_1metric_1_1hamming.html#adc4f0fa9d5c064e28db067e4e4ad0e74',1,'kgraph::metric::hamming']]],
  ['black',['Black',['../classengar_1_1_basic_colors.html#a63f0e6c75357f318e7fc69bfa2f38b91',1,'engar::BasicColors']]],
  ['block',['block',['../classengar_1_1_dlib_face_recognizer.html#a71dd7c12b266fca996d3727b13761343',1,'engar::DlibFaceRecognizer']]],
  ['blue',['Blue',['../classengar_1_1_basic_colors.html#acb639a1178b0fd200b82f6d2d43e9160',1,'engar::BasicColors::Blue()'],['../namespaceengar.html#a7dbeaf847d05db7283da869f84cfd66c',1,'engar::blue()']]]
];
