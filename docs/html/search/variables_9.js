var searchData=
[
  ['k',['K',['../classengar_1_1_image_based_engine.html#af2b633113731a482cb1d9eb12229bd38',1,'engar::ImageBasedEngine::K()'],['../classengar_1_1_integrated_a_r_engine.html#a912d7fedd71f4fb86dedcff694f7ab87',1,'engar::IntegratedAREngine::K()']]],
  ['knnsearchtime',['knnSearchTime',['../classengar_1_1_annoy_matcher.html#a3d3ebee51261c3e0fa6d13220590fd60',1,'engar::AnnoyMatcher::knnSearchTime()'],['../classengar_1_1_f_l_a_n_n_matcher.html#afde7f4d001f1e40804f448ea287b6d8b',1,'engar::FLANNMatcher::knnSearchTime()'],['../classengar_1_1_k_graph_matcher.html#aff481eb7c8b506a92a20754c84c45d04',1,'engar::KGraphMatcher::knnSearchTime()'],['../classengar_1_1_m_r_p_t_matcher.html#ac5c87828d804eb0899ea6f8ce51a68d3',1,'engar::MRPTMatcher::knnSearchTime()'],['../classengar_1_1_n_g_t_matcher.html#af5afb427783abdf848b282c757475def',1,'engar::NGTMatcher::knnSearchTime()'],['../classengar_1_1_n_m_s_matcher.html#ac74d6fec0a42536a7891f2e8ad2022ac',1,'engar::NMSMatcher::knnSearchTime()']]],
  ['kpmatches',['KPMatches',['../classengar_1_1_ar_detection_result.html#ae81027e6efee3385ce00ef9e221cc255',1,'engar::ArDetectionResult']]]
];
