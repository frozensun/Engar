var searchData=
[
  ['name',['Name',['../classengar_1_1_identity.html#ac2119a91706f8b2114b930b34cc75246',1,'engar::Identity::Name()'],['../classengar_1_1_inferrer.html#afa8627c0b6f2c67005e3c08dae61a649',1,'engar::Inferrer::Name()']]],
  ['needstrain',['needsTrain',['../classengar_1_1_annoy_matcher.html#a2d9baa5992f5e51aadb8984c5a868de0',1,'engar::AnnoyMatcher::needsTrain()'],['../classengar_1_1_f_l_a_n_n_matcher.html#a73fe46bd4553dd7b526240ddcf935e60',1,'engar::FLANNMatcher::needsTrain()'],['../classengar_1_1_k_graph_matcher.html#aa97f6aa6be6226b51e98f0f1da84e187',1,'engar::KGraphMatcher::needsTrain()'],['../classengar_1_1_m_r_p_t_matcher.html#af738564118dab250c6522bd582cf7334',1,'engar::MRPTMatcher::needsTrain()'],['../classengar_1_1_n_g_t_matcher.html#a2f1ad4f5e2befe8e86feb09de01db56a',1,'engar::NGTMatcher::needsTrain()'],['../classengar_1_1_n_m_s_matcher.html#a3cb9982d507934912bd8c5afbb72cd5f',1,'engar::NMSMatcher::needsTrain()']]],
  ['net',['net',['../classengar_1_1_dlib_face_recognizer.html#a8655b37a0174174cff36c909c8d5a502',1,'engar::DlibFaceRecognizer::net()'],['../classengar_1_1_caffe_net_abstract_inferrer.html#aca5cebc7e3fd12366a3cdfa80061f455',1,'engar::CaffeNetAbstractInferrer::net()']]],
  ['nfeatures',['nfeatures',['../classengar_1_1_ar_detector.html#a4547f71bc68e83d9992b79cfa64f9118',1,'engar::ArDetector']]],
  ['ngtsearchepsilon',['ngtSearchEpsilon',['../classengar_1_1_fast_detector.html#a372bd9aa8badc4617ef5f6bfc589f7fa',1,'engar::FastDetector::ngtSearchEpsilon()'],['../classengar_1_1_orb_detector.html#a5a0e8bc304cb5a1d13f2445370e95580',1,'engar::OrbDetector::ngtSearchEpsilon()']]],
  ['normals',['normals',['../classengar_1_1_l_k_tracker.html#ac03aa13f1ae60dfef516d91ea1eed6ee',1,'engar::LKTracker']]]
];
