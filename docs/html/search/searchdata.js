var indexSectionsWithContent =
{
  0: "abcdefghijklmnopqrstuvwy~",
  1: "abcdfghiklmnoprstuv",
  2: "ekr",
  3: "abcdefghijklmnoprstu~",
  4: "abcdefghiklmnopqrstvwy",
  5: "abcdiprsv",
  6: "s",
  7: "t",
  8: "o"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "related"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends"
};

