//
// Created by tigershark on 21/9/19.
//


#include <iostream>
#include <string>
#include <ctime>
#include <thread>
#include <mutex>
#include <future>
#include <condition_variable>
#include "TrackedFace.h"
#include <opencv2/core/types.hpp>
#include <ostream>
#include <memory>
#include <opencv2/core.hpp>
#include <opencv2/tracking.hpp>


namespace engar {

    using namespace std;

    TrackedFace::TrackedFace(cv::Rect2d &bbox) {
        Bbox = bbox;
    }


    TrackedFace::~TrackedFace() {
//            cout << "Destroying tracked face." << (long) this << endl;
        stop = true;
        lock.notify_all();
        try {
            if (t->joinable()) t->join();
        }
        catch (exception &e) {
            cout << e.what() << endl;
        }
    }

    void TrackedFace::Init() {
//            tracker = TrackerKCF::create(TrackerKCF::Params());
//            tracker = TrackerMIL::create(TrackerMIL::Params()); // ~35ms
//        tracker = cv::TrackerMOSSE::create();                   // ~3ms
            tracker = cv::TrackerKCF::create();                       // ~20ms  correct lost
//            tracker = TrackerCSRT::create();                       // ~38ms  doesn't lose
//            tracker = TrackerGOTURN::create();                       // ~48ms

        t = std::make_unique<std::thread>([this]() {
            std::mutex mtx;
            std::unique_lock<std::mutex> lk(mtx);
            lock.wait(lk);
            bool faceLost = true;
            while (!stop) {
                try {
                    faceLost = !tracker->update(workFrame, Bbox);

                } catch (exception &e) {
                    cout << "Exception in thread" << t->get_id() << endl << e.what() << endl << endl;
                }

                resultPromise->set_value(faceLost);
                lock.wait(lk);
//                    cout << "Thread iteration " << t->get_id() << endl;
            }
//                cout << "Thread ends " << t->get_id() << endl;
            return 0;
        });
    }

    void TrackedFace::ResetTracker(cv::InputArray workFrame, cv::Rect2d faceBBox) {
        tracker->init(workFrame, faceBBox);
    }

    void TrackedFace::ResetTracker(cv::Mat workFrame, cv::Rect2d faceBBox) {
        tracker->init(workFrame, faceBBox);
    }

    std::future<bool> TrackedFace::Track(cv::Mat &f) {
        workFrame = f;
        resultPromise = std::make_unique<std::promise<bool >>();
        lock.notify_one();
        return resultPromise->get_future();
    }

    void TrackedFace::Reset() {
        Active = false;
        FoundFrame = 0;
        RecognitionAttemptFrame = 0;
        CurrentIdentity = Identity::Unknown();
        InferredInfo.clear();
    }

}

