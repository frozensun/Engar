//
// Created by TigerShark on 9/10/2017.
//

#include "Utils.h"

#include <memory>
#include "BinBoostDetector.h"
#include "binboost/BoostDesc.h"

cv::Ptr<engar::ArDetectionResult>
engar::BinBoostDetector::detect(cv::InputArray &frame, const cv::OutputArray &descriptors_object) {

    Ptr <KeypointMatches> keysAndMatch = new KeypointMatches();
    Ptr <vector<KeyPoint>> keypoints_scene = new vector<KeyPoint>;
    Mat descriptors_scene;

//-- Detect the keypoints using ORB Detector and Calculate descriptors (feature vectors)
    UMat cop;
    frame.copyTo(cop);
    detector->detect(cop, *keypoints_scene);
    extractor->compute(cop, *keypoints_scene, descriptors_scene);
    cout << "BinBoostDetector.detect: detectAndCompute ends." << endl << std::flush;

//-- Matching descriptor vectors using FLANN matcher
    vector<vector<DMatch> > matches;

    matcher->knnMatch(descriptors_object, descriptors_scene, matches, 2);
//	matcher2->knnMatch(descriptors_scene, matches, 2);
    cout << "Engine.ProcessFrame: matching ends." << endl << std::flush;

    matchCount++;

//    double max_dist = 0;
//    double min_dist = 100;
//
//    //-- Quick calculation of max and min distances between keypoints
//    Utils::calculateMaxAndMinDistance(matches, min_dist, max_dist);

//-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
    Ptr <vector<DMatch>> good_matches = new vector<DMatch>;

// Apply ratio test
    const float ratio = 0.9f;    // As in Lowe's paper
    for (auto &match : matches) {
        if (!match.empty()
            && match[0].distance < 50
            && match[0].distance < ratio * match[1].distance) {
//			int a = matches[i][0].trainIdx;
//			matches[i][0].trainIdx = matches[i][0].queryIdx;
//			matches[i][0].queryIdx = a;
            good_matches->push_back(match[0]);
        }
    }

    keysAndMatch->SceneKeypoints = keypoints_scene;
    keysAndMatch->GoodMatches = good_matches;

    cv::Ptr<engar::ArDetectionResult> result(new ArDetectionResult());

    result->KPMatches = keysAndMatch;

    return result;
}

engar::BinBoostDetector::BinBoostDetector() {
    int nrIter = 15;
//    this->detector = FastFeatureDetector::create(20, true);// new DynamicAdaptedFeatureDetector(new SurfAdjuster(), 900, 1100, nrIter);

    this->detector = ORB::create(nfeatures, scaleFactor, nlevels, edgeThreshold, firstLevel, WTA_K, ORB::HARRIS_SCORE,
                                 patchSize);// new DynamicAdaptedFeatureDetector(new SurfAdjuster(), 900, 1100, nrIter);

    // extract descriptors
    this->extractor = std::make_unique<boostDesc::BinBoost>("models/binboost/matrices/binboost.bin");

//        extractor = Ptr<DescriptorExtractor>(new BinBoost("models/binboost/matrices/binboost_128.bin"));

//        extractor = Ptr<DescriptorExtractor>(new BinBoost("models/binboost/matrices/binboost_256.bin"));


    this->matcher = new BFMatcher(NORM_HAMMING, false);
}

Ptr <engar::ArDetectionResult> engar::BinBoostDetector::detect(cv::InputArray &frame) {
    throw runtime_error("Unsupported operation.");
}

void engar::BinBoostDetector::computeKeyPoints(Mat &target, vector<KeyPoint> &keypoints_object, UMat &descriptors_object) {

    if (target.channels() > 1) {
        cvtColor(target, tempFrame, COLOR_BGR2GRAY);
    } else {
        target.copyTo(tempFrame);
    }


    auto t1 = std::chrono::high_resolution_clock::now();

    // Calculamos los keypoints del target.
    detector->detect(tempFrame, keypoints_object);

    auto t2 = std::chrono::high_resolution_clock::now();
    cout << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() / 1000 << endl;

    t1 = std::chrono::high_resolution_clock::now();
    Mat temp = descriptors_object.getMat(ACCESS_RW);
    extractor->compute(tempFrame, keypoints_object, temp);
    t2 = std::chrono::high_resolution_clock::now();
    cout << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() / 1000 << endl;

    tempFrame.release();

//    vector<vector<DMatch> > matches;
//    matcher->knnMatch(perObjectDescriptors, perObjectDescriptors, matches, 2);
//    //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
//    Ptr<vector<DMatch> > good_matches = new vector<DMatch>;
//    this->filteredKps.clear();
//    // Apply ratio test
//    const float ratio = 0.8f;    // As in Lowe's paper
//    for (auto &match : matches) {
//        if (!match.empty() && match[1].distance > 10) {
//            filteredKps.push_back((*perObjectKeypoints)[match[0].queryIdx]);
//        }
//    }
//    perObjectKeypoints = &filteredKps;
    ///////////////////////////////////////////////////////////////////////////

//    Scalar green = Scalar(0, 255, 0);
//    vector<Point2f> points;
//    KeyPoint::convert(perObjectKeypoints, points);
//    Mat temp;
//    cvtColor(target, temp, COLOR_GRAY2RGB);
//    for (auto &point : points) {
//        circle(temp, point, 3, green, -1, 8);
//    }

    cout << "Detector initialized, found " << keypoints_object.size() << " keypoints." << endl;
}
