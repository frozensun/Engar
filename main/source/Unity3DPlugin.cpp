//
// Created by TigerShark on 23-Aug-17.
//
#if _MSC_VER // this is defined when compiling with Visual Studio
#define EXPORT_API UNITY_INTERFACE_EXPORT // Visual Studio needs annotating exported functions with this
#else
#define EXPORT_API // XCode does not need annotating exported functions, so define is empty
#endif

//#include <plugins/GLEW/glew.h>
#include <iostream>

#include "Unity/IUnityInterface.h"
#include "Unity/IUnityGraphics.h"
#include <opencv2/imgproc.hpp>
#include "LKTracker.h"
//#include <main/HomographyHelper.h>
#include "ArDetectionResult.h"
#include "OrbDetector.h"
#include "ArbitraryImageEngine.h"
#include <csignal>
#include <fstream>
#include "Unity3DFrameProvider.h"

using namespace cv;

namespace engar {
    typedef void(UNITY_INTERFACE_API *DebugCallback)(const char *str);

    DebugCallback gDebugCallback;

    extern "C" void UNITY_INTERFACE_EXPORT RegisterDebugCallback(DebugCallback callback) {
        if (callback) {
            gDebugCallback = callback;
        }
    }

    void DebugInUnity(std::string message) {
        if (gDebugCallback) {
            gDebugCallback(message.c_str());
        }
    }

// Example low level rendering Unity plugins

// --------------------------------------------------------------------------
// SetTimeFromUnity, an example function we export which is called by one of the scripts.

    static float g_Time;

    extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API SetTimeFromUnity(float t) { g_Time = t; }



// --------------------------------------------------------------------------
// SetTextureFromUnity, an example function we export which is called by one of the scripts.

    static void *loopbackTextureHandle = nullptr;
    static int camTextureWidth = 0;
    static int camTextureHeight = 0;

    static void *camTextureHandle = nullptr;
//    static Mat grayFrame;

    float trackingPointErrorThreshold = 15;

    static Mat colorFrame, objectMask;
//static vector<PointAndIndex> newPoints;

    static Mat homography;


    const double f = 621.53903021141821, w = 640, h = 480;
    Mat rvec, tvec, inliers;

    Scalar red = Scalar(0, 0, 255);
    Scalar green = Scalar(0, 255, 0);
    Scalar blue = Scalar(255, 0, 0);
    vector<Point3f> axis{
            Point3f(64, 0, 0),
            Point3f(0, 64, 0),
            Point3f(0, 0, 64)
    };
    double results[12];

    struct TargetData {
        void *textureHandle;
        int w;
        int h;
        Mat img;
    };

    vector<TargetData> targetsData;

    unique_ptr<ImageBasedEngine> engine;
    Unity3DFrameProvider *frameProvider;
    ofstream logFile;

    void SignalHandler(int signal) {
        printf("Signal %d", signal);
        exit(1);
        throw "!Access Violation!";
    }


    extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API InitForImages() {
        signal(SIGSEGV, SignalHandler);
        frameProvider = new engar::Unity3DFrameProvider();
        LKTracker *tracker = new LKTracker(trackingPointErrorThreshold);

        std::shared_ptr<engar::ArDetector> detector = std::make_shared<engar::OrbDetector>(200);
//        main::ArDetector *detector = new AkazeDetector(img_object);

        engine = std::make_unique<engar::ArbitraryImageEngine>(frameProvider, detector, tracker, 640, 480); //FIXME no hardcodear tamanio de los targets
        logFile.open("log.txt");

        std::cerr.rdbuf(logFile.rdbuf());
        std::cout.rdbuf(logFile.rdbuf());

    }

//    extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API ProcessImageTargets() {
//        for (const TargetData &targetData: targetsData) {
//            imshow("AR ImageBasedEngine Demo", targetData.img);
//            engine->AddTarget(targetData.img);
//        }
//    }

// FIXME uncomment
//    extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API AddImageTarget(void *textureHandle, int w, int h) {
//
////        targetsData.push_back(TargetData{textureHandle, w, h});
//
//        logFile << "AddImageTarget" << endl;
//        try {
//            UMat img_object = UMat(h, w, CV_8UC3);
//            unsigned char *dst = img_object.data;
//            unsigned char *ptrFrame = (unsigned char *) textureHandle;
//            for (size_t i = 0; i < h * w; ++i, ptrFrame += 3, dst += 3) {
//                *dst = *(ptrFrame + 2);
//                *(dst + 1) = *(ptrFrame + 1);
//                *(dst + 2) = *(ptrFrame);
//            }
////            memccpy(dst, textureHandle, 0, w * 3 * h);
//
//            flip(img_object, img_object, 0);
//            logFile << "ImageTarget copied." << endl;
//////    frame.copyTo(image);
////////    cvtColor(frame, image, CV_RGB2BGR);
//////    cvtColor(frame, frame, CV_BGR2GRAY);
////            imshow("AR Engine Demo", img_object);
//            engine->AddTarget(img_object);
//            logFile << "ImageTarget added to engine." << endl;
//        } catch (char *e) {
//            logFile << e << endl << std::flush;
//        }
//    }

//    extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API InitForQrs() {
//        VideoFrameProvider *frameProvider = new Unity3DFrameProvider();
//        LKTracker *tracker = new LKTracker(trackingPointErrorThreshold);
//        main::ArDetector *qrAr = new QrDetector();
//        engine = std::make_unique<main::EngineQr>(frameProvider, qrAr, tracker);
//    }


    extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API CreateWindow() {
        namedWindow("AR Engine Demo", 1);
    }

    extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API ShowImageOnWindow() {
        imshow("AR Engine Demo", colorFrame);
    }

    extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API ShowFrameOnWindow() {
        imshow("AR Engine Demo", frameProvider->lastFrame);
    }

    extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API SetLoopbackTextureFromUnity(void *textureHandle) {
        // A script calls this at initialization time; just remember the texture pointer here.
        // Will update texture pixels each frame from the plugins rendering event (texture update
        // needs to happen on the rendering thread).
        loopbackTextureHandle = textureHandle;
    }

    extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API SetCamTextureFromUnity(void *textureHandle, int w, int h) {
        camTextureHandle = textureHandle;
        camTextureWidth = w;
        camTextureHeight = h;
//        frameProvider->lastFrame = UMat(camTextureHeight, camTextureWidth, CV_8UC3);
        frameProvider->lastFrame = Mat(w, h, CV_8UC1);

    }

    extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API Dispose() {
        destroyAllWindows();
        logFile.flush();
        logFile.close();
    }

#if UNITY_WEBGL
    typedef void	(UNITY_INTERFACE_API * PluginLoadFunc)(IUnityInterfaces* unityInterfaces);
    typedef void	(UNITY_INTERFACE_API * PluginUnloadFunc)();

    extern "C" void	UnityRegisterRenderingPlugin(PluginLoadFunc loadPlugin, PluginUnloadFunc unloadPlugin);

    extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API RegisterPlugin()
    {
        UnityRegisterRenderingPlugin(UnityPluginLoad, UnityPluginUnload);
    }
#endif

//extern "C" int UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API UpdateTextureFromUnity(void *textureData, int width, int height) {
////    frame = *new Mat();
////    const int rowPitch = width * 3;
////    void *textureDataPtr = new unsigned char[rowPitch * height];
////    unsigned char *dst = (unsigned char *) textureDataPtr;
////    unsigned char *ptrFrame = (unsigned char *) textureHandle;
//    if (textureData == NULL) {
//        return 777;
//    }
//    return *((int *) textureData);
////    for (size_t i = 0; i < height * width; ++i, ptrFrame += 3, dst += 3) {
////        *dst = *(ptrFrame + 2);
////        *(dst + 1) = *(ptrFrame + 1);
////        *(dst + 2) = *(ptrFrame);
////    }
////    memccpy(textureHandle, textureDataPtr, 0, width*3*height);
////    frame.data = (uchar *) textureDataPtr;
////    frame.copyTo(image);
//////    cvtColor(frame, image, CV_RGB2BGR);
////    cvtColor(frame, frame, CV_BGR2GRAY);
//
//}

//    extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API GrabFrame() {
////    frame = Mat(g_TextureHeight, g_TextureWidth, CV_8UC1);
////    void *textureDataPtr = image.data;
//        GLuint gltex = (GLuint) (std::size_t) (camTextureHandle);
//        // Update texture data, and free the memory buffer
//        glBindTexture(GL_TEXTURE_2D, gltex);
//        glGetTexImage(GL_TEXTURE_2D,
//                      0,
//                      GL_BGR, // GL will convert to this format
//                      GL_UNSIGNED_BYTE,   // Using this data type per-pixel
//                      frameProvider->lastFrame.handle(ACCESS_RW)); //FIXME revisar si este puntero sirve, el viejo es mat.data
//
//        flip(frameProvider->lastFrame, frameProvider->lastFrame, 0);
//    }
//
//
//
//    extern "C" int *UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API ProcessFrame(char *buffer) {
//        logFile << "Process frame starts." << endl << std::flush;
////        cvtColor(frameProvider->lastFrame, grayFrame, CV_BGR2GRAY);
//
//
//        try {
//            engine->ProcessFrame(homography, rvec, tvec);
//        } catch (const std::exception &e) {
//            logFile << e.what() << endl << std::flush;
//            strcpy(buffer, e.what());
//        }
//
//        logFile << "Process frame went through engine." << endl << std::flush;
//        void *ret = nullptr;
//
//        if (engine->state == ImageBasedEngine::TARGET_TRACKING) {
//            Mat rotationMatrix;
//            Rodrigues(rvec, rotationMatrix, noArray());
//
//            results[0] = tvec.at<double>(0, 0);
//            results[1] = tvec.at<double>(1, 0);
//            results[2] = tvec.at<double>(2, 0);
//
////            memcpy(&results + sizeof(double) * 3, rotationMatrix.data, sizeof(double) * 9);
//            results[3] = rotationMatrix.at<double>(0, 0);
//            results[4] = rotationMatrix.at<double>(1, 0);
//            results[5] = rotationMatrix.at<double>(2, 0);
//            results[6] = rotationMatrix.at<double>(3, 0);
//            results[7] = rotationMatrix.at<double>(4, 0);
//            results[8] = rotationMatrix.at<double>(5, 0);
//            results[9] = rotationMatrix.at<double>(6, 0);
//            results[10] = rotationMatrix.at<double>(7, 0);
//            results[11] = rotationMatrix.at<double>(8, 0);
//            ret = results;
//        }
//        logFile << "Process frame results copied." << endl << std::flush;
//        return (int *) ret;
//    }
//
//    static void ModifyTexturePixels() {
//        void *textureHandle = loopbackTextureHandle;
//        int width = camTextureWidth;
//        int height = camTextureHeight;
//        if (!textureHandle)
//            return;
//        if (frameProvider->lastFrame.channels() == 0)
//            return;
//
//        // Flip image since the video comes upside down
//        Mat temp;
//        flip(frameProvider->lastFrame, temp, 0);
//
//        void *textureDataPtr = temp.data;
//
//        GLuint gltex = (GLuint) (size_t) (textureHandle);
//
//        // Bind (select) gl texture
//        glBindTexture(GL_TEXTURE_2D, gltex);
//        // Upload new data to it
//        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_BGR, GL_UNSIGNED_BYTE, textureDataPtr);
//    }
//
////    void GrabImageTargets() {
////        for (TargetData &targetData: targetsData) {
////            logFile << targetData.textureHandle << endl;
////            logFile << targetData.w << endl;
////            logFile << targetData.h << endl;
////            logFile.flush();
////            Mat img_object = Mat(targetData.h, targetData.w, CV_8UC3);
////            GLuint gltex = (GLuint) (size_t) (targetData.textureHandle);
////            // Update texture data, and free the memory buffer
////            glBindTexture(GL_TEXTURE_2D, gltex);
////            glGetTexImage(GL_TEXTURE_2D,
////                          0,
////                          GL_BGR, // GL will convert to this format
////                          GL_UNSIGNED_BYTE,   // Using this data type per-pixel
////                          (img_object).data);
////
////            flip((img_object), (img_object), 0);
////            targetData.img = img_object;
////
////            logFile << "Textura copiada." << endl;
////            logFile.flush();
////        }
////    }
//
//    static void UNITY_INTERFACE_API OnRenderEvent(int eventID) {
//        switch (eventID) {
////            case 1: {
////                DebugInUnity("RenderEvent case 1 start.");
////                logFile << "RenderEvent case 1 start." << endl;
////                logFile.flush();
////
////                GrabImageTargets();
////                DebugInUnity("RenderEvent case 1 end.");
////
////                logFile << "RenderEvent case 1 end." << endl;
////                logFile.flush();
////            }
//            case 2: {
////                logFile <<"ModifyTexturePixels start."<<endl;
////                logFile.flush();
//
////                logFile <<"ModifyTexturePixels end."<<endl;
////                logFile.flush();
//
////                logFile <<"GrabFrame start."<<endl;
////                logFile.flush();
//                GrabFrame();
////                logFile <<"GrabFrame end."<<endl;
////                logFile.flush();
//            }
//            case 3: {
//                ModifyTexturePixels();
//            }
//        }
//    }
//
//// --------------------------------------------------------------------------
//// GetRenderEventFunc, an example function we export which is used to get a rendering event callback function.
//    extern "C" UnityRenderingEvent UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API GetRenderEventFunc() {
//        return OnRenderEvent;
//    }

}