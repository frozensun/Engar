/*
 * Homography.cpp
 *
 *  Created on: Nov 18, 2014
 *      Author: tigershark
 */

#include "HomographyHelper.h"

#include <iostream>
#include <ctime>

#include "opencv2/calib3d/calib3d.hpp"
#include <opencv2/imgproc.hpp>

#include "Utils.h"

using namespace cv;

namespace engar {
    HomographyHelper::HomographyHelper(Mat &img_object, Mat &img_scene, Ptr<KeypointMatches> keypointMatches) {

        //init(perObjectKeypoints, keypoints_scene, good_matches, img_object, img_scene);
    }

    HomographyHelper::HomographyHelper() = default;

    bool HomographyHelper::find(vector<Point2f> &initialCorners,
                                vector<KeyPoint> &keypoints_object,
                                Ptr<KeypointMatches> keypointMatches,
                                vector<Point2f> &estimatedCorners,
                                vector<KeyPoint> &inliers) {

        clock_t begin, end;
        Mat mask;

        vector<KeyPoint> &keypoints_scene = *(keypointMatches->SceneKeypoints);
        vector<DMatch> &good_matches = *(keypointMatches->GoodMatches);

        if (good_matches.size() > 10) {

            inliers.clear();

            //-- Localize the object from img_1 in img_2
            std::vector<Point2f> obj;
            std::vector<Point2f> scene;
            begin = clock();
            for (auto &good_match : good_matches) {
                //-- Get the keypoints from the good matches
                obj.push_back(keypoints_object[good_match.queryIdx].pt);
                scene.push_back(keypoints_scene[good_match.trainIdx].pt);
            }

            Mat matrix = findHomography(obj, scene, RANSAC, 11, mask);

            vector<DMatch> inlierMatches;
            int inliersCount = 0;
            for (int i = 0; i < mask.rows; i++) {
                if (mask.at<bool>(i)) {
                    inliers.push_back(keypoints_scene[good_matches[i].trainIdx]);
                    inlierMatches.push_back(good_matches[i]);
                    inliersCount++;
                }
            }

            end = clock();

            if (inliersCount < 4) {
                return false;
            }
//
//        cout << "\tFind Homography time: " << double(end - begin) / CLOCKS_PER_SEC * 1000;
//        cout << " --> Inliers matches: " << matchesCount << endl;



            //drawMatches(img_object, perObjectKeypoints, img_scene, keypoints_scene, inlierMatches, image, Scalar::all(-1), Scalar::all(-1), vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);



            // Transform the points using homography
            perspectiveTransform(initialCorners, estimatedCorners, matrix);

            bool isParallelogram = Utils::doIntersect(estimatedCorners[0], estimatedCorners[2], estimatedCorners[1], estimatedCorners[3]);
//        isParallelogram = Utils::doIntersect(estimatedCorners[0], estimatedCorners[2], estimatedCorners[1], estimatedCorners[3]);

            // If the transformed points do not form a parallelogram, the object was not recognized
            if (isParallelogram) {

                Point2f a(abs(estimatedCorners[0].x - estimatedCorners[1].x), abs(estimatedCorners[0].y - estimatedCorners[1].y));
                Point2f b(abs(estimatedCorners[0].x - estimatedCorners[2].x), abs(estimatedCorners[0].y - estimatedCorners[2].y));
                double area = a.cross(b);

                if (area > 500) {
                    cout << "Is parallelogram:" << isParallelogram << " Area: " << area << endl;


                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }

            //-- Show detected matches
            //imshow("LK Demo", image);
        } else {
            //imshow("LK Demo", img_scene);
            return false;
        }
    }


    bool HomographyHelper::find(const vector<Point2f> &objectPoints,
                                const vector<Point2f> &scenePoints,
                                const vector<Point2f> &initialCorners,
                                vector<Point2f> &estimatedCorners,
                                vector<Point2f> &inliers,
                                Mat &matrix) {

        clock_t begin, end;
        Mat mask;

        if (objectPoints.size() > 5) {

            inliers.clear();
            matrix = findHomography(objectPoints, scenePoints, RANSAC, 5, mask);

            // Limpiamos los puntos que quedaron fuera del rectangulo actual
            int inliersCount = 0;
            uchar *data = mask.data;
            for (int i = 0; i < mask.rows; i++) {
                if (*data) {
                    inliers.push_back(scenePoints[i]);
                    inliersCount++;
                }
                data++;
            }
            ////////////////////////////////////////////////////////////////

            end = clock();

            if (inliersCount < 4) {
                return false;
            }
//
//        cout << "\tFind Homography time: " << double(end - begin) / CLOCKS_PER_SEC * 1000;
//        cout << " --> Inliers matches: " << matchesCount << endl;



            //drawMatches(img_object, perObjectKeypoints, img_scene, keypoints_scene, inlierMatches, image, Scalar::all(-1), Scalar::all(-1), vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);



            // Transform the points using homography
            perspectiveTransform(initialCorners, estimatedCorners, matrix);

            // If the transformed points do not form a parallelogram, the object was not recognized
            bool isParallelogram = Utils::doIntersect(estimatedCorners[0], estimatedCorners[2], estimatedCorners[1], estimatedCorners[3]);

//        Point2f a = estimatedCorners[0] - estimatedCorners[2];
//        Point2f b = estimatedCorners[1] - estimatedCorners[3];
//
//        double aMag = norm(a);
//        double bMag = norm(b);
//        isParallelogram &= (aMag > 0.3 * bMag && bMag > 0.3 * aMag);
            if (isParallelogram) {

                double area = computeQuadrilateralArea(estimatedCorners);

                return (area > 500);
            } else {
                return false;
            }

            //-- Show detected matches
            //imshow("LK Demo", image);
        } else {
            //imshow("LK Demo", img_scene);
            return false;
        }
    }

    double HomographyHelper::computeQuadrilateralArea(const vector<Point2f> &estimatedCorners) {
        Point2f a(abs(estimatedCorners[0].x - estimatedCorners[1].x), abs(estimatedCorners[0].y - estimatedCorners[1].y));
        Point2f b(abs(estimatedCorners[0].x - estimatedCorners[2].x), abs(estimatedCorners[0].y - estimatedCorners[2].y));
        return abs(a.cross(b));
    }


    HomographyHelper::HomographyHelper(Mat &img_object, vector<KeyPoint> &keypoints_object, Mat &img_scene, vector<KeyPoint> &keypoints_scene,
                                       vector<DMatch> &good_matches) {

        //init(perObjectKeypoints, keypoints_scene, good_matches, img_object, img_scene);
    }

    HomographyHelper::~HomographyHelper() = default;

}