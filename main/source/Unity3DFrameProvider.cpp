//
// Created by TigerShark on 17/9/2017.
//

#include "Unity3DFrameProvider.h"

void engar::Unity3DFrameProvider::GrabFrame(UMat &frame) {
    frame = this->lastFrame.getUMat(cv::ACCESS_READ);
}

void engar::Unity3DFrameProvider::GrabFrame(Mat &frame) {
    frame = this->lastFrame;
}

void engar::Unity3DFrameProvider::Reset() {

}
