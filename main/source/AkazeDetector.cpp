////
//// Created by TigerShark on 9/10/2017.
////

#include "Utils.h"
#include "AkazeDetector.h"
#include <opencv2/imgproc.hpp>

using namespace cv;

namespace engar {

    cv::Ptr<ArDetectionResult> AkazeDetector::detect(cv::InputArray &frame, const cv::OutputArray &descriptors_object) {
        auto t1 = std::chrono::high_resolution_clock::now();
        KeypointMatches &keysAndMatch = *(this->result->KPMatches);
        UMat descriptors_scene;;
        keysAndMatch.SceneKeypoints->clear();

        detector->detectAndCompute(frame, noArray(), *keysAndMatch.SceneKeypoints, descriptors_scene);
        detectTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

        /// Matching descriptor vectors
        t1 = std::chrono::high_resolution_clock::now();
        matches.clear();

        matcher->knnMatch(descriptors_scene, matches, 2);

        /// Apply ratio test
        keysAndMatch.GoodMatches->clear();

        const float ratio = 0.8f;    // As in Lowe's paper
        for (auto &match                : matches) {
            if (!match.empty() && match[0].distance < 80 && match[0].distance <= ratio * match[1].distance) {
                keysAndMatch.GoodMatches->emplace_back(match[0]);
            }
        }

        matchTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);
        matchCount++;

        return result;
    }

    engar::AkazeDetector::AkazeDetector() {
/** @brief The AKAZE constructor

@param descriptor_type Type of the extracted descriptor: DESCRIPTOR_KAZE,
DESCRIPTOR_KAZE_UPRIGHT, DESCRIPTOR_MLDB or DESCRIPTOR_MLDB_UPRIGHT.
@param descriptor_size Size of the descriptor in bits. 0 -\> Full size
@param descriptor_channels Number of channels in the descriptor (1, 2, 3)
@param threshold Detector response threshold to accept point
@param nOctaves Maximum octave evolution of the image
@param nOctaveLayers Default number of sublevels per scale level
@param diffusivity Diffusivity type. DIFF_PM_G1, DIFF_PM_G2, DIFF_WEICKERT or
DIFF_CHARBONNIER
*/
//(int descriptor_type=AKAZE::DESCRIPTOR_MLDB, int descriptor_size = 0, int descriptor_channels = 3, float threshold = 0.001f
// , int nOctaves = 4, int nOctaveLayers = 4, int diffusivity = KAZE::DIFF_PM_G2);
        this->detector = AKAZE::create(AKAZE::DESCRIPTOR_MLDB,
                                       0, 3,
                                       0.010f, 6,
                                       4, KAZE::DIFF_PM_G2);

        this->matcher = new BFMatcher(NORM_HAMMING, false);


    }

    Ptr<engar::ArDetectionResult> engar::AkazeDetector::detect(cv::InputArray &frame) {
        return Ptr<engar::ArDetectionResult>();
    }

    void engar::AkazeDetector::computeKeyPoints(Mat &target, vector<KeyPoint> &keypoints_object, UMat &descriptors_object) {
        Mat temp;
        if (target.channels() > 1) {
            cvtColor(target, temp, COLOR_BGR2GRAY);
        } else {
            temp = target;
        }

        /// Calculamos los keypoints del target.
        detector->detectAndCompute(temp, noArray(), keypoints_object, descriptors_object);
    }


    void engar::AkazeDetector::computeKeyPoints(Mat &target, vector<KeyPoint> &keypoints_object, Mat &descriptors_object) {
        Mat temp;
        if (target.channels() > 1) {
            cvtColor(target, temp, COLOR_BGR2GRAY);
        } else {
            temp = target;
        }
        // Calculamos los keypoints del target.
        detector->detectAndCompute(temp, noArray(), keypoints_object, descriptors_object);

        cout << "Found " << keypoints_object.size() << " keypoints." << endl;
    }

}