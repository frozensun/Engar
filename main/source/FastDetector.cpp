////
//// Created by TigerShark on 9/10/2017.
////

#include <algorithm>
#include "Utils.h"
#include "FastDetector.h"
#include <opencv2/xfeatures2d/nonfree.hpp>
#include <opencv2/imgproc.hpp>
#include <matchers/FLANNMatcher.h>
#include <kgraph.h>
#include <matchers/KGraphMatcher.h>
#include "matchers/NGTMatcher.h"
#include "matchers/NMSMatcher.h"

using namespace cv;

namespace engar {

    cv::Ptr<ArDetectionResult> FastDetector::detect(cv::InputArray &frame, const cv::OutputArray &descriptors_object) {
        auto t1 = std::chrono::high_resolution_clock::now();
        KeypointMatches &keysAndMatch = *(this->result->KPMatches);
        UMat descriptors_scene;;
        keysAndMatch.SceneKeypoints->clear();

        detector->detectAndCompute(frame, noArray(), *keysAndMatch.SceneKeypoints, descriptors_scene);
        ulong top = this->nfeatures;
        if (keysAndMatch.SceneKeypoints->size() > top) {
            keysAndMatch.SceneKeypoints->resize(min(top, keysAndMatch.SceneKeypoints->size()));
            descriptors_scene = descriptors_scene(Rect(0, 0, descriptors_scene.cols, min(this->nfeatures, descriptors_scene.rows)));
        }

//        Mat temp;
//        frame.copyTo(temp);
//        Utils::PrintTrackingPoints(temp, *keysAndMatch.SceneKeypoints);
//        imshow("Features", temp);

//        detector->detect(frame, *keysAndMatch.SceneKeypoints);
//        descriptor->compute(frame, *keysAndMatch.SceneKeypoints, descriptors_scene);
        detectTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

        /// Matching descriptor vectors
        t1 = std::chrono::high_resolution_clock::now();
//        matches.clear();

        matcher->knnMatch(descriptors_scene, matches, 2);

        /// Apply ratio test
        keysAndMatch.GoodMatches->clear();

//        vector<Mat> temp;
//        descriptors_object.getMatVector(temp);
//        Mat temp2 = descriptors_scene.getMat(ACCESS_READ);
        const float ratio = 0.8f;    // As in Lowe's paper
        int i = 0;
        for (auto &match : matches) {
//            if (!match.empty() && match[0].distance < 80 && match[0].distance <= ratio * match[1].distance) { // TODO DESCOMENTAR!!
            keysAndMatch.GoodMatches->emplace_back(match[0]);
//                cout << i<< endl;
//                cout << temp[0].row(match[0].trainIdx) << endl;
//                cout << temp2.row(match[0].queryIdx) << endl << endl;
//                i++;
//            }
        }

        matchTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);
        matchCount++;

        return result;
    }

    engar::FastDetector::FastDetector(int nFeatures) {
        this->nfeatures = nFeatures;
        this->detector = FastFeatureDetector::create(70);
//        this->detector = SIFT::create(this->nfeatures);

//        this->matcher = new BFMatcher(NORM_L2, false);

//        ::flann::IndexParams indexParams = ::flann::AutotunedIndexParams();
//        indexParams = ::flann::AutotunedIndexParams(0.8, 0.001, 0, 0.2);
////        ::flann::IndexParams indexParams = ::flann::HierarchicalClusteringIndexParams(32, ::flann::FLANN_CENTERS_RANDOM, 4, 100);
//        ::flann::SearchParams searchParams = ::flann::SearchParams();
//        searchParams.checks = 32;
//        searchParams.cores = 12;
//        this->matcher = new main::FLANNMatcher<float, ::flann::L2, float>(indexParams, searchParams);

//        indexParams = Ptr<cv::flann::IndexParams>(new cv::flann::AutotunedIndexParams(0.8, 0.001, 0, 0.2));
//        searchParams = Ptr<cv::flann::SearchParams>(new cv::flann::SearchParams());
//        searchParams->setInt("checks", 32);
//        searchParams->setInt("cores", 12);
//        this->matcher = new FlannBasedMatcher(indexParams, searchParams);

//        NGT::Property property;
//        property.setDefault();
//        property.dimension = 128;
//        property.distanceType = NGT::Property::DistanceType::DistanceTypeL2;
//        property.objectType = NGT::Property::ObjectType::Float;
//        property.edgeSizeForCreation = 20;
//        property.outgoingEdge = 8;
//        property.incomingEdge = 16;
////        property.edgeSizeForCreation = 20;
////        property.outgoingEdge = 4;
////        property.incomingEdge = 8;
//        property.indexType = NGT::Property::IndexType::GraphAndTree;
//        property.graphType = NGT::Property::GraphType::GraphTypeANNG;
//        this->matcher = new main::NGTMatcher(property, 0.1);

//        similarity::AnyParams *indexParams = new similarity::AnyParams({"efConstruction=2000", "M=10"});
//        similarity::AnyParams *searchParams = new similarity::AnyParams({"ef=10"});
//        this->matcher = new NMSMatcher<float>("hnsw", "l2", indexParams, searchParams);

        kgraph::KGraph::IndexParams indexParams;
        indexParams.K = 1;
        indexParams.L = 10;
        indexParams.recall = 0.5;
        indexParams.iterations = 32;
        this->matcher = new engar::KGraphMatcher<float, kgraph::metric::l2>(128, indexParams);

        this->result.reset(new ArDetectionResult());
        this->result->KPMatches = new KeypointMatches();
        this->result->KPMatches->SceneKeypoints->reserve(this->nfeatures);
        this->result->KPMatches->GoodMatches->reserve(this->nfeatures);
        this->matches.resize(this->nfeatures);
    }

    Ptr<engar::ArDetectionResult> engar::FastDetector::detect(cv::InputArray &frame) {
        return Ptr<engar::ArDetectionResult>();
    }

    void engar::FastDetector::computeKeyPoints(Mat &target, vector<KeyPoint> &keypoints_object, UMat &descriptors_object) {
        Mat temp;
        if (target.channels() > 1) {
            cvtColor(target, temp, COLOR_BGR2GRAY);
        } else {
            temp = target;
        }

        /// Calculamos los keypoints del target.
        detector->detectAndCompute(temp, noArray(), keypoints_object, descriptors_object);
        ulong top = this->nfeatures;
        if (keypoints_object.size() > top) {
            keypoints_object.resize(min(top, keypoints_object.size()));
            descriptors_object = descriptors_object(Rect(0, 0, descriptors_object.cols, min(this->nfeatures, descriptors_object.rows)));
        }

//        target.copyTo(temp);
//        Utils::PrintTrackingPoints(temp, keypoints_object);
//        imshow("Features", temp);
//        descriptor->compute(temp, keypoints_object, descriptors_object);
    }


    void engar::FastDetector::computeKeyPoints(Mat &target, vector<KeyPoint> &keypoints_object, Mat &descriptors_object) {
        Mat temp;
        if (target.channels() > 1) {
            cvtColor(target, temp, COLOR_BGR2GRAY);
        } else {
            temp = target;
        }
        // Calculamos los keypoints del target.
        detector->detectAndCompute(temp, noArray(), keypoints_object, descriptors_object);

        ulong top = this->nfeatures;
        if (keypoints_object.size() > top) {
            keypoints_object.resize(min(top, keypoints_object.size()));
            descriptors_object = descriptors_object(Rect(0, 0, descriptors_object.cols, min(this->nfeatures, descriptors_object.rows)));
        }
//        detector->detect(temp, keypoints_object);
//        descriptor->compute(temp, keypoints_object, descriptors_object);

//        target.copyTo(temp);
//        Utils::PrintTrackingPoints(temp, keypoints_object);
//        imshow("AR ImageBasedEngine Demo", temp);

//        cout << "Found " << keypoints_object.size() << " keypoints." << endl;
    }

}