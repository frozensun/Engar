//
// Created by tigershark on 2/12/19.
//
#include <random>
#include <iostream>
#include <iomanip>
#include <thread>
#include <mutex>

#include "opencv2/imgcodecs.hpp"

#include "inferrers/CaffeNetAbstractInferrer.h"

using std::vector;
using namespace std;
using namespace cv;
using namespace cv::dnn;

namespace engar {

    CaffeNetAbstractInferrer::CaffeNetAbstractInferrer(std::string name) : Inferrer(name) {
        thread t([&]() {
            std::mutex mtx;
            std::unique_lock<std::mutex> lk(mtx);

            while (!End) {
                while (!pendingChips.empty()) {
                    try {
                        Mat inputBlob = pendingChips.front();
                        pendingChips.pop();

                        net.setInput(inputBlob, inputName);

                        Mat detection = net.forward(outputName);

                        map<string, vector<pair<string, float>>> *allInferences = resultLocations.front();
                        resultLocations.pop();

                        vector<pair<string, float>> result;

                        int size = detection.cols;

                        if (size < 1) { continue; }

                        result.reserve(size);

                        for (int i = 0; i < size; i++) {
                            result.emplace_back(classes[i], detection.at<float>(i));
                        }

                        sort(result.begin(), result.end(), [](pair<string, float> &p1, pair<string, float> &p2) {
                            return p1.second > p2.second;
                        });

                        (*allInferences)[Name] = move(result);
                    } catch (Exception &e) {
                        cout << "Exception in inference thread" << endl << e.what() << endl << endl;
                    }
                }
//                this_thread::sleep_for(250ms);
                lock.wait(lk);
            }
            cout << "Infer thread ends " << endl;
        });
        t.detach();
    }

    void CaffeNetAbstractInferrer::AsyncInfer(Mat &image, map<string, vector<pair<string, float>>> &result) {
//        UMat inputBlob = cv::dnn::blobFromImage(image, 1.0, insize, meanColor, true).getUMat(ACCESS_READ, USAGE_ALLOCATE_DEVICE_MEMORY);
        Mat inputBlob = cv::dnn::blobFromImage(image, 1.0, insize, meanColor, true);
        resultLocations.push(&result);
        pendingChips.push(inputBlob);
        lock.notify_one();
    }

    void CaffeNetAbstractInferrer::Infer(Mat &image, vector<pair<string, float>> &result) {
        Mat inputBlob = cv::dnn::blobFromImage(image, 1.0, insize, meanColor, true);//.getUMat(ACCESS_RW, USAGE_ALLOCATE_SHARED_MEMORY);

        net.setInput(inputBlob, inputName);
        cv::Mat detection = net.forward(outputName);

        int size = detection.cols;
        result = std::vector<std::pair<std::string, float>>();
        result.reserve(size);

        for (int i = 0; i < size; i++) {
            result.emplace_back(classes[i], detection.at<float>(i));
        }

        sort(result.begin(), result.end(), [](pair<string, float> &p1, pair<string, float> &p2) {
            return p1.second > p2.second;
        });

        cout << detection;
    }

    void CaffeNetAbstractInferrer::Infer(UMat &image, std::vector<std::pair<std::string, float>> &result) {
        UMat inputBlob = cv::dnn::blobFromImage(image, 1.0, insize, meanColor).getUMat(ACCESS_RW, USAGE_ALLOCATE_SHARED_MEMORY);

        net.setInput(inputBlob, inputName);
        cv::Mat detection = net.forward(outputName);

        int size = detection.cols;
        result = std::vector<std::pair<std::string, float>>();
        result.reserve(size);

        for (int i = 0; i < size; i++) {
            result.emplace_back(classes[i], detection.at<float>(i));
        }

        sort(result.begin(), result.end(), [](pair<string, float> &p1, pair<string, float> &p2) {
            return p1.second > p2.second;
        });
    }

}