//
// Created by TigerShark on 17/9/2017.
//

#include <opencv2/core/core.hpp>
#include <iostream>
#include "OpenCvFrameProvider.h"

using namespace cv;

engar::OpenCvFrameProvider::OpenCvFrameProvider() {
    this->videoCapture = new VideoCapture(0);

    if (!this->videoCapture->set(cv::CAP_PROP_FRAME_WIDTH, 640)) {
        cerr << "Failed to set frame width: " << 640 << " (ignoring)" << endl;
    }

    if (!this->videoCapture->set(cv::CAP_PROP_FRAME_HEIGHT, 480)) {
        cerr << "Failed to set frame height: " << 480 << " (ignoring)" << endl;
    }

    if (!this->videoCapture->set(cv::CAP_PROP_FPS, 120)) {
        cerr << "Failed to set fps: " << 25 << " (ignoring)" << endl;
    }

    if (!this->videoCapture->set(cv::CAP_PROP_EXPOSURE, 0.01)) {
        cerr << "Failed to set CAP_PROP_EXPOSURE: " << 0 << " (ignoring)" << endl;
    }

    if (!this->videoCapture->set(cv::CAP_PROP_GAIN, 0.01)) {
        cerr << "Failed to set CAP_PROP_GAIN: " << 0 << " (ignoring)" << endl;
    }
}

engar::OpenCvFrameProvider::OpenCvFrameProvider(int deviceId, int w, int h) {
    this->videoCapture = new VideoCapture(deviceId);

    if (!this->videoCapture->set(cv::CAP_PROP_FRAME_WIDTH, w)) {
        cerr << "Failed to set frame width: " << w << " (ignoring)" << endl;
    }

    if (!this->videoCapture->set(cv::CAP_PROP_FRAME_HEIGHT, h)) {
        cerr << "Failed to set frame height: " << h << " (ignoring)" << endl;
    }

    if (!this->videoCapture->set(cv::CAP_PROP_FPS, 120)) {
        cerr << "Failed to set fps: " << 25 << " (ignoring)" << endl;
    }

    if (!this->videoCapture->set(cv::CAP_PROP_EXPOSURE, 0.01)) {
        cerr << "Failed to set CAP_PROP_EXPOSURE: " << 0 << " (ignoring)" << endl;
    }

    if (!this->videoCapture->set(cv::CAP_PROP_GAIN, 0.01)) {
        cerr << "Failed to set CAP_PROP_GAIN: " << 0 << " (ignoring)" << endl;
    }

//    cout << "OpenCV VideoCapture initialized. Running on: " << this->videoCapture->getBackendName() << endl;
}

engar::OpenCvFrameProvider::~OpenCvFrameProvider() {
    this->videoCapture->release();
}

void engar::OpenCvFrameProvider::GrabFrame(UMat &frame) {
    this->videoCapture->read(frame);
//    * (this->videoCapture) >> frame;
}

void engar::OpenCvFrameProvider::GrabFrame(Mat &frame) {
    *(this->videoCapture) >> lastFrame;
    lastFrame.copyTo(frame);
}

void engar::OpenCvFrameProvider::Reset() {

}
