//
// Created by tigershark on 13/9/19.
//

#include "DlibFaceDetector.h"
#include "opencv2/core/mat.hpp"
#include "dlib/opencv.h"
#include <dlib/image_processing/frontal_face_detector.h>

using namespace dlib;
using namespace std;

using namespace cv;

namespace engar {

    DlibFaceDetector::DlibFaceDetector() {
        detector = dlib::get_frontal_face_detector();
    }

    void engar::DlibFaceDetector::detect(const UMat &colorFrame, std::vector<Rect> &faces) {
        // Run the face detector on the image of our action heroes, and for each face extract a
        // copy that has been normalized to 150x150 pixels in size and appropriately rotated
        // and centered.

        cv_image<bgr_pixel> img(colorFrame.getMat(ACCESS_RW));

        std::vector<rectangle> results = detector(img);
        for (const rectangle &face : results) {
            faces.emplace_back(face.left(), face.top(), face.width(), face.height());
        }

        if (faces.empty()) {
            cout << "No faces found in image!" << endl;
        }
    }

}