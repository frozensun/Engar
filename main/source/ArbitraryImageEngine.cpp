//
// Created by TigerShark on 9/10/2017.
//

#include "HomographyHelper.h"
#include "ArDetector.h"
#include "Utils.h"
#include <numeric>
#include <chrono>
#include "ArbitraryImageEngine.h"

using namespace std;
using namespace cv;
using namespace engar;

namespace engar {

    ArbitraryImageEngine::ArbitraryImageEngine(VideoFrameProvider *frameProvider, std::shared_ptr<ArDetector> detector, LKTracker *tracker, int targetCols,
                                               int targetRows,
                                               Mat &k) : ImageBasedEngine(frameProvider, detector, tracker) {
        K = k;
        cout << endl << "Creating Image Engine........";
        this->state = TARGET_DETECTION;

        this->targetCols = targetCols;
        this->targetRows = targetRows;

        /// Creamos corners base que iremos proyectando al detectar un target
        int halfW = targetCols / 2;
        int halfH = targetRows / 2;
        initialCorners.emplace_back(Point2f(-halfW, halfH));
        initialCorners.emplace_back(Point2f(halfW, halfH));
        initialCorners.emplace_back(Point2f(halfW, -halfH));
        initialCorners.emplace_back(Point2f(-halfW, -halfH));

        cfTargetPoints.reserve(200);
        cfScenePoints.reserve(200);

        cout << "done." << endl;
    }

    ArbitraryImageEngine::ArbitraryImageEngine(VideoFrameProvider *frameProvider, std::shared_ptr<ArDetector> detector, LKTracker *tracker, int targetCols,
                                               int targetRows)
            : ImageBasedEngine(
            frameProvider, detector, tracker) {
        cout << endl << "Creating Image Engine........";
        this->state = TARGET_DETECTION;

        this->targetCols = targetCols;
        this->targetRows = targetRows;

        /// Creamos corners base que iremos proyectando al detectar un target
        int halfW = targetCols / 2;
        int halfH = targetRows / 2;
        initialCorners.emplace_back(Point2f(-halfW, halfH));
        initialCorners.emplace_back(Point2f(halfW, halfH));
        initialCorners.emplace_back(Point2f(halfW, -halfH));
        initialCorners.emplace_back(Point2f(-halfW, -halfH));

        cfTargetPoints.reserve(200);
        cfScenePoints.reserve(200);

        cout << "done." << endl;
    }

    void ArbitraryImageEngine::AddTarget(Mat &target) {
//        cout << "Adding target " << this->targetCount << ":" << endl;

        vector<KeyPoint> &targetKeypoints = perObjectKeypoints.emplace_back(vector<KeyPoint>());
        Mat &targetDescriptors = perObjectDescriptors.emplace_back(Mat());

//        Mat copy;
//        cv::resize(target, copy, Size(this->targetCols, this->targetRows), 0, 0, INTER_LINEAR_EXACT);
        this->detector->computeKeyPoints(target, targetKeypoints, targetDescriptors);
//        cout << endl << targetDescriptors << endl;
        targets.emplace_back(target);

        if (targetKeypoints.size() < this->detector->nfeatures) {
            cout << "Warning: Target " << this->targetCount << ": only " << targetKeypoints.size() << "/" << this->detector->nfeatures << " keypoints found."
                 << endl;
        }

        vector<Point3f> &target3DPoints = baseObj3DPoints.emplace_back(vector<Point3f>());
        vector<Point2f> &target2DPoints = baseObj2DPoints.emplace_back(vector<Point2f>());

//        cout << "\tCentering key points." << endl;
        float x = target.cols / 2.0f;
        float y = target.rows / 2.0f;

        for (KeyPoint &kp : targetKeypoints) {
            kp.pt.x = kp.pt.x - x;
            kp.pt.y = kp.pt.y - y;
            target2DPoints.emplace_back(kp.pt);
            target3DPoints.emplace_back(kp.pt.x, kp.pt.y, 0);
        }
        this->detector->matcher->add(targetDescriptors);
        this->targetCount++;
    }

    Mat ArbitraryImageEngine::getRawFrame() const {
        return Mat(); //this->colorFrame;
    }

    void ArbitraryImageEngine::Train() {
        this->detector->matcher->train();
    }

    void ArbitraryImageEngine::ShowTargets() {
        Mat dispImage = Mat::zeros(Size(1080, 810), CV_8U);
        int scale = ceil(sqrt(this->targetCount));
        int w = (int) (1080 / scale);
        int h = (int) (810 / scale);

        // Set the image ROI to display the current image
        // Resize the input image and copy the it to the Single Big Image
        for (int m = 0; m < scale; m++) {
            for (int n = 0; n < scale; n++) {
                int idx = m * scale + n;
                if (idx == this->targetCount) break;

                const Mat &img = this->targets[idx];
                Rect ROI(m * w, n * h, w, h);
                Mat temp;
                resize(img, temp, Size(ROI.width, ROI.height));
                temp.copyTo(dispImage(ROI));
            }
        }

        namedWindow("Targets");
//        for (const auto &kp : targetKeypoints) {
//            circle(copy, kp.pt, 3, Utils::Colors.Green, 2);
//        }
        imshow("Targets", dispImage);
    }

    void ArbitraryImageEngine::ProcessFrame(int &id, vector<cv::Point2f> &corners, Mat &homography, Mat &rvec, Mat &tvec) {
        auto t1 = std::chrono::high_resolution_clock::now();
        this->frameProvider->GrabFrame(frame);
        pf_grabFrame += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);
//        cout << "ImageBasedEngine.ProcessFrame: frame grabed." << endl << std::flush;

        ProcessFrame(frame, id, corners, homography, rvec, tvec);
    }

    void ArbitraryImageEngine::ProcessFrame(InputArray inputFrame, int &id, vector<cv::Point2f> &corners, Mat &homography, Mat &rvec, Mat &tvec) {
        auto t1 = std::chrono::high_resolution_clock::now();

        auto t4 = std::chrono::high_resolution_clock::now();
        if (inputFrame.empty()) {
            return;
        } else {
            frame = inputFrame.getMat();
        }

        pf_copyFrame += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t4);

        if (state == TARGET_DETECTION) {

            detectionStep(homography, rvec, tvec, corners);
            detectingCount++;
            detectingFrameTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

        } else if (state == TARGET_TRACKING) {

            trackingStep(homography, rvec, tvec, corners);
            auto temp = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

            if (temp < minTrackingTime) {
                minTrackingTime = temp;
            }
            if (temp > maxTrackingTime) {
                maxTrackingTime = temp;
            }
            trackingCount++;
            trackingFrameTime += temp;
        }

        id = targetId;

        frameTime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);
        allFrameTime += frameTime;
        frameCount++;
        if (frameTime > maxFrameTime) {
            maxFrameTime = frameTime;
        }
    }


    void ArbitraryImageEngine::trackingStep(Mat &homography, Mat &rvec, Mat &tvec, vector<cv::Point2f> &corners) {


        auto t1 = std::chrono::high_resolution_clock::now();
        stillFound = tracker->track(frame, prevScenePoints, currentScenePoints, homography);
        pf_tracker += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

        // If we have enough remaining points, find homography
        if (prevScenePoints.size() > 20 && currentScenePoints.size() > 20) {

            t1 = std::chrono::high_resolution_clock::now();
            Mat inliersMask;
            homography = findHomography(prevScenePoints, currentScenePoints, cv::RANSAC, 11, inliersMask);
            pf_homografyFind += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

            /// Limpiamos los puntos que quedaron fuera del rectangulo actual
            t1 = std::chrono::high_resolution_clock::now();
            unsigned int inliersCount = 0;
            uchar *data = inliersMask.data;
            for (int i = 0; i < inliersMask.rows; i++) {
                if (*data) {
                    inliersCount++;
                }
                data++;
            }
            pf_homografyInlierCleanUp += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);
            //////////////////////////////////////////////////////////////////

            if (inliersCount < 11) {
                prevScenePoints.clear();
                currentScenePoints.clear();
                return;
            }

            /// Transformamos los puntos usando la homografia
            perspectiveTransform(prevCorners, corners, homography);
            /////////////////////////////////////////

            /// Si los puntos transformados no forman un paralelogramo, la imagen no fue correctamente detectada
            t1 = std::chrono::high_resolution_clock::now();
            bool isParallelogram = Utils::doIntersect(corners[0], corners[2], corners[1], corners[3]);
            double area = 0;
            if (isParallelogram) {
                area = HomographyHelper::computeQuadrilateralArea(corners);
            }
            pf_shapeCheck += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

            if (inliersCount < 11 || area < 500) {
                prevScenePoints.clear();
                currentScenePoints.clear();

                stillFound = false;
            }
        } else {
            stillFound = false;
        }

        if (stillFound) {
            t1 = std::chrono::high_resolution_clock::now();
            perspectiveTransform(cfObj2DPoints, cfObj2DPoints, homography);
            pf_transform2DPoints += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

            // TODO no hace falta que sean esos puntos si los voy a transformar, se puede hacer lo mismo que con los qr para el caso

//            bool useExtrinsicGuess = false, int iterationsCount = 100,
//            float reprojectionError = 8.0, double confidence = 0.99,
//            OutputArray inliers = noArray(), int flags = SOLVEPNP_ITERATIVE );


            /// Obtenemos rvec y tvec
            t1 = std::chrono::high_resolution_clock::now();
            Mat inl;
            bool foundPnP = solvePnPRansac(cfObj3DPoints, cfObj2DPoints, K, noArray(), rvec, tvec, false, 100, 8.0, 0.99, inl, SOLVEPNP_ITERATIVE);
            pf_solvePnP += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

            if (!foundPnP) {
                prevScenePoints.clear();
                currentScenePoints.clear();
                state = TARGET_DETECTION;
                cout << "Image lost, reverse to detection..." << endl;
            }

            t1 = std::chrono::high_resolution_clock::now();
            const int *data = reinterpret_cast<int *>(inl.data);
            for (int i = 0; i < inl.rows; i++) {
                cfObj2DPoints[i] = move(cfObj2DPoints[*data]);
                cfObj3DPoints[i] = move(cfObj3DPoints[*data]);
                data++;
            }
            cfObj2DPoints.resize(inl.rows);
            cfObj3DPoints.resize(inl.rows);
            pf_3dOutlierCleanUp += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

            t1 = std::chrono::high_resolution_clock::now();
            std::swap(prevScenePoints, currentScenePoints);
            std::swap(corners, prevCorners);
            pf_swaps += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);
        } else {
            prevScenePoints.clear();
            currentScenePoints.clear();
            state = TARGET_DETECTION;
            targetId = -1;
            cout << "Image lost, reverse to detection..." << endl;
        }
    }

    void ArbitraryImageEngine::detectionStep(Mat &homography, Mat &rvec, Mat &tvec, vector<cv::Point2f> &corners) {

        try {
            Ptr<ArDetectionResult> result = detector->detect(frame, this->perObjectDescriptors);

            vector<KeyPoint> &currentFrameSceneKP = *(result->KPMatches->SceneKeypoints);
            vector<DMatch> &currentFrameGoodMatches = *(result->KPMatches->GoodMatches);

            cfObj2DPoints.clear();
            cfObj3DPoints.clear();
            cfTargetPoints.clear();
            cfScenePoints.clear();

            unsigned int inliersCount = 0;

            targetId = -1;
            vector<float> distances;

            if (currentFrameGoodMatches.size() > 10) {
                /// Determinamos cual fue el target encontrado
                ushort counts[this->targetCount];
                memset(&counts, 0, sizeof(ushort) * this->targetCount);
                for (auto const &good_match : currentFrameGoodMatches) {
                    counts[good_match.imgIdx]++;
                }

                ushort currentMax = 0;
                for (int i = 0; i < this->targetCount; i++) {
                    const ushort &count = counts[i];
                    if (count > 10 && count > currentMax) {
                        currentMax = count;
                        targetId = i;
                    }
                }
                if (targetId == -1) return;

                for (auto const &good_match : currentFrameGoodMatches) {
                    /// Salteamos las correspondencias que fueron con otras imagenes
                    /// TODO aplicar logica multi image target aqui
                    if (good_match.imgIdx != targetId) continue;

                    distances.emplace_back(good_match.distance);

                    /// Get the keypoints from the good matches
                    cfTargetPoints.emplace_back(perObjectKeypoints[targetId][good_match.trainIdx].pt);
                    cfScenePoints.emplace_back(move(currentFrameSceneKP[good_match.queryIdx].pt));
                    cfObj3DPoints.emplace_back(baseObj3DPoints[targetId][good_match.trainIdx]);
                    cfObj2DPoints.emplace_back(baseObj2DPoints[targetId][good_match.trainIdx]);
                }
                ////////////////////////////////////////////////////////////////////////

//                Utils::PrintTrackingPoints(colorFrame, cfScenePoints);

                auto t1 = std::chrono::high_resolution_clock::now();
                Mat inliersMask;
                homography = findHomography(cfTargetPoints, cfScenePoints, cv::RANSAC, 5, inliersMask);
                pf_homografyDetect += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

                if (homography.empty()) {
//                    cout << "No homography" << endl;
                    targetId = -1;
                    return;
                }

                /// Limpiamos los puntos que quedaron fuera del rectangulo actual
                // La matriz inliersMask contiene true para pares que son inliers y false para los que no
                uchar *data = inliersMask.data;
                for (int i = 0; i < inliersMask.rows; i++) {
                    if (*data) {

                        cfObj3DPoints[inliersCount] = move(cfObj3DPoints[i]);
                        cfObj2DPoints[inliersCount] = move(cfObj2DPoints[i]);
                        distances[inliersCount] = distances[i];
                        inliersCount++;
                    }
                    data++;
                }
                cfObj3DPoints.resize(inliersCount);
                cfObj2DPoints.resize(inliersCount);
                distances.resize(inliersCount);
                /////////////////////////////////////////////////////////////////

                if (inliersCount < 11) {
//                    cout << inliersCount << "  ";
                    targetId = -1;
                    return;
                }

                /// Transform the points using homography
                vector<Point2f> origin = {Point2f(0, 0)};
                perspectiveTransform(initialCorners, corners, homography);

                /// If the transformed points do not form a parallelogram, the object was not recognized
                bool isParallelogram = Utils::doIntersect(corners[0], corners[2], corners[1], corners[3]);
                if (isParallelogram) {
                    double area = HomographyHelper::computeQuadrilateralArea(corners);

                    if (area > 500) {


//                        float distSum = 0;
//                        for (float dist : distances) {
//                            distSum += dist;
//                        }
//                        cout << "Parallelogram with area: " << area << " and inliers avg error of: " << distSum / distances.size() << " detected." << endl;

//                        perspectiveTransform(origin, coordsCenter, matrix);
//                        circle(colorFrame, coordsCenter[0], 3, Utils::Colors.Yellow, 2);
                    } else {
//                        cout << "Is parallelogram with area = " << area << endl;
                        targetId = -1;
                        return;
                    }
                } else {
//                    cout << "Is NOT a parallelogram" << endl;
                    targetId = -1;
                    return;
                }

            } else {
#ifdef TRACE_ENGINE
                cout << "Not enough good matches " << currentFrameGoodMatches.size() << endl;
#endif
                targetId = -1;
                return;
            }
            //////////////////////////////////////////////////////////////////////////////////////////

            Mat inlierIdxs;
            bool foundPnP = solvePnPRansac(cfObj3DPoints, cfObj2DPoints, K, noArray(), rvec, tvec, false, 100, 8.0, 0.99, inlierIdxs, SOLVEPNP_ITERATIVE);
            if (!foundPnP) {
                prevScenePoints.clear();
                currentScenePoints.clear();
                state = TARGET_DETECTION;
                cout << "Object lost, reverse to detection..." << endl;
            }

            /// Limpiamos los puntos que quedaron fuera del pnp
            // La matriz inliersIdxs contiene los indices de los pares que son inliers
            inliersCount = 0;
            for (int i = 0; i < inlierIdxs.rows; i++) {
                int idx = inlierIdxs.at<int>(i);
                cfObj3DPoints[inliersCount] = move(cfObj3DPoints[idx]);
                cfObj2DPoints[inliersCount] = move(cfObj2DPoints[idx]);
                distances[inliersCount] = distances[idx];
                inliersCount++;
            }
            cfObj3DPoints.resize(inliersCount);
            cfObj2DPoints.resize(inliersCount);
            distances.resize(inliersCount);
            /////////////////////////////////////////////////////////////////

            /// Estadisticas
            float sum = 0;
            float max = 0;
            float min = LONG_MAX;
            for (float dist : distances) {
                sum += dist;
                max = (max > dist) ? max : dist;
                min = (min < dist) ? min : dist;
            }
            distanceSum += sum;
            distanceMinSum += min;
            distanceMaxSum += max;
            pointsCount += distances.size();
            ////////////////////////////////////////////////////////////////

            this->GoodMatchesCount += inlierIdxs.rows;
            this->DetectionsCount++;

            state = TARGET_TRACKING; /// TODO DESCOMENTAR!!!!!
            cout << "Image with id " << targetId << " found @[" << corners[3] << ", " << corners[0] << ", " << corners[2] << ", " << corners[1] << "] with "
                 << inliersCount << " kp, starting tracker..." << endl;

            /// Draw lines between the corners
//            Point2f offset(0, 0);
//            Utils::PrintEnclosingLines(colorFrame, offset, corners);
            //////////////////////////////////

            perspectiveTransform(this->cfObj2DPoints, this->cfObj2DPoints, homography);

//            cout << "Starting image tracker... " << endl << std::flush;
            tracker->init(frame, corners, prevScenePoints);

            prevCorners.clear();
            prevCorners.push_back(corners[0]);
            prevCorners.push_back(corners[1]);
            prevCorners.push_back(corners[2]);
            prevCorners.push_back(corners[3]);

        } catch (const exception &e) {
            cout << e.what() << endl << std::flush;
        }
    }

}