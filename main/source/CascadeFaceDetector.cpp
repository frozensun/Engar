//
// Created by tigershark on 12/8/19.
//

#include "CascadeFaceDetector.h"

#include "opencv2/objdetect.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>

using namespace std;
using namespace cv;

namespace engar {
    CascadeFaceDetector::CascadeFaceDetector(double initialScale, const Size &minFaceSize) {
        if (!nestedCascade.load("models/haarcascades/haarcascade_smile.xml"))
            cerr << "WARNING: Could not load classifier cascade for nested objects" << endl;
        if (!cascade.load("models/haarcascades/haarcascade_frontalface_default.xml")) {
            cerr << "ERROR: Could not load classifier cascade" << endl;
        }
        this->scale = initialScale;
        this->tryflip = false;
        this->minFaceSize = minFaceSize;
    }

    void CascadeFaceDetector::detect(const UMat &colorFrame, vector<Rect> &faces) {
        UMat gray;
        if (colorFrame.channels() > 1) {
            cvtColor(colorFrame, gray, COLOR_BGR2GRAY);
        } else {
            gray = colorFrame;
        }

        faces.clear();
        faces2.clear();

        double fx = 1 / scale;
        resize(gray, smallImg, Size(0, 0), fx, fx, INTER_LINEAR_EXACT);
        equalizeHist(smallImg, smallImg);

        cascade.detectMultiScale(smallImg, faces,
                                 1.2, 2, 0
                                         //|CASCADE_FIND_BIGGEST_OBJECT
                                         //|CASCADE_DO_ROUGH_SEARCH
                                         | CASCADE_SCALE_IMAGE,
                                 minFaceSize);

        if (tryflip) {
            flip(smallImg, smallImg, 1);
            cascade.detectMultiScale(smallImg, faces2,
                                     1.1, 2, 0
                                             //|CASCADE_FIND_BIGGEST_OBJECT
                                             //|CASCADE_DO_ROUGH_SEARCH
                                             | CASCADE_SCALE_IMAGE,
                                     minFaceSize);
            for (Rect &face : faces2) {
                faces.emplace_back(move(face));
            }
//            for (vector<Rect>::const_iterator r = faces2.begin(); r != faces2.end(); ++r) {
//                faces.emplace_back(smallImg.cols - r->x - r->width, r->y, r->width, r->height);
//            }
        }

    }
}