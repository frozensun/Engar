//
// Created by TigerShark on 9/10/2017.
//

#include "NGT/Index.h"
#include "matchers/NGTMatcher.h"
#include <flann/algorithms/dist.h>
#include "OrbDetector.h"
#include "Utils.h"
#include "matchers/NMSMatcher.h"
#include <matchers/FLANNMatcher.h>
#include "opencv4/opencv2/core/mat.hpp"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <matchers/KGraphMatcher.h>

namespace engar {

    using namespace cv;

    cv::Ptr<engar::ArDetectionResult>
    engar::OrbDetector::detect(cv::InputArray &frame, const cv::OutputArray &descriptors_object) {
        /// Detect the keypoints using ORB Detector and Calculate descriptors (feature vectors)
        auto t1 = std::chrono::high_resolution_clock::now();
        KeypointMatches &keysAndMatch = *(this->result->KPMatches);
        UMat descriptors_scene;
        Mat desc_scene;
//    UMat cop;
//    frame.copyTo(cop);
        keysAndMatch.SceneKeypoints->clear();
        detector->detectAndCompute(frame, noArray(), *keysAndMatch.SceneKeypoints, desc_scene);
        detectTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

        /// Matching descriptor vectors
        t1 = std::chrono::high_resolution_clock::now();
//    matches.clear();
        matcher->knnMatch(desc_scene, matches, 2);

        vector<Mat> temp;
        descriptors_object.getMatVector(temp);
        Mat temp2 = desc_scene;

        int i = 0;

        /// Apply ratio test
        keysAndMatch.GoodMatches->clear();
        const float ratio = 0.8f;    // As in Lowe's paper
        for (auto &match : matches) {
            if (!match.empty()
                && match[0].distance < 80
                && match[0].distance <= ratio * match[1].distance) {
                keysAndMatch.GoodMatches->emplace_back(match[0]);

#ifdef TRACE_DETECTORS
                cout << i << endl;

                Mat r = temp[0].row(match[0].trainIdx);
                for (int a = 0; a < r.cols; a++) {
                    int c = r.at<int>(a);
                    std::bitset<16> y(c);
                    std::cout << y << ' ';
                }

                cout << endl;
                r = temp2.row(match[0].queryIdx);
                for (int a = 0; a < r.cols; a++) {
                    int c = r.at<int>(a);
                    std::bitset<16> y(c);
                    std::cout << y << ' ';
                }
                cout << endl << endl;
#endif

                i++;
            }
        }

        matchTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);
        matchCount++;

        return result;
    }

    engar::OrbDetector::OrbDetector(int nFeatures) {
        this->nfeatures = nFeatures;
        // ORB(int nfeatures=500, float scaleFactor=1.2f, int nlevels=8, int edgeThreshold=31, int firstLevel=0, int WTA_K=2, int scoreType=ORB::HARRIS_SCORE, int patchSize=31)
        this->detector = ORB::create(nfeatures, scaleFactor, nlevels, edgeThreshold, firstLevel, WTA_K, scoreType, patchSize);

//    this->matcher = new BFMatcher(NORM_HAMMING, false);

//    ::flann::IndexParams indexParams = ::flann::HierarchicalClusteringIndexParams(32, ::flann::FLANN_CENTERS_RANDOM, 4, 100);
//    ::flann::SearchParams searchParams = ::flann::SearchParams();
//    searchParams.cores = 12;
//    this->matcher = new main::FLANNMatcher<unsigned char, ::flann::HammingPopcnt, int>(indexParams, searchParams);
//

//    NGT::Property property;
//    property.setDefault();
//    property.dimension = 32;
//    property.distanceType = NGT::Property::DistanceType::DistanceTypeHamming;
//    property.objectType = NGT::Property::ObjectType::Uint8;
//    property.edgeSizeForCreation = 40;
//    property.outgoingEdge = 8;
//    property.incomingEdge = 20;
//    property.indexType = NGT::Property::IndexType::GraphAndTree;
//    property.graphType = NGT::Property::GraphType::GraphTypeANNG;
//    this->matcher = new main::NGTMatcher(property);

        similarity::AnyParams *indexParams = new similarity::AnyParams({"efConstruction=1600", "M=24", "post=2"});
        similarity::AnyParams *searchParams = new similarity::AnyParams({"ef=32"});
        this->matcher = new NMSMatcher<int>("hnsw", "bit_hamming", indexParams, searchParams);

//    kgraph::KGraph::IndexParams indexParams;
//    indexParams.K = 1;
//    indexParams.L = 10;
//    indexParams.recall = 0.5;
//    indexParams.iterations = 32;
//    this->matcher = new KGraphMatcher<unsigned char, kgraph::metric::hamming>(32, indexParams);

        this->result.reset(new ArDetectionResult());
        this->result->KPMatches = new KeypointMatches();
        this->result->KPMatches->SceneKeypoints->reserve(this->nfeatures);
        this->result->KPMatches->GoodMatches->reserve(this->nfeatures);
        this->matches.resize(this->nfeatures);
    }

    Ptr<engar::ArDetectionResult> engar::OrbDetector::detect(cv::InputArray &frame) {
        throw runtime_error("Unsupported operation.");
    }

    void engar::OrbDetector::computeKeyPoints(Mat &target, vector<KeyPoint> &keypoints_object, UMat &descriptors_object) {
        Mat temp;
        if (target.channels() > 1) {
            cvtColor(target, temp, cv::COLOR_BGR2GRAY);
        } else {
            temp = target;
        }

        /// Calculamos los keypoints del target.
        detector->detectAndCompute(temp, noArray(), keypoints_object, descriptors_object);

//    vector<vector<DMatch> > matches;
//    matcher->knnMatch(perObjectDescriptors, perObjectDescriptors, matches, 2);
//    //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
//    Ptr<vector<DMatch> > good_matches = new vector<DMatch>;
//    this->filteredKps.clear();
//    // Apply ratio test
//    const float ratio = 0.8f;    // As in Lowe's paper
//    for (auto &match : matches) {
//        if (!match.empty() && match[1].distance > 10) {
//            filteredKps.push_back((*perObjectKeypoints)[match[0].queryIdx]);
//        }
//    }
//    perObjectKeypoints = &filteredKps;
        ///////////////////////////////////////////////////////////////////////////

//    Scalar green = Scalar(0, 255, 0);
//    vector<Point2f> points;
//    KeyPoint::convert(perObjectKeypoints, points);
//    Mat temp;
//    cvtColor(target, temp, COLOR_GRAY2RGB);
//    for (auto &point : points) {
//        circle(temp, point, 3, green, -1, 8);
//    }
    }


    void engar::OrbDetector::computeKeyPoints(Mat &target, vector<KeyPoint> &keypoints_object, Mat &descriptors_object) {
        Mat temp;
        if (target.channels() > 1) {
            cvtColor(target, temp, cv::COLOR_BGR2GRAY);
        } else {
            temp = target;
        }
        // Calculamos los keypoints del target.
        detector->detectAndCompute(temp, noArray(), keypoints_object, descriptors_object);

//    cout << "Found " << keypoints_object.size() << " keypoints." << endl;
    }

    engar::OrbDetector::~OrbDetector() {
        this->matcher.release();
    }
}