#include <opencv2/opencv.hpp>
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include <iostream>
#include <ctype.h>
#include <ctime>

#include "LKTracker.h"

#define DEBUG = 1

using namespace cv;
using namespace std;
namespace engar {

    LKTracker::LKTracker(float pointErrorThreshold) {
        this->pointErrorThreshold = pointErrorThreshold;
        this->maxError = 0;

        this->termcrit = TermCriteria(TermCriteria::Type::MAX_ITER | TermCriteria::Type::EPS, 50, 0.03);
        this->subPixWinSize = Size(10, 10);
        this->winSize = Size(11, 11);
        this->regenGFTT = false;

        vector<Point2f> obj_corners(4);
        oldCorners[0] = newCorners[0];
        oldCorners[1] = newCorners[1];
        oldCorners[2] = newCorners[2];
        oldCorners[3] = newCorners[3];

//        maxTrans = UMat::zeros(1, 3, CV_64FC1);
    }

    LKTracker::~LKTracker() = default;

    void LKTracker::init(InputOutputArray &frame, vector<Point2f> &prevPoints) {
        if (frame.channels() > 1) {
            cvtColor(frame, prevGrayFrame, COLOR_BGR2GRAY);
        } else {
            frame.copyTo(prevGrayFrame);
        }

        cornerSubPix(prevGrayFrame, prevPoints, subPixWinSize, Size(-1, -1), termcrit);

        maxError = 0;


        ///// KalmanFilter experiment ////////////
//	KF = new KalmanFilter(4, 2, 0);
//	UMat_<float> state(4, 1); // (x, y, Vx, Vy)
//	UMat processNoise(4, 1, CV_32F);
//
//	KF->statePre.at<float>(0) = points[0][0].x;
//	KF->statePre.at<float>(1) = points[0][0].y;
//	KF->statePre.at<float>(2) = 0;
//	KF->statePre.at<float>(3) = 0;
//	KF->transitionUMatrix = *(UMat_<float>(4, 4) << 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
//
//	setIdentity(KF->measurementUMatrix);
//	setIdentity(KF->processNoiseCov, Scalar::all(1e-4));
//	setIdentity(KF->measurementNoiseCov, Scalar::all(1e-1));
//	setIdentity(KF->errorCovPost, Scalar::all(.1));
        //////////////////////////////////////////
    }

    void LKTracker::init(InputOutputArray &frame, Rect &roi, vector<Point2f> &prevPoints) {
//        cout << "LKTracker.init: starts." << endl << std::flush;
        if (frame.channels() > 1) {
            cvtColor(frame, prevGrayFrame, COLOR_BGR2GRAY);
        } else {
            frame.copyTo(prevGrayFrame);
        }

        cv::Mat m(prevGrayFrame.rows, prevGrayFrame.cols, CV_8UC1); //gray
        m.setTo(Scalar(0));
        rectangle(m, roi, white, -1);

        goodFeaturesToTrack(prevGrayFrame, prevPoints, MAX_COUNT, 0.01, 10, m, 3, false, 0.04);
//        cout << "LKTracker.init: gftt." << endl << std::flush;

        cornerSubPix(prevGrayFrame, prevPoints, subPixWinSize, Size(-1, -1), termcrit);
//        cout << "LKTracker.init: csp." << endl << std::flush;

//        maxTrans = UMat::zeros(1, 3, CV_64FC1);
        maxError = 0;

        newCorners[1] = oldCorners[1] = {(float) roi.x, (float) roi.y};
        newCorners[2] = oldCorners[2] = {(float) (roi.x + roi.width), (float) roi.y};
        newCorners[0] = oldCorners[0] = {(float) roi.x, (float) (roi.y - roi.height)};
        newCorners[3] = oldCorners[3] = {(float) (roi.x + roi.width), (float) (roi.y - roi.height)};

//        cout << "LKTracker.init: ends." << endl << std::flush;
    }

    void LKTracker::init(InputOutputArray &frame, vector<Point2f> &roi, vector<Point2f> &prevPoints) {
//        cout << "LKTracker.init: starts." << endl << std::flush;
        if (frame.channels() > 1) {
            cvtColor(frame, prevGrayFrame, COLOR_BGR2GRAY);
        } else {
            prevGrayFrame = frame.getMat();
        }

        Mat mask = Mat(frame.size(), CV_8U, Scalar(0));
        const Point points[4] = {roi[0], roi[1], roi[2], roi[3]};
        cv::fillConvexPoly(mask, points, 4, cv::Scalar(255, 255, 255));

        goodFeaturesToTrack(prevGrayFrame, prevPoints, MAX_COUNT, 0.01, 10, mask, 3, false, 0.04);
//        cout << "LKTracker.init: gftt." << endl << std::flush;

        cornerSubPix(prevGrayFrame, prevPoints, subPixWinSize, Size(-1, -1), termcrit);
//        cout << "LKTracker.init: csp." << endl << std::flush;

        maxError = 0;

        newCorners[1] = oldCorners[1] = roi[0];
        newCorners[2] = oldCorners[2] = roi[1];
        newCorners[0] = oldCorners[0] = roi[2];
        newCorners[3] = oldCorners[3] = roi[3];

//        cout << "LKTracker.init: ends." << endl << std::flush;
    }

    bool LKTracker::track(InputOutputArray &frame, vector<Point2f> &prevPoints, vector<Point2f> &currentPoints, Mat &homography) {
        if (frame.channels() > 1) {
            cvtColor(frame, grayFrame, COLOR_BGR2GRAY);
        } else {
//            frame.copyTo(grayFrame);
            grayFrame = frame.getMat();
        }

        if (!prevPoints.empty()) {
            vector<uchar> status;
            vector<float> err;

//        if (prevGrayFrame.empty())
//            grayFrame.copyTo(prevGrayFrame);

            // Update the points using lucas kanade
            calcOpticalFlowPyrLK(prevGrayFrame, grayFrame, prevPoints, currentPoints, status, err, winSize, 3, termcrit, 0, 0.001);

            // For each point, check if it was detected and prune the ones lost
            size_t i, k;
            for (i = k = 0; i < currentPoints.size(); i++) {

                if (!status[i] || err[i] > this->pointErrorThreshold)
                    continue;

                if (err[i] > maxError) {
                    maxError = err[i];
//                    cout << "Max point tracking error updated: " << err[i] << endl;
                }

                currentPoints[k] = currentPoints[i];
                prevPoints[k] = prevPoints[i];

                k++;
            }
            // Resize the vector to prune lost points.... doesn't seem like an efficient idea, just updating an index should be better
            if (currentPoints.size() != k && prevPoints.size() != k) {
                currentPoints.resize(k);
                prevPoints.resize(k);
//                cout << "\tTracking points dropped: " << currentPoints.size() - k << ". Remaining: " << k << endl;
            }


            // If the tracked points fall bellow the threshold, generate new ones
//        if (regenGFTT && k < this->RegenerationThreshold) {
//            perspectiveTransform(oldCorners, newCorners, homography);
//
//            //-- Update the corners
//            oldCorners[0] = newCorners[0];
//            oldCorners[1] = newCorners[1];
//            oldCorners[2] = newCorners[2];
//            oldCorners[3] = newCorners[3];
//
//            if (prevPoints.size() < RegenerationThreshold) {
//                getNewPoints(grayFrame, RegenerationRate, newPoints, newCorners, currentPoints);
//                prevPoints.insert(prevPoints.end(), newPoints.begin(), newPoints.end());
//                currentPoints.insert(currentPoints.end(), newPoints.begin(), newPoints.end());
//            }
//        }


        }
//    std::swap(currentPoints, prevPoints);
        cv::swap(prevGrayFrame, grayFrame); // Cambiar con punteros
        return true;
    }

    void LKTracker::getNewPoints(UMat &frame, int amount, vector<Point2f> &newPoints, vector<Point2f> &corners, vector<Point2f> &currentPoints) {
        newPoints.clear();

        vector<Point2f> rawPoints;
        float minX = FLT_MAX, minY = FLT_MAX, maxX = 0, maxY = 0;
        for (int i = 0; i < 4; i++) {
            minX = min(minX, corners[i].x);
            minY = min(minY, corners[i].y);
            maxX = max(maxX, corners[i].x);
            maxY = max(maxY, corners[i].y);
        }

        cv::Rect mask(static_cast<int>(minX), static_cast<int>(minY), static_cast<int>(maxX), static_cast<int>(maxY));
        cv::Mat m(grayFrame.rows, grayFrame.cols, CV_8UC1); //gray
        m.setTo(Scalar(0));
        rectangle(m, mask, white, -1);

        imshow("Debug", m);
        //waitKey(0);

        goodFeaturesToTrack(grayFrame, rawPoints, amount, 0.01, 10, m, 3, false, 0.04);
        assert(!rawPoints.empty());
        cornerSubPix(grayFrame, rawPoints, subPixWinSize, Size(-1, -1), termcrit);

        bool isRep = false;
        uint k = 0;
        for (uint i = 0; i < rawPoints.size(); i++) {
            for (uint j = 0; j < currentPoints.size(); j++) {
                double res = cv::norm(rawPoints[i] - currentPoints[j]);
                //cout << res << " - ";
                if (res < 1) {
                    isRep = true;
                    break;
                }
            }
            if (!isRep) {
                newPoints.push_back(rawPoints[i]);
                k++;
            }
            isRep = false;
        }
        //cout << endl << newPoints.size() << endl;
    }
}