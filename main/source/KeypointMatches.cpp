/*
 * KeypointMatches.cpp
 *
 *  Created on: Nov 20, 2014
 *      Author: tigershark
 */

#include "KeypointMatches.h"

using namespace cv;

KeypointMatches::KeypointMatches() {
    this->SceneKeypoints.reset(new vector<KeyPoint>);
    this->GoodMatches.reset(new vector<DMatch>);
}

KeypointMatches::~KeypointMatches() {
    this->SceneKeypoints.release();
    this->GoodMatches.release();
// TODO Auto-generated destructor stub
}

