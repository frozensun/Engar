#define RAPIDJSON_HAS_STDSTRING 1

#include <iostream>
#include <filesystem>
#include <csignal>
#include "OrbDetector.h"
#include "FastDetector.h"
#include "ArbitraryImageEngine.h"
#include "Identity.h"
#include <inferrers/GenderInferrer.h>
#include <inferrers/AgeInferrer.h>
#include <inferrers/FERInferrer.h>
#include "opencv2/core/ocl.hpp"
#include "StaticFrameProvider.hpp"
#include "FaceEngine.hpp"
#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"
#include <NGT/Index.h>
#include "IntegratedAREngine.h"

using namespace std;
using namespace cv;
using namespace engar;

namespace fs = std::filesystem;


IntegratedAREngine::IntegratedAREngine(int width, int height, VideoFrameProvider *fp) {
    w = width;
    h = height;
    frameProvider = fp;

    /// Construimos un tracker
    cout << endl << "Creating Image Tracker" << endl;
    LKTracker *tracker = new LKTracker(trackingPointErrorThreshold);
    ///////////////////////

    /// Construimos un detector
    cout << endl << "Creating Image Detector" << endl;
    shared_ptr<engar::ArDetector> detector = shared_ptr<engar::ArDetector>(new OrbDetector(poiAmount));
    ////////////////////////

    K = Mat(3, 3, CV_64FC1);
    K.at<double>(0, 0) = f;
    K.at<double>(0, 2) = w / 2.0;
    K.at<double>(1, 1) = f;
    K.at<double>(1, 2) = h / 2.0;
    K.at<double>(2, 2) = 1;

    /// Construimos el engine de imagenes
    imageEngine = new ArbitraryImageEngine(frameProvider, detector, tracker, captureWidth, captureHeight, K);
    ////////////////////////

    //// FACE SUB-PROCESS ENGINE ////////////////////////////////////////////////////////
    vector<TrackedFace *> trackedFaces;

    faceEngine = new FaceEngine(frameProvider, captureWidth, captureHeight);

    /// Generamos los descriptores de rostros si hace falta
    //        generateFaceEmbeddings("img/faces", "face_embeddings_dlib.json", *faceEngine->detector.get(), *faceEngine->recognizer.get());

}


void IntegratedAREngine::Train() {
    /// Entrenamos el indice de imagenes
    cout << endl << "Training image engine matcher" << endl;
    imageEngine->Train();
    /////////////////////////////////////////////////////////////////////////////////////

    /// Agregamos -1 como "desconocido"
    cout << endl << "Adding target faces to face engine" << endl;
    Identity unknownIdentity;
    unknownIdentity.Id = -1;
    unknownIdentity.Name = "Desconocido";
    identities[unknownIdentity.Id] = unknownIdentity;

    /// Entrenamos el indice de rostros
    faceEngine->Train();
    ///////////////

    /// Agregamos los inferidores de informacion
    faceEngine->AddInferrer("age", new AgeInferrer("age"));
    faceEngine->AddInferrer("gender", new GenderInferrer("gender"));
    faceEngine->AddInferrer("fer", new FERInferrer("fer"));
    ///////////////
}

void IntegratedAREngine::AddInferrer(Inferrer *inf) {
    faceEngine->AddInferrer(inf->Name, inf);
}

void IntegratedAREngine::ProcessFrame(int &targetId, vector<cv::Point2f> &corners, cv::Mat &homography, cv::Mat &rvec, cv::Mat &tvec,
                                      bool &facesFound, vector<TrackedFace *> &trackedFaces) {
    try {

        Mat frame;
        frameProvider->GrabFrame(frame);

        frameCount++;

        if (frameCount % 2 == 0) {
            imageEngine->ProcessFrame(frame, targetId, corners, homography, rvec, tvec);
        } else {
            facesFound = faceEngine->ProcessFrame(frame, trackedFaces, identities);
        }

    } catch (exception &e) {
        cout << "Standard exception: " << e.what() << endl;
    }

}

void IntegratedAREngine::LoadImagesDirectly(std::vector<string> imagePaths) {
    /// Agregamos las images
    cout << endl << "Adding image targets to image engine" << endl;
    for (const string &target : imagePaths) {
        Mat temp = imread(target, IMREAD_GRAYSCALE);
        imageEngine->AddTarget(temp);
    }
}

void IntegratedAREngine::LoadFaceEmbeddings(string path) {
    /// Cargamos los rostros pre-procesados y entrenamos el indice
    vector<Identity> loadedIdentities = loadFaceEmbeddings(path);

    for (Identity &identity : loadedIdentities) {
        identity.Id = faceEngine->AddFace(identity.Descriptor);
        identities[identity.Id] = identity;
    }
}

void IntegratedAREngine::LoadFacesDirectly(std::vector<string> facesPaths) {
    for (const string &target : facesPaths) {
        UMat temp = imread(target, IMREAD_GRAYSCALE).getUMat(ACCESS_READ);
        faceEngine->AddFaceFromImage(temp);
    }
}

void IntegratedAREngine::LoadImageDescriptors(string path) {

}

vector<Identity> IntegratedAREngine::loadFaceEmbeddings(const string path) {
    string json;
    ifstream file(path);
    rapidjson::Document d;
    rapidjson::IStreamWrapper wrapper(file);
    d.ParseStream(wrapper);
    vector<Identity> loadedIdentities;

    for (rapidjson::Value::ConstValueIterator itr = d.Begin(); itr != d.End(); ++itr) {
        loadedIdentities.emplace_back(*itr);
    }

    file.close();
    return loadedIdentities;
}

void IntegratedAREngine::PrintStatistics() const {
    imageEngine->PrintStatistics();
    faceEngine->PrintStatistics();
}
