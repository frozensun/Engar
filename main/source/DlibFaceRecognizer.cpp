//
// Created by tigershark on 13/9/19.
//

#include <opencv2/core/mat.hpp>
#include "DlibFaceRecognizer.h"

#include <dlib/dnn.h>
#include <dlib/opencv.h>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <matchers/NMSMatcher.h>
#include "matchers/NGTMatcher.h"

namespace engar {

    DlibFaceRecognizer::DlibFaceRecognizer() {
        // We will also use a face landmarking model to align faces to a standard pose:  (see face_landmark_detection_ex.cpp for an introduction)
        dlib::deserialize("models/dlib/shape_predictor_5_face_landmarks.dat") >> sp;
        // And finally we load the DNN responsible for face recognition.
        dlib::deserialize("models/dlib/dlib_face_recognition_resnet_model_v1.dat") >> net;

        similarity::AnyParams *indexParams = new similarity::AnyParams({"efConstruction=2000", "M=10"});
        similarity::AnyParams *searchParams = new similarity::AnyParams({"ef=10"});
        this->matcher = new NMSMatcher<float>("hnsw", "l2", indexParams, searchParams);

//        this->matcher = new cv::BFMatcher(cv::NORM_L2, false);
//        NGT::Property property;
//        property.setDefault();
//        property.dimension = 128;
//        property.distanceType = NGT::Property::DistanceType::DistanceTypeL2;
//        property.indexType = NGT::Property::IndexType::GraphAndTree;
//        this->matcher = NGTMatcher::create(property);

        //int branching = 32, cvflann::flann_centers_init_t centers_init = cvflann::FLANN_CENTERS_RANDOM, int trees = 4, int leaf_size = 100
//        Ptr<flann::IndexParams> params = Ptr<flann::IndexParams>(new flann::LshIndexParams(12, 20, 2));
//    Ptr<flann::IndexParams> params = Ptr<flann::IndexParams>(new flann::HierarchicalClusteringIndexParams);
//        Ptr<flann::SearchParams> sParams = Ptr<flann::SearchParams>(new flann::SearchParams);

//        this->matcher = Ptr<DescriptorMatcher>(new FlannBasedMatcher(params, sParams));


        this->matches.resize(1);
    }

    int DlibFaceRecognizer::Recognize(cv::UMat &colorFrame, cv::Rect2d &face) {
        cv::Mat mat = colorFrame.getMat(cv::ACCESS_READ);
        return Recognize(mat, face);
    }

    int DlibFaceRecognizer::Recognize(dlib::matrix<dlib::rgb_pixel> face_chip) {
        callCount++;

        std::vector<dlib::matrix<dlib::rgb_pixel>> faceChips;
        faceChips.emplace_back(std::move(face_chip));

        // This call asks the DNN to convert each face image in faces into a 128D vector.
        // In this 128D vector space, images from the same person will be close to each other
        // but vectors from different people will be far apart.  So we can use these vectors to
        // identify if a pair of images are from the same person or from different people.
        auto t1 = std::chrono::high_resolution_clock::now();
        std::vector<dlib::matrix<float, 0, 1>> face_descriptors = net(faceChips);
        descriptorTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

        cv::Mat descriptor = dlib::toMat(face_descriptors[0]);
        cv::transpose(descriptor, descriptor);
//        cv::UMat uDescriptor = descriptor.getUMat(cv::ACCESS_READ);

        t1 = std::chrono::high_resolution_clock::now();
        this->matcher->knnMatch(descriptor, matches, 1);
        matchTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

        if (matches.empty()) return -1;

        cv::DMatch match = matches[0][0];

        return match.distance < 0.6 ? match.imgIdx : -1;
    }

    int DlibFaceRecognizer::Recognize(cv::Mat &colorFrame, cv::Rect2d &face) {
        callCount++;
        dlib::cv_image<dlib::bgr_pixel> frame(colorFrame);
        dlib::rectangle faceChip(face.x, face.y, face.x + face.width, face.y + face.height);

        ///  extract a copy that has been normalized to 150x150 pixels in size and appropriately rotated and centered.
        auto t1 = std::chrono::high_resolution_clock::now();
        auto shape = sp(frame, faceChip);
        dlib::matrix<dlib::rgb_pixel> face_chip;
        extract_image_chip(frame, get_face_chip_details(shape, 150, 0.25), face_chip);
        chipTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

        cv::namedWindow("Face chip");
        cv::Mat bgrChip;
        cv::cvtColor(dlib::toMat(face_chip), bgrChip, cv::COLOR_RGB2BGR);
        cv::imshow("Face chip", bgrChip);

        std::vector<dlib::matrix<dlib::rgb_pixel>> faceChips;
        faceChips.emplace_back(std::move(face_chip));

        // This call asks the DNN to convert each face image in faces into a 128D vector.
        // In this 128D vector space, images from the same person will be close to each other
        // but vectors from different people will be far apart.  So we can use these vectors to
        // identify if a pair of images are from the same person or from different people.
        t1 = std::chrono::high_resolution_clock::now();
        std::vector<dlib::matrix<float, 0, 1>> face_descriptors = net(faceChips);
        descriptorTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

        cv::Mat descriptor = dlib::toMat(face_descriptors[0]);
        cv::transpose(descriptor, descriptor);
//        cv::UMat uDescriptor = descriptor.getUMat(cv::ACCESS_READ);

        t1 = std::chrono::high_resolution_clock::now();
        this->matcher->knnMatch(descriptor, matches, 1);
        matchTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);
//        descriptor.release();

        if (matches.empty()) return -1;

        cv::DMatch match = matches[0][0];

        return match.distance < 0.6 ? match.imgIdx : -1;
    }

    std::vector<dlib::matrix<dlib::rgb_pixel>> engar::DlibFaceRecognizer::jitter_image(const dlib::matrix<dlib::rgb_pixel> &img) {
        // All this function does is make 100 copies of img, all slightly jittered by being
        // zoomed, rotated, and translated a little bit differently. They are also randomly
        // mirrored left to right.
        thread_local dlib::rand rnd;

        std::vector<dlib::matrix<dlib::rgb_pixel>> crops;
        for (int i = 0; i < 100; ++i)
            crops.emplace_back(dlib::jitter_image(img, rnd));

        return crops;
    }

    void DlibFaceRecognizer::Train() {
        this->matcher->train();
    }

    int DlibFaceRecognizer::AddFace(const cv::UMat &img, cv::Rect roi) {
        this->matcher->add(DescribeFace(img, roi));

        facesCount++;
        return facesCount;
    }

    cv::Mat DlibFaceRecognizer::DescribeFace(const cv::UMat &img, cv::Rect roi) {
        cv::Mat face = img.getMat(cv::ACCESS_READ);

        cv::namedWindow("Added Face");
        cv::imshow("Added Face", face(roi));

        ///  extract a copy that has been normalized to 150x150 pixels in size and appropriately rotated and centered.
        dlib::cv_image<dlib::bgr_pixel> frame(face);
        auto shape = sp(frame, dlib::rectangle(roi.x, roi.y, roi.x + roi.width, roi.y + roi.height));
        dlib::matrix<dlib::rgb_pixel> face_chip;

        extract_image_chip(frame, get_face_chip_details(shape, 150, 0.25), face_chip);

        cv::namedWindow("Face chip");
        cv::Mat bgrChip;
        cv::cvtColor(dlib::toMat(face_chip), bgrChip, cv::COLOR_RGB2BGR);
        cv::imshow("Face chip", bgrChip);

        /// compute descriptor
        dlib::matrix<float, 0, 1> descriptor = dlib::mean(dlib::mat(net(jitter_image(face_chip))));

        cv::Mat matDescriptor = dlib::toMat(descriptor);
        cv::transpose(matDescriptor, matDescriptor);
        return matDescriptor;
    }

    int DlibFaceRecognizer::AddFace(const cv::Mat &descriptor) {
        this->matcher->add(descriptor);

        facesCount++;
        return facesCount;
    }

}