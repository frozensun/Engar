//
// Created by TigerShark on 17/9/2017.
//

#include "ArDetector.h"
#include <iostream>
#include "ImageBasedEngine.h"


using namespace std;
using namespace cv;
using namespace engar;

namespace engar {


    ImageBasedEngine::ImageBasedEngine(VideoFrameProvider *frameProvider, std::shared_ptr<ArDetector> detector, LKTracker *tracker) {
        this->frameProvider = frameProvider;
        this->detector = detector;
        this->tracker = tracker;


        K = Mat(3, 3, CV_64FC1);
        K.at<double>(0, 0) = f;
        K.at<double>(0, 2) = w / 2;
        K.at<double>(1, 1) = f;
        K.at<double>(1, 2) = h / 2;
        K.at<double>(2, 2) = 1;
    }

    ImageBasedEngine::ImageBasedEngine(VideoFrameProvider *frameProvider, std::shared_ptr<ArDetector> detector, LKTracker *tracker, Mat &k) {
        this->frameProvider = frameProvider;
        this->detector = detector;
        this->tracker = tracker;


        K = k;
    }

    ImageBasedEngine::~ImageBasedEngine() {
        PrintStatistics();

    }

    void ImageBasedEngine::PrintStatistics() const {
        cout << endl << endl << "Image Engine Statistics" << endl;
        cout << "Avg Good Matches: " << GoodMatchesCount / DetectionsCount << endl;
        cout << "  Avg Distance: " << distanceSum / pointsCount << endl;
        cout << "  Avg Min Distance: " << distanceMinSum / DetectionsCount << endl;
        cout << "  Avg Max Distance: " << distanceMaxSum / DetectionsCount << endl;

        cout << fixed << "Avg Frame time: " << allFrameTime.count() / frameCount / 1000 << " ms\n";
        cout << fixed << "Avg FPS: " << 1000000 / (allFrameTime.count() / frameCount) << " fps\n";
        cout << fixed << "Max Frame time: " << maxFrameTime.count() / 1000 << " ms\n";

        cout << fixed << "Detect times: " << endl;
        cout << fixed << "  Avg = " << detectingFrameTime.count() / detectingCount / 1000 << " ms\n";
        cout << fixed << "    Detect = " << detector->detectTime.count() / detectingCount / 1000 << " ms\n";
        cout << fixed << "    Match = " << detector->matchTime.count() / detectingCount / 1000 << " ms\n";
        cout << fixed << "      Homography = " << pf_homografyDetect.count() / detectingCount / 1000 << " ms\n";

        cout << fixed << "Track times: " << endl;
        cout << fixed << "  Avg = " << trackingFrameTime.count() / trackingCount / 1000 << " ms\n";
        cout << fixed << "  Min = " << minTrackingTime.count() / 1000 << " ms\n";
        cout << fixed << "  Max = " << maxTrackingTime.count() / 1000 << " ms\n";

        cout << fixed << "Grab frame: " << pf_grabFrame.count() / frameCount / 1000 << " ms\n";
        cout << fixed << "Copy frame: " << pf_copyFrame.count() / frameCount / 1000 << " ms\n";

        cout << fixed << "Tracker: " << pf_tracker.count() / trackingCount / 1000 << " ms\n";
        cout << fixed << "Homography: " << pf_homografyFind.count() / trackingCount / 1000 << " ms\n";
        cout << fixed << "Outlier clean up: " << pf_homografyInlierCleanUp.count() / trackingCount / 1000 << " ms\n";
        cout << fixed << "Shape checks: " << pf_shapeCheck.count() / trackingCount / 1000 << " ms\n";
        cout << fixed << "Perspective transform: " << pf_transform2DPoints.count() / trackingCount / 1000 << " ms\n";
        cout << fixed << "3D outlier clean up: " << pf_3dOutlierCleanUp.count() / trackingCount / 1000 << " ms\n";
        cout << fixed << "Solve PnP: " << pf_solvePnP.count() / trackingCount / 1000 << " ms\n";
        cout << fixed << "Draws: " << pf_draw.count() / trackingCount / 1000 << " ms\n";
        cout << fixed << "Swaps: " << pf_swaps.count() / trackingCount / 1000 << " ms\n";
        cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
        cout << endl << endl;
    }

    void ImageBasedEngine::ResetStatistics() {
        allFrameTime = std::chrono::microseconds::zero();
        trackingFrameTime = std::chrono::microseconds::zero();
        minTrackingTime = std::chrono::microseconds::max();
        maxTrackingTime = std::chrono::microseconds::zero();
        detectingFrameTime = std::chrono::microseconds::zero();
        maxFrameTime = std::chrono::microseconds::zero();
        frameCount = 0;
        trackingCount = 0;
        detectingCount = 0;
        GoodMatchesCount = 0;
        DetectionsCount = 1;
        frameTime = std::chrono::microseconds::zero();

        pf_tracker = std::chrono::microseconds::zero();
        pf_homografyInlierCleanUp = std::chrono::microseconds::zero();
        pf_shapeCheck = std::chrono::microseconds::zero();
        pf_homografyFind = std::chrono::microseconds::zero();
        pf_homografyDetect = std::chrono::microseconds::zero();
        pf_draw = std::chrono::microseconds::zero();
        pf_3dOutlierCleanUp = std::chrono::microseconds::zero();
        pf_solvePnP = std::chrono::microseconds::zero();
        pf_transform2DPoints = std::chrono::microseconds::zero();
        pf_swaps = std::chrono::microseconds::zero();
        pf_grabFrame = std::chrono::microseconds::zero();
        pf_copyFrame = std::chrono::microseconds::zero();

        distanceSum = 0;
        distanceMinSum = 0;
        distanceMaxSum = 0;
        pointsCount = 0;

        if (detector != nullptr) {
            detector->detectTime = std::chrono::microseconds::zero();
            detector->matchTime = std::chrono::microseconds::zero();
        }
    }


//    bool ImageBasedEngine::ProcessFrame(Mat &homography, Mat &rvec, Mat &tvec, Mat *colorFrame) {
//
//        bool found = false;
//
//        this->frameProvider->GrabFrame(frame);
//
//        if (frame.empty())
//            return false;
//        else if (frame.channels() > 1) {
//            if (colorFrame != NULL) {
//                frame.copyTo(*colorFrame);
//            }
//            cvtColor(frame, frame, CV_BGR2GRAY);
//        }
//
//        if (!tracking) {
//            detectionStep();
//        } else {
//            found = trackingStep(homography, rvec, tvec, colorFrame);
//        }
//        end = clock();
//
//        frameTime = (long) (double(end - begin) / CLOCKS_PER_SEC * 1000);
//        allFrameTime += frameTime;
//        frameCount++;
//        if (frameTime > maxFrameTime) {
//            maxFrameTime = frameTime;
//        }
//
//        if (tracking) {
//            trackingCount++;
//            trackingFrameTime += frameTime;
//        } else {
//            detectingCount++;
//            detectingFrameTime += frameTime;
//        }
//
//        return found;
//    }
//
//    bool ImageBasedEngine::trackingStep(Mat &homography, Mat &rvec, Mat &tvec, Mat *colorFrame) {
//        bool found = false;
//        stillFound = tracker->track(frame);
//
//        if (stillFound) {
//            // Obtenemos homografia para actualizar los extremos del QR
//            tracker->computeHomography(homography);
//
//            // Transformamos los extremos
//            perspectiveTransform(oldCorners, newCorners, homography);
//
//            oldCorners[0] = newCorners[0];
//            oldCorners[1] = newCorners[1];
//            oldCorners[2] = newCorners[2];
//            oldCorners[3] = newCorners[3];
//
//            // Obtenemos rvec y tvec
//            found = solvePnP(dPoints, newCorners, K, noArray(), rvec, tvec, false, SOLVEPNP_P3P);
//
//            if (colorFrame != NULL) {
//                ///-- Draw lines between the corners
//                tracker->PrintTrackingPoints(*colorFrame);
//                Point2f offset(0, 0);
//                line(*colorFrame, newCorners[0] + offset, newCorners[1] + offset, green, 4);
//                line(*colorFrame, newCorners[1] + offset, newCorners[2] + offset, green, 4);
//                line(*colorFrame, newCorners[2] + offset, newCorners[3] + offset, green, 4);
//                line(*colorFrame, newCorners[3] + offset, newCorners[0] + offset, green, 4);
//                ///////////////////////////////////
//
//                if (found) {
//                    Point2f center = Point2f((float) tvec.at<double>(0, 0) + w / 2, (float) tvec.at<double>(1, 0) + h / 2);
//
//                    imgpts.clear();
//                    projectPoints(axis, rvec, tvec, K, noArray(), imgpts);
//
//                    line(*colorFrame, center, imgpts[0], red, 4);
//                    line(*colorFrame, center, imgpts[1], green, 4);
//                    line(*colorFrame, center, imgpts[2], blue, 4);
//                }
//            }
//        } else {
//            tracking = false;
//            cout << "Object lost, reverse to detection..." << endl;
//        }
//        return found;
//    }
//
//    void ImageBasedEngine::detectionStep() {
//        try {
//            Ptr<ArDetectionResult> result(detector->detect(frame));
//
//            if (result->Detected) {
//                tracking = true;
//                cout << "Object found @[(" << result->MinX << ", " << result->MinY << "); (" << result->MaxX
//                     << ", " << result->MaxY << ")], starting tracker..." << endl;
//
//                int offset = 30;
//                oldCorners[0] = *(result->CornerA);
//                oldCorners[1] = *(result->CornerB);
//                oldCorners[2] = *(result->CornerC);
//                oldCorners[3] = *(result->CornerD);
//
//                Rect mask(max(result->MinX - offset, 0), max(result->MinY - offset, 0),
//                          min(2 * offset + result->MaxX - result->MinX, frame.cols),
//                          min(2 * offset + result->MaxY - result->MinY, frame.rows));
//                tracker->init(frame, mask);
//            }
//        } catch (const exception &e) {
//            cout << e.what() << endl;
//        }
//    }


}