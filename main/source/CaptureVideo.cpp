/*
 * CaptureVideo.cpp
 *
 *  Created on: Nov 20, 2014
 *      Author: tigershark
 */

#include <opencv2/core/core.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "CaptureVideo.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace cv;

CaptureVideo::CaptureVideo() {
    //if (!camId.empty())
//		cap.open(camId);
//	else
    this->cap = new VideoCapture;
    this->cap->open(0);

    if (!this->cap->isOpened()) {
        cout << "Could not initialize capturing...\n";
        return;
    }

}

Ptr<Mat> CaptureVideo::getFrame() {
    Ptr<Mat> frame;
    *(this->cap) >> (*frame);
    //if (frame.empty())
    //return noArray();
    cvtColor(*frame, *frame, COLOR_BGR2GRAY);

    return frame;
}

CaptureVideo::~CaptureVideo() {
    this->cap.release();
}

