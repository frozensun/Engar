//
// Created by tigershark on 9/10/19.
//

#include <algorithm>
#include <execution>
#include <utility>
#include <vector>
#include <knnquery.h>
#include <methodfactory.h>
#include <spacefactory.h>
#include <init.h>
#include <pstl/execution_defs.h>
#include "matchers/NMSMatcher.h"
#include "Range.hpp"

namespace engar {
    template<class T>
    NMSMatcher<T>::~NMSMatcher() {
//        std::cout << std::fixed << "knn Search Avg time: " << knnSearchTime.count() / (double) calls / 1000.0 << " ms\n";
//        std::cout << std::fixed << "Query Build Avg time: " << queryBuildTime.count() / (double) calls / 1000.0 << " ms\n";
    }

    template<class T>
    void NMSMatcher<T>::add(const cv::_InputArray &descriptors) {
        needsTrain = true;
        DescriptorMatcher::add(descriptors);
    }

    template<class T>
    void NMSMatcher<T>::train() {

        if (needsTrain) {
            this->space = similarity::SpaceFactoryRegistry<T>::Instance().CreateSpace(this->distanceType, this->indexParams);
            this->index = similarity::MethodFactoryRegistry<T>::Instance().CreateMethod(true, indexType, distanceType, *space, dataset);

            ushort imgIdx = 0;
            for (auto &d : trainDescCollection) {
                appendMat(d, imgIdx);
                imgIdx++;
            }
            for (auto const &ud : utrainDescCollection) {
                cv::Mat d = ud.getMat(cv::ACCESS_READ);
                appendMat(d, imgIdx);
                imgIdx++;
            }

            index->CreateIndex(similarity::AnyParams(indexParams));

            index->SetQueryTimeParams(queryParams);
            needsTrain = false;
        }
    }

    template<class T>
    void NMSMatcher<T>::SetQueryParams(similarity::AnyParams &qParams) {
        index->SetQueryTimeParams(qParams);
    }

    template<class T>
    void NMSMatcher<T>::appendMat(cv::Mat &desc, int imgId) {
        if (desc.isContinuous()) {
            int cols = desc.cols;
            for (int i = 0; i < desc.rows; i++) {
                void *row = desc.ptr<void>(i);
                dataset.emplace_back(new similarity::Object(i, imgId, cols * desc.elemSize(), row));
            }
        }
    }

    template<class T>
    void NMSMatcher<T>::knnMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch>> &matches, int k, cv::InputArray &masks,
                                     bool compactResult) {
        cv::Mat desc = queryDescriptors.getMat();
        std::vector<cv::DMatch> m_base(k, cv::DMatch());
        int cols = desc.cols;

#pragma omp parallel for
        for (int j = 0; j < desc.rows; j++) {

            auto t1 = std::chrono::high_resolution_clock::now();
            void *row = desc.ptr<void>(j);
            similarity::Object *queryObject = new similarity::Object(-1, -1, cols * desc.elemSize(), row);
            similarity::KNNQuery<T> query(*space, queryObject, k);
            queryBuildTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

            t1 = std::chrono::high_resolution_clock::now();
            index->Search(&query);
            knnSearchTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

            delete queryObject;

            similarity::KNNQueue<T> *result = query.Result()->Clone();
            int resultSize = result->Size();
            std::vector<cv::DMatch> match(k, cv::DMatch());

            for (int i = resultSize - 1; i >= 0; i--) {
                cv::DMatch &m = match[i];
                m.distance = sqrt(result->TopDistance());
                const similarity::Object *obj = result->Pop();
                m.queryIdx = j;
                m.trainIdx = obj->id();
                m.imgIdx = obj->label();
            }

            matches[j] = match;
        }
    }

    template<>
    void NMSMatcher<int>::knnMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch>> &matches, int k, cv::InputArray &masks,
                                       bool compactResult) {

        cv::Mat desc = queryDescriptors.getMat();
        std::vector<cv::DMatch> m_base(k, cv::DMatch());

        int cols = desc.cols;

#pragma omp parallel for
        for (int j = 0; j < desc.rows; j++) {

            auto t1 = std::chrono::high_resolution_clock::now();
            void *row = desc.ptr<void>(j);
            similarity::Object *queryObject = new similarity::Object(-1, -1, cols * desc.elemSize(), row);
            similarity::KNNQuery<int> query(*space, queryObject, k);
            queryBuildTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

            t1 = std::chrono::high_resolution_clock::now();
            index->Search(&query);
            knnSearchTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

            delete queryObject;

            similarity::KNNQueue<int> *result = query.Result()->Clone();
            int resultSize = result->Size();
            matches[j] = std::vector<cv::DMatch>(k, cv::DMatch());
            std::vector<cv::DMatch> &match = matches[j];

            for (int i = resultSize - 1; i >= 0; i--) {
                cv::DMatch &m = match[i];
                m.distance = result->TopDistance();
                const similarity::Object *obj = result->Pop();
                m.queryIdx = j;
                m.trainIdx = obj->id();
                m.imgIdx = obj->label();
            }
        }
    }

    template<class T>
    NMSMatcher<T>::NMSMatcher(string type, string distance, similarity::AnyParams *iParams, similarity::AnyParams *qParams) : indexParams(*iParams),
                                                                                                                              queryParams(*qParams),
                                                                                                                              indexType(std::move(type)),
                                                                                                                              distanceType(
                                                                                                                                      std::move(distance)) {
        similarity::initLibrary();
    }

    template<class T>
    bool NMSMatcher<T>::isMaskSupported() const {
        return false;
    }

    template<class T>
    cv::Ptr<cv::DescriptorMatcher> NMSMatcher<T>::clone(bool emptyTrainData) const {
        return cv::Ptr<DescriptorMatcher>();
    }

    template<class T>
    void NMSMatcher<T>::radiusMatchImpl(cv::InputArray &queryDescriptors, std::vector<std::vector<cv::DMatch>> &matches, float maxDistance,
                                        cv::InputArray &masks, bool compactResult) {
        std::cout << "radiusMatch" << std::endl;
    }

    template
    class engar::NMSMatcher<float>;

    template
    class engar::NMSMatcher<int>;
}