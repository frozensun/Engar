//
// Created by tigershark on 9/10/19.
//

#include <vector>
#include "matchers/FLANNMatcher.h"


namespace engar {

    using namespace std;

    template<class ElementType, template<class> class Distance, class ResultType>
    FLANNMatcher<ElementType, Distance, ResultType>::FLANNMatcher(const ::flann::IndexParams &property, const ::flann::SearchParams &searchParams) {
        this->properties = property;
        this->searchParams = searchParams;
        this->index = new ::flann::Index<Distance<ElementType>>(properties);
    }

    template<class ElementType, template<class> class Distance, class ResultType>
    FLANNMatcher<ElementType, Distance, ResultType>::~FLANNMatcher() {
        cout << std::fixed << "knn Search Avg time: " << knnSearchTime.count() / (double) calls / 1000.0 << " ms\n";

        delete[] data.ptr();
        delete index;
    }

    template<class ElementType, template<class> class Distance, class ResultType>
    void FLANNMatcher<ElementType, Distance, ResultType>::add(const cv::_InputArray &descriptors) {
        needsTrain = true;
        descriptorCount += descriptors.rows();
        dimensions = descriptors.cols();
        DescriptorMatcher::add(descriptors);
    }

    template<class ElementType, template<class> class Distance, class ResultType>
    void FLANNMatcher<ElementType, Distance, ResultType>::train() {
        data = ::flann::Matrix<ElementType>(new ElementType[descriptorCount * dimensions], descriptorCount, dimensions);

        if (needsTrain) {
            this->descLimits.emplace_back(0);
            imgIdxs.reserve(descriptorCount);
            ushort imgIdx = 0;
            ulong descIdx = 0;
            ulong descCurrentLimit = 0;
            for (auto const &d : trainDescCollection) {
                appendMat(d, data, descCurrentLimit);

                descCurrentLimit += d.rows;
                for (; descIdx < descCurrentLimit; descIdx++) {
                    imgIdxs.emplace_back(imgIdx);
                }
                imgIdx++;
                this->descLimits.emplace_back(descCurrentLimit);
            }
            for (auto const &ud : utrainDescCollection) {
                cv::Mat d = ud.getMat(cv::ACCESS_READ);
                appendMat(d, data, descCurrentLimit);

                descCurrentLimit += d.rows;
                for (; descIdx < descCurrentLimit; descIdx++) {
                    imgIdxs.emplace_back(imgIdx);
                }
                imgIdx++;
                this->descLimits.emplace_back(descCurrentLimit);
            }

            index->buildIndex(data);

            needsTrain = false;
        }
    }

    template<class ElementType, template<class> class Distance, class ResultType>
    void FLANNMatcher<ElementType, Distance, ResultType>::appendMat(const cv::Mat &d, ::flann::Matrix<ElementType> &data, ulong offset) {
        if (d.isContinuous()) {
            std::size_t bytes = dimensions * sizeof(ElementType);
            for (int i = 0; i < d.rows; i++) {
                memcpy(reinterpret_cast<void *>(data[offset + i]), d.ptr<void>(i), bytes);
            }
        }
    }

    template<class ElementType, template<class> class Distance, class ResultType>
    void
    FLANNMatcher<ElementType, Distance, ResultType>::knnMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch>> &allMatches, int k,
                                                                  cv::InputArray &masks,
                                                                  bool compactResult) {
        calls++;

        cv::Mat asMat = queryDescriptors.getMat();
        ::flann::Matrix<ElementType> query(reinterpret_cast<ElementType *>(asMat.data), asMat.rows, asMat.cols);
        ::flann::Matrix<ResultType> dists(new ResultType[asMat.rows * k], asMat.rows, k);
        ::flann::Matrix<size_t> indices(new size_t[asMat.rows * k], asMat.rows, k);

        index->knnSearch(query, indices, dists, k, searchParams);

        for (int i = 0; i < indices.rows; i++) {
            allMatches[i] = std::vector<cv::DMatch>(k, cv::DMatch());
            std::vector<cv::DMatch> &matches = allMatches[i];

            for (int nn = 0; nn < k; nn++) {
                cv::DMatch &dMatch = matches[nn];
                int id = indices[i][nn];
                int trainIdx = id - descLimits[imgIdxs[id]];
                dMatch.queryIdx = i;
                dMatch.trainIdx = trainIdx;
                dMatch.imgIdx = imgIdxs[id];
                dMatch.distance = dists[i][nn];
            }

        }

        delete[] dists.ptr();
        delete[] indices.ptr();
    }

    template<class ElementType, template<class> class Distance, class ResultType>
    bool FLANNMatcher<ElementType, Distance, ResultType>::isMaskSupported() const {
        return false;
    }

    template<class ElementType, template<class> class Distance, class ResultType>
    cv::Ptr<cv::DescriptorMatcher> FLANNMatcher<ElementType, Distance, ResultType>::clone(bool emptyTrainData) const {
        return cv::Ptr<DescriptorMatcher>();
    }

    template<class ElementType, template<class> class Distance, class ResultType>
    void
    FLANNMatcher<ElementType, Distance, ResultType>::radiusMatchImpl(cv::InputArray &queryDescriptors, std::vector<std::vector<cv::DMatch>> &matches,
                                                                     float maxDistance,
                                                                     cv::InputArray &masks, bool compactResult) {
        cout << "radiusMatch" << endl;
    }

    template
    class engar::FLANNMatcher<float, ::flann::L2, float>;

    template
    class engar::FLANNMatcher<unsigned char, ::flann::HammingPopcnt, int>;

    template
    class engar::FLANNMatcher<unsigned char, ::flann::Hamming, unsigned int>;

    template
    class engar::FLANNMatcher<unsigned long long, ::flann::HammingPopcnt, int>;
}