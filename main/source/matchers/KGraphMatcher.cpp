//
// Created by tigershark on 9/10/19.
//

#include <algorithm>
#include <execution>
#include <utility>
#include <iostream>
#include <vector>

#include <pstl/execution_defs.h>
#include "matchers/KGraphMatcher.h"
#include "Range.hpp"

using namespace std;
using namespace cv;

namespace engar {

    template<class T, class D>
    KGraphMatcher<T, D>::~KGraphMatcher() {
        delete this->oracle;
        delete this->index;
        this->trainData.deallocate();
//        std::cout << std::fixed << "knn Search Avg time: " << knnSearchTime.count() / (double) calls / 1000.0 << " ms\n";
//        std::cout << std::fixed << "Query Build Avg time: " << queryBuildTime.count() / (double) calls / 1000.0 << " ms\n";
    }

    template<class T, class D>
    void KGraphMatcher<T, D>::add(const cv::_InputArray &descriptors) {
        needsTrain = true;
        descriptorCount += descriptors.rows();
        DescriptorMatcher::add(descriptors);
    }

    template<class T, class D>
    void KGraphMatcher<T, D>::train() {

        if (needsTrain) {
            this->descLimits.emplace_back(0);
            imgIdxs.reserve(descriptorCount);
            ushort imgIdx = 0;
            ulong descIdx = 0;
            ulong descCurrentLimit = 0;
            for (auto &d : trainDescCollection) {

                this->trainData.push_back(d);

                descCurrentLimit += d.rows;
                for (; descIdx < descCurrentLimit; descIdx++) {
                    imgIdxs.emplace_back(imgIdx);
                }
                imgIdx++;
                this->descLimits.emplace_back(descCurrentLimit);
            }
            for (auto const &ud : utrainDescCollection) {
                cv::Mat d = ud.getMat(cv::ACCESS_READ);

                this->trainData.push_back(d);

                descCurrentLimit += d.rows;
                for (; descIdx < descCurrentLimit; descIdx++) {
                    imgIdxs.emplace_back(imgIdx);
                }
                imgIdx++;
                this->descLimits.emplace_back(descCurrentLimit);
            }

            kgraph::MatrixProxy<T> proxy(this->trainData);
            this->oracle = new kgraph::MatrixOracle<T, D>(proxy);    // defined as above

            IndexParams.reverse = -1;

            index->build(*(this->oracle), this->IndexParams, &(this->IndexInfo));

            needsTrain = false;
        }
    }

    template<class T, class D>
    void KGraphMatcher<T, D>::knnMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch>> &matches, int k, cv::InputArray &masks,
                                           bool compactResult) {
        cv::Mat desc = queryDescriptors.getMat();
        this->SearchParams.K = k;

#pragma omp parallel for
        for (int j = 0; j < desc.rows; j++) {

            auto t1 = std::chrono::high_resolution_clock::now();
            T *row = desc.ptr<T>(j);
            queryBuildTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

            std::vector<unsigned> result(k);
            std::vector<float> distances(k);

            t1 = std::chrono::high_resolution_clock::now();
            // MyOracle::query(float const *) returns a search oracle
            index->search(this->oracle->query(row), this->SearchParams, &result[0], &distances[0], nullptr);
            knnSearchTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

            matches[j] = std::vector<cv::DMatch>(k, cv::DMatch());
            std::vector<cv::DMatch> &match = matches[j];

            for (int i = 0; i < k; i++) {
                cv::DMatch &m = match[i];
                int id = result[i];
                int trainIdx = id - descLimits[imgIdxs[id]];
                m.queryIdx = j;
                m.trainIdx = trainIdx;
                m.imgIdx = imgIdxs[id];
                m.distance = distances[i];
            }
        }
    }

    template<class T, class D>
    KGraphMatcher<T, D>::KGraphMatcher(int dimensions, kgraph::KGraph::IndexParams &params) {
        this->IndexParams = params;
        this->index = kgraph::KGraph::create();
        this->trainData = Mat(cv::Size(dimensions, 0), CV_32F);
    }

    template<>
    KGraphMatcher<unsigned char, kgraph::metric::hamming>::KGraphMatcher(int dimensions, kgraph::KGraph::IndexParams &params) {
        this->IndexParams = params;
        this->index = kgraph::KGraph::create();
        this->trainData = Mat(cv::Size(dimensions, 0), CV_8U);
    }

    template<class T, class D>
    bool KGraphMatcher<T, D>::isMaskSupported() const {
        return false;
    }

    template<class T, class D>
    cv::Ptr<cv::DescriptorMatcher> KGraphMatcher<T, D>::clone(bool emptyTrainData) const {
        return cv::Ptr<DescriptorMatcher>();
    }

    template<class T, class D>
    void KGraphMatcher<T, D>::radiusMatchImpl(cv::InputArray &queryDescriptors, std::vector<std::vector<cv::DMatch>> &matches, float maxDistance,
                                              cv::InputArray &masks, bool compactResult) {
        std::cout << "radiusMatch" << std::endl;
    }

    template
    class engar::KGraphMatcher<float, kgraph::metric::l2>;

    template
    class engar::KGraphMatcher<float, kgraph::metric::l2sqr>;

    template
    class engar::KGraphMatcher<unsigned char, kgraph::metric::hamming>;

    template
    class engar::KGraphMatcher<unsigned long long, kgraph::metric::hamming>;
}