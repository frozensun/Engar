//
// Created by tigershark on 9/10/19.
//

#include <iostream>
#include <vector>

#include "matchers/AnnoyMatcher.h"

namespace engar {
    template<class T, class D>
    AnnoyMatcher<T, D>::~AnnoyMatcher() {
//        std::cout << std::fixed << "knn Search Avg time: " << knnSearchTime.count() / (double) calls / 1000.0 << " ms\n";
//        std::cout << std::fixed << "Query Build Avg time: " << queryBuildTime.count() / (double) calls / 1000.0 << " ms\n";
    }

    template<class T, class D>
    void AnnoyMatcher<T, D>::add(const cv::_InputArray &descriptors) {
        needsTrain = true;
        descriptorCount += descriptors.rows();
        DescriptorMatcher::add(descriptors);
    }

    template<class T, class D>
    void AnnoyMatcher<T, D>::train() {

        if (needsTrain) {
            this->descLimits.emplace_back(0);
            imgIdxs.reserve(descriptorCount);
            ushort imgIdx = 0;
            ulong descIdx = 0;
            ulong descCurrentLimit = 0;
            for (auto &d : trainDescCollection) {
                appendMat(d);

                descCurrentLimit += d.rows;
                for (; descIdx < descCurrentLimit; descIdx++) {
                    imgIdxs.emplace_back(imgIdx);
                }
                imgIdx++;
                this->descLimits.emplace_back(descCurrentLimit);
            }
            for (auto const &ud : utrainDescCollection) {
                cv::Mat d = ud.getMat(cv::ACCESS_READ);
                appendMat(d);

                descCurrentLimit += d.rows;
                for (; descIdx < descCurrentLimit; descIdx++) {
                    imgIdxs.emplace_back(imgIdx);
                }
                imgIdx++;
                this->descLimits.emplace_back(descCurrentLimit);
            }

            index->build(this->trees);

            needsTrain = false;
        }
    }

    template<class T, class D>
    void AnnoyMatcher<T, D>::appendMat(cv::Mat &desc) {
        if (desc.isContinuous()) {
            for (int i = 0; i < desc.rows; i++) {
                T *row = desc.ptr<T>(i);
                index->add_item(idx, row);
                idx++;
            }
        }
    }

    template<class T, class D>
    void AnnoyMatcher<T, D>::knnMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch>> &matches, int k, cv::InputArray &masks,
                                          bool compactResult) {
        cv::Mat desc = queryDescriptors.getMat();

//#pragma omp parallel for
        for (int j = 0; j < desc.rows; j++) {

            auto t1 = std::chrono::high_resolution_clock::now();
            T *row = desc.ptr<T>(j);
            queryBuildTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

            std::vector<unsigned int> result;
            std::vector<float> distances;

            t1 = std::chrono::high_resolution_clock::now();
            index->get_nns_by_vector(row, k, this->SearchPrecision, &result, &distances);
            knnSearchTime += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);

            matches[j] = std::vector<cv::DMatch>(k, cv::DMatch());
            std::vector<cv::DMatch> &match = matches[j];
//            try to focus dmg on the ones on the sides and reduce action bar for the ones in the middle
            for (int i = 0; i < k; i++) {
                cv::DMatch &m = match[i];
                int id = result[i];
                int trainIdx = id - descLimits[imgIdxs[id]];
                m.queryIdx = j;
                m.trainIdx = trainIdx;
                m.imgIdx = imgIdxs[id];
                m.distance = distances[i];
            }
        }
    }

    template<class T, class D>
    AnnoyMatcher<T, D>::AnnoyMatcher(int dimensions, int trees) {
        this->trees = trees;
        this->index = new annoy::AnnoyIndex<unsigned int, T, D, Kiss32Random>(dimensions);
    }

    template<class T, class D>
    bool AnnoyMatcher<T, D>::isMaskSupported() const {
        return false;
    }

    template<class T, class D>
    cv::Ptr<cv::DescriptorMatcher> AnnoyMatcher<T, D>::clone(bool emptyTrainData) const {
        return cv::Ptr<DescriptorMatcher>();
    }

    template<class T, class D>
    void AnnoyMatcher<T, D>::radiusMatchImpl(cv::InputArray &queryDescriptors, std::vector<std::vector<cv::DMatch>> &matches, float maxDistance,
                                             cv::InputArray &masks, bool compactResult) {
        std::cout << "radiusMatch" << std::endl;
    }

    template
    class engar::AnnoyMatcher<float, annoy::Euclidean>;

    template
    class engar::AnnoyMatcher<unsigned char, annoy::Hamming>;

    template
    class engar::AnnoyMatcher<int, annoy::Hamming>;

    template
    class engar::AnnoyMatcher<unsigned long long, annoy::Hamming>;
}