//
// Created by tigershark on 9/10/19.
//

#include <vector>
#include <iostream>
#include <execution>

#include <eigen3/Eigen/Core>
#include <opencv2/core/eigen.hpp>

#include "matchers/MRPTMatcher.h"
#include "Range.hpp"

using namespace std;
using namespace cv;

namespace engar {


    MRPTMatcher::MRPTMatcher(float targetRecall, int dimensions) {
        this->target_recall = targetRecall;
        this->d = dimensions;
        this->descLimits.emplace_back(0);
        this->trainData = Mat(Size(this->d, 0), CV_32F);
    }

    MRPTMatcher::~MRPTMatcher() {
        cout << std::fixed << "knn Search Avg time: " << knnSearchTime.count() / (double) calls / 1000.0 << " ms\n";
        delete this->index;
    }

    void MRPTMatcher::add(const cv::_InputArray &descriptors) {
        needsTrain = true;
        Mat descriptorsMat = descriptors.getMat();
        descriptorCount += descriptorsMat.rows;

        this->trainData.push_back(descriptorsMat);

        descCurrentLimit += descriptorsMat.rows;
        for (; descIdx < descCurrentLimit; descIdx++) {
            imgIdxs.emplace_back(imgIdx);
        }
        imgIdx++;
        this->descLimits.emplace_back(descCurrentLimit);
    }

    void MRPTMatcher::train() {

        if (needsTrain) {
            Mat t;
            transpose(this->trainData, t);
            cv::cv2eigen(t, this->eigen_mat);

            this->index = new Mrpt(eigen_mat);
            this->index->grow_autotune(target_recall, 2);
            this->autotunedParams = this->index->parameters();
            this->voteThreshold = this->autotunedParams.votes;
            needsTrain = false;
        }
    }

    void MRPTMatcher::knnMatchImpl(InputArray queryDescriptors, vector<vector<DMatch>> &matches, int k, InputArray &masks, bool compactResult) {

        calls++;

        Mat batchQuery = queryDescriptors.getMat();

        std::vector<cv::DMatch> m_base(k, cv::DMatch());

//        static Range::range iter(0, 200, 1);
//        std::for_each(std::execution::par, iter.begin(), iter.end(),
//                      [this, k, &matches, &batchQuery](int queryIdx)
#pragma omp parallel for
        for (int queryIdx = 0; queryIdx < batchQuery.rows; queryIdx++) {
            Mat query;
            transpose(batchQuery.row(queryIdx), query);
            Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> q;

            cv::cv2eigen(query, q);

            int indices[k];
            float distances[k];
            this->index->query(q, 2, this->voteThreshold, indices, distances);

            matches[queryIdx] = std::vector<cv::DMatch>(k, cv::DMatch());
            std::vector<cv::DMatch> &match = matches[queryIdx];

            for (int i = 0; i < k; i++) {
                cv::DMatch &m = match[i];
                int id = indices[i];
                int trainIdx = id - descLimits[imgIdxs[id]];
                m.queryIdx = queryIdx;
                m.trainIdx = trainIdx;
                m.imgIdx = imgIdxs[id];
                m.distance = distances[i];
            }
        }
//                      );
    }

    bool MRPTMatcher::isMaskSupported() const {
        return false;
    }

    cv::Ptr<cv::DescriptorMatcher> MRPTMatcher::clone(bool emptyTrainData) const {
        return cv::Ptr<DescriptorMatcher>();
    }

    void MRPTMatcher::radiusMatchImpl(cv::InputArray &queryDescriptors, std::vector<std::vector<cv::DMatch>> &matches, float maxDistance,
                                      cv::InputArray &masks, bool compactResult) {
        cout << "radiusMatch" << endl;
    }

    bool MRPTMatcher::empty() const {
        return this->trainData.empty();
    }

}