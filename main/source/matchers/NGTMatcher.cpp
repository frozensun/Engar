//
// Created by tigershark on 9/10/19.
//

#include <vector>
#include "NGT/Index.h"
#include "NGT/GraphReconstructor.h"
#include "NGT/GraphOptimizer.h"
#include "matchers/NGTMatcher.h"


namespace engar {

    using namespace std;

    NGTMatcher::NGTMatcher(int dimension, bool buildTree, float searchEpsilon, float searchRadius) {
        this->searchRadius = searchRadius;
        this->searchEpsilon = searchEpsilon;
        properties.setDefault();
        properties.dimension = dimension;
        properties.graphType = NGT::Property::GraphType::GraphTypeANNG;

        index = new NGT::Index(properties);
    }

    NGTMatcher::NGTMatcher(NGT::Property &property, float searchEpsilon, float searchRadius) {
        properties = property;
        this->searchRadius = searchRadius;
        this->searchEpsilon = searchEpsilon;
        index = new NGT::Index(properties);
    }

    NGTMatcher::~NGTMatcher() {
        cout << std::fixed << "knn Search Avg time: " << knnSearchTime.count() / (double) calls / 1000.0 << " ms\n";
        index->close();
    }

    void NGTMatcher::add(const cv::_InputArray &descriptors) {
        needsTrain = true;
        descriptorCount += descriptors.rows();
        DescriptorMatcher::add(descriptors);
    }

    void NGTMatcher::train() {

        if (needsTrain) {
            this->descLimits.emplace_back(0);
            imgIdxs.reserve(descriptorCount);
            ushort imgIdx = 0;
            ulong descIdx = 0;
            ulong descCurrentLimit = 0;
            for (auto const &d : trainDescCollection) {
                appendMat(d);

                descCurrentLimit += d.rows;
                for (; descIdx < descCurrentLimit; descIdx++) {
                    imgIdxs.emplace_back(imgIdx);
                }
                imgIdx++;
                this->descLimits.emplace_back(descCurrentLimit);
            }
            for (auto const &ud : utrainDescCollection) {
                cv::Mat d = ud.getMat(cv::ACCESS_READ);
                appendMat(d);

                descCurrentLimit += d.rows;
                for (; descIdx < descCurrentLimit; descIdx++) {
                    imgIdxs.emplace_back(imgIdx);
                }
                imgIdx++;
                this->descLimits.emplace_back(descCurrentLimit);
            }

            index->createIndex(12);
//            if (this->properties.graphType == NGT::NeighborhoodGraph::GraphTypeANNG) {
//                NGT::GraphOptimizer optimizer;
//                optimizer.numOfOutgoingEdges = properties.outgoingEdge;
//                optimizer.numOfIncomingEdges = properties.incomingEdge;
//                optimizer.execute(*index);
//            }

            needsTrain = false;
        }
    }

    void NGTMatcher::appendMat(const cv::Mat &d) {
        if (d.isContinuous()) {
            if (properties.objectType == NGT::ObjectSpace::Uint8) {
                for (int i = 0; i < d.rows; i++) {
                    const uchar *p = d.ptr<unsigned char>(i);
                    std::vector<unsigned char> obj(p, p + properties.dimension);
                    index->append(obj);
                }
            } else {
                index->append(reinterpret_cast<float *>(d.data), d.rows);
            }
        }
    }

    void NGTMatcher::knnMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch>> &matches, int k, cv::InputArray &masks,
                                  bool compactResult) {
        calls++;
        if (properties.objectType == NGT::Property::ObjectType::Uint8) {
            knnMatchUChar(matches, k, queryDescriptors);
        } else {
            knnMatchFloat(matches, k, queryDescriptors);
        }
    }

    void NGTMatcher::knnMatchFloat(std::vector<std::vector<cv::DMatch>> &allMatches, int k, cv::InputArray &queryDescriptors) const {
        cv::Mat desc = queryDescriptors.getMat();

        // allocate query objects.
        NGT::Object *queryObjects[desc.rows];
        for (ulong i = 0; i < desc.rows; i++) {
            const float *p = desc.ptr<float>(i);
            queryObjects[i] = index->allocateObject(p, properties.dimension);
        }

#pragma omp parallel for
        for (ulong i = 0; i < desc.rows; i++) {
            allMatches[i] = std::vector<cv::DMatch>(k, cv::DMatch());
            std::vector<cv::DMatch> &matches = allMatches[i];

            NGT::ObjectDistances objects;        // a result set.
            NGT::SearchContainer sc(*queryObjects[i]);        // search parametera container.
            sc.setResults(&objects);            // set the result set.
            sc.setSize(k);                // the number of resultant objects.
            sc.setRadius(this->searchRadius);            // search radius.
            sc.setEpsilon(this->searchEpsilon);            // set exploration coefficient.

            index->search(sc);

            ulong r = 0;
            for (NGT::ObjectDistance const &result : sc.getResult()) {
                cv::DMatch &dMatch = matches[r];
                int id = result.id - 1;
                int trainIdx = id - descLimits[imgIdxs[id]];
                dMatch.queryIdx = i;
                dMatch.trainIdx = trainIdx;
                dMatch.imgIdx = imgIdxs[id];
                dMatch.distance = result.distance;
                r++;
            }

            // delete the query object.
            index->deleteObject(queryObjects[i]);
            sc.getResult().clear();
        }
    }

    void NGTMatcher::knnMatchUChar(std::vector<std::vector<cv::DMatch>> &allMatches, int k, cv::InputArray &queryDescriptors) {
        cv::Mat desc = queryDescriptors.getMat();

        // allocate query objects.
        NGT::Object *queryObjects[desc.rows];
        for (ulong i = 0; i < desc.rows; i++) {
            const unsigned char *p = desc.ptr<unsigned char>(i);
            std::vector<unsigned char> obj(p, p + properties.dimension);
            queryObjects[i] = index->allocateObject(obj);
        }

#pragma omp parallel for
        for (ulong i = 0; i < desc.rows; i++) {
            allMatches[i] = std::vector<cv::DMatch>(k, cv::DMatch());
            std::vector<cv::DMatch> &matches = allMatches[i];

            NGT::ObjectDistances objects;        // a result set.
            NGT::SearchContainer sc(*queryObjects[i]);        // search parametera container.
            sc.setResults(&objects);            // set the result set.
            sc.setSize(k);                // the number of resultant objects.
            sc.setRadius(this->searchRadius);            // search radius.
            sc.setEpsilon(this->searchEpsilon);            // set exploration coefficient.

            index->search(sc);

            matches.reserve(sc.getResult().size());
            ulong r = 0;
            for (NGT::ObjectDistance const &result : sc.getResult()) {
                cv::DMatch &dMatch = matches[r];
                int id = result.id - 1;
                int trainIdx = id - descLimits[imgIdxs[id]];
                dMatch.queryIdx = i;
                dMatch.trainIdx = trainIdx;
                dMatch.imgIdx = imgIdxs[id];
                dMatch.distance = result.distance;
                r++;
            }

            // delete the query object.
            index->deleteObject(queryObjects[i]);
            sc.getResult().clear();
        }
    }

    bool NGTMatcher::isMaskSupported() const {
        return false;
    }

    cv::Ptr<cv::DescriptorMatcher> NGTMatcher::clone(bool emptyTrainData) const {
        return cv::Ptr<DescriptorMatcher>();
    }

    void NGTMatcher::radiusMatchImpl(cv::InputArray &queryDescriptors, std::vector<std::vector<cv::DMatch>> &matches, float maxDistance,
                                     cv::InputArray &masks, bool compactResult) {
        cout << "radiusMatch" << endl;
    }

    NGTMatcher *NGTMatcher::create(int dimension, bool buildTree) {
        return new NGTMatcher(dimension, buildTree);
    }

    NGTMatcher *NGTMatcher::create(NGT::Property &property) {
        return new NGTMatcher(property);
    }

}