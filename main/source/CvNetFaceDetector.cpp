//2
// Created by tigershark on 12/9/19.
//

#include <opencv2/dnn/dnn.hpp>
#include <opencv2/imgproc.hpp>
#include "CvNetFaceDetector.h"

using namespace cv;
using namespace cv::dnn;

namespace engar {

    CvNetFaceDetector::CvNetFaceDetector(int w, int h, float confidenceThreshold) {
        net = make_unique<Net>(cv::dnn::readNetFromCaffe(caffeConfigFile, caffeWeightFile));
//        net->setPreferableTarget(DNN_TARGET_CPU);
        net->setPreferableBackend(DNN_BACKEND_VKCOM);
        net->setPreferableTarget(DNN_TARGET_VULKAN);
        this->width = w;
        this->height = h;
        this->confidenceThreshold = confidenceThreshold;
    }

    void engar::CvNetFaceDetector::detect(const UMat &colorFrame, vector<Rect> &faces) {
        Mat colorMini;
        resize(colorFrame, colorMini, insize, 0, 0, cv::INTER_LINEAR_EXACT);

        UMat inputBlob = cv::dnn::blobFromImage(colorMini, 1.0, insize, meanColor).getUMat(ACCESS_READ, USAGE_ALLOCATE_DEVICE_MEMORY);
        net->setInput(inputBlob, inputName);
        cv::Mat detection = net->forward(outputName);

        cv::Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());

        faces.clear();

        for (int i = 0; i < detectionMat.rows; i++) {
            float confidence = detectionMat.at<float>(i, 2);

            if (confidence > confidenceThreshold) {
                int x1 = static_cast<int>(detectionMat.at<float>(i, 3) * colorFrame.cols);
                int y1 = static_cast<int>(detectionMat.at<float>(i, 4) * colorFrame.rows);
                int x2 = static_cast<int>(detectionMat.at<float>(i, 5) * colorFrame.cols);
                int y2 = static_cast<int>(detectionMat.at<float>(i, 6) * colorFrame.rows);
                Rect rect = Rect(cv::Point(x1, y1), cv::Point(x2, y2));

                if (rect.area() > 1) {
                    faces.emplace_back(move(rect));
                }
            }
        }
    }

    void CvNetFaceDetector::detect(const _InputArray &colorFrame, vector<cv::Rect> &faces) {
        Mat colorMini;
        resize(colorFrame, colorMini, insize, 0, 0, cv::INTER_LINEAR_EXACT);

        Mat inputBlob = cv::dnn::blobFromImage(colorMini, 1.0, insize, meanColor);
        net->setInput(inputBlob, inputName);
        cv::Mat detection = net->forward(outputName);

        cv::Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());

        faces.clear();

        for (int i = 0; i < detectionMat.rows; i++) {
            float confidence = detectionMat.at<float>(i, 2);

            if (confidence > confidenceThreshold) {
                int x1 = static_cast<int>(detectionMat.at<float>(i, 3) * colorFrame.cols());
                int y1 = static_cast<int>(detectionMat.at<float>(i, 4) * colorFrame.rows());
                int x2 = static_cast<int>(detectionMat.at<float>(i, 5) * colorFrame.cols());
                int y2 = static_cast<int>(detectionMat.at<float>(i, 6) * colorFrame.rows());
                Rect rect = Rect(cv::Point(x1, y1), cv::Point(x2, y2));

                if (rect.area() > 1) {
                    faces.emplace_back(move(rect));
                }
            }
        }
    }

}