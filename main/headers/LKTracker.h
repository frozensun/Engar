/*
 * LKTracker.h
 *
 *  Created on: Nov 22, 2014
 *      Author: tigershark
 */

#ifndef LKTRACKER_H_
#define LKTRACKER_H_

#include <vector>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "PointAndIndex.h"

using namespace std;

namespace engar {
    class LKTracker {
    public:
        explicit LKTracker(float);

        virtual ~LKTracker();

        // Use provided keypoints
        void init(cv::InputOutputArray &, vector<cv::Point2f> &);

        // Calculate own keypoints from roi
        void init(cv::InputOutputArray &, cv::Rect &, vector<cv::Point2f> &);

// Calculate own keypoints from roi
        void init(cv::InputOutputArray &frame, vector<cv::Point2f> &roi, vector<cv::Point2f> &prevPoints);

        bool track(cv::InputOutputArray &, vector<cv::Point2f> &, vector<cv::Point2f> &, cv::Mat &);

//    Point3f mark;
//        UMat maxTrans;

        vector<cv::UMat> rotations;
        vector<cv::UMat> translations;
        vector<cv::UMat> normals;
        const double f = 621.53903021141821, w = 640, h = 480;

        const cv::Scalar green = cv::Scalar(0, 255, 0);
        const cv::Scalar white = cv::Scalar(255, 255, 255);


//    void computeHomography(UMat &UMat);

//    bool computePnP(UMat &rvec, UMat &tvec, UMat &k, int pnpFlag);


    private:
//    vector<Point2f> points[2];
        vector<cv::Point2f> newPoints;
        cv::TermCriteria termcrit;
        cv::Size subPixWinSize, winSize;
        cv::Mat homography;
//        HomographyHelper H;

        static const int MAX_COUNT = 50;
        static const int RegenerationThreshold = 70;
        static const int RegenerationRate = 20;
        static const int LostThreshold = 20;

        cv::Mat prevGrayFrame, grayFrame, image;
        vector<cv::Point2f> oldCorners{{0., 0.},
                                       {0., 0.}};
        vector<cv::Point2f> newCorners{{0., 0.},
                                       {0., 0.}};

        float maxError;
        float pointErrorThreshold;
        bool regenGFTT;

//    Ptr<KalmanFilter> KF;

        void getNewPoints(cv::UMat &, int, vector<cv::Point2f> &, vector<cv::Point2f> &, vector<cv::Point2f> &);

    };
}
#endif /* LKTRACKER_H_ */
