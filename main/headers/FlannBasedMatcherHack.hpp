//
// Created by tigershark on 2/9/19.
//

#ifndef ENGAR_FLANNBASEDMATCHERHACK_HPP
#define ENGAR_FLANNBASEDMATCHERHACK_HPP

#include "opencv2/features2d.hpp"


namespace engar {
    class FlannBasedMatcherHack : public cv::FlannBasedMatcher {
    public:
        cvflann::flann_distance_t distType;

        FlannBasedMatcherHack(const cv::Ptr<cv::flann::IndexParams> &indexParams,
                              const cv::Ptr<cv::flann::SearchParams> &searchParams,
                              cvflann::flann_distance_t _distType)
                : cv::FlannBasedMatcher(indexParams, searchParams) {
            this->distType = _distType;
        }

        void setIndex(const cv::Ptr<cv::flann::Index> &_flannIndex) {
            this->flannIndex = _flannIndex;
        }

        cv::Ptr<cv::flann::Index> getIndex() {
            return this->flannIndex;
        }

        void train() override {
            if (!flannIndex || mergedDescriptors.size() < addedDescCount) {
                // FIXIT: Workaround for 'utrainDescCollection' issue (PR #2142)
                if (!utrainDescCollection.empty()) {
                    CV_Assert(trainDescCollection.empty());
                    for (auto &i : utrainDescCollection)
                        trainDescCollection.push_back(i.getMat(cv::ACCESS_READ));
                }
                mergedDescriptors.set(trainDescCollection);
                flannIndex = cv::makePtr<cv::flann::Index>(mergedDescriptors.getDescriptors(), *indexParams, this->distType);
            }
        }


    };
}

#endif //ENGAR_FLANNBASEDMATCHERHACK_HPP
