//
// Created by TigerShark on 17/9/2017.
//

#ifndef ENGAR_IMAGEBASEDENGINE_H
#define ENGAR_IMAGEBASEDENGINE_H

#include <ratio>
#include <chrono>
#include "VideoFrameProvider.h"
#include "LKTracker.h"
#include "ArDetector.h"

namespace engar {

    /**
     * Base abstracta para motores implementando sub-procesos basados en imagenes.
     * Puede funcionar independientemente del motor integrado de ser necesario.
     */
    class ImageBasedEngine {

    public:

        cv::Mat K;

        enum State {
            TARGET_DETECTION,
            TARGET_TRACKING,
        };

        ImageBasedEngine(VideoFrameProvider *frameProvider, std::shared_ptr<ArDetector> detector, LKTracker *tracker);

        ImageBasedEngine(VideoFrameProvider *frameProvider, std::shared_ptr<ArDetector> detector, LKTracker *tracker, cv::Mat &k);

        virtual void ProcessFrame(int &targetId, vector<cv::Point2f> &corners, cv::Mat &homography, cv::Mat &rvec, cv::Mat &tvec) = 0;

        virtual void ProcessFrame(cv::InputArray frame, int &targetId, vector<cv::Point2f> &corners, cv::Mat &homography, cv::Mat &rvec, cv::Mat &tvec) = 0;

        virtual void AddTarget(cv::Mat &target) = 0;

        /**
         * Compute any pre-processes on the targets.
         */
        virtual void Train() = 0;

        virtual cv::Mat getRawFrame() const = 0;


        std::shared_ptr<ArDetector> detector;

        State state;

        void PrintStatistics() const;

        void ResetStatistics();

        void ProjectPoint(std::vector<cv::Point2f> &points);

        ~ImageBasedEngine();

    protected:
        VideoFrameProvider *frameProvider;
        LKTracker *tracker;

        bool stillFound = false;

        cv::Mat frame; //, colorFrame;
        cv::Mat homography;

        const double f = 621.53903021141821;
        const float w = 640, h = 480;

        //        vector<PointAndIndex> newPoints;

//        vector<cv::Point2f> oldPoints;
//        const cv::Point2f center = {w / 2, h / 2};

//        vector<cv::Point2f> inliers;
//        vector<cv::Point3f> dPoints{
//                cv::Point3f(-64, -64, 0),
//                cv::Point3f(-64, 64, 0),
//                cv::Point3f(64, 64, 0),
//                cv::Point3f(64, -64, 0)
//        };

        virtual void detectionStep(cv::Mat &homography, cv::Mat &rvec, cv::Mat &tvec, vector<cv::Point2f> &corners) = 0;

        virtual void trackingStep(cv::Mat &homography, cv::Mat &rvec, cv::Mat &tvec, vector<cv::Point2f> &corners) = 0;

        float frameCount = 0;
        float trackingCount = 0;
        float detectingCount = 0;
        unsigned long long GoodMatchesCount = 0;
        unsigned long long DetectionsCount = 1;

        float distanceSum = 0;
        float distanceMinSum = 0;
        float distanceMaxSum = 0;
        unsigned long long pointsCount = 0;

        std::chrono::microseconds allFrameTime = std::chrono::microseconds::zero();
        std::chrono::microseconds trackingFrameTime = std::chrono::microseconds::zero();
        std::chrono::microseconds minTrackingTime = std::chrono::microseconds::max();
        std::chrono::microseconds maxTrackingTime = std::chrono::microseconds::zero();
        std::chrono::microseconds detectingFrameTime = std::chrono::microseconds::zero();
        std::chrono::microseconds maxFrameTime = std::chrono::microseconds::zero();

        std::chrono::microseconds frameTime = std::chrono::microseconds::zero();

        std::chrono::microseconds pf_tracker = std::chrono::microseconds::zero();
        std::chrono::microseconds pf_homografyInlierCleanUp = std::chrono::microseconds::zero();
        std::chrono::microseconds pf_shapeCheck = std::chrono::microseconds::zero();
        std::chrono::microseconds pf_homografyFind = std::chrono::microseconds::zero();
        std::chrono::microseconds pf_homografyDetect = std::chrono::microseconds::zero();
        std::chrono::microseconds pf_draw = std::chrono::microseconds::zero();
        std::chrono::microseconds pf_3dOutlierCleanUp = std::chrono::microseconds::zero();
        std::chrono::microseconds pf_solvePnP = std::chrono::microseconds::zero();
        std::chrono::microseconds pf_transform2DPoints = std::chrono::microseconds::zero();
        std::chrono::microseconds pf_swaps = std::chrono::microseconds::zero();
        std::chrono::microseconds pf_grabFrame = std::chrono::microseconds::zero();
        std::chrono::microseconds pf_copyFrame = std::chrono::microseconds::zero();
    };

}
#endif //ENGAR_IMAGEBASEDENGINE_H
