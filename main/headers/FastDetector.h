////
//// Created by TigerShark on 9/10/2017.
////

#ifndef ENGAR_AKAZEDETECTOR_H
#define ENGAR_AKAZEDETECTOR_H

#include "ArDetector.h"
#include <opencv2/core/core.hpp>
#include <flann/flann.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>
#include <vector>
#include <params.h>

#include "KeypointMatches.h"

namespace engar {

    class FastDetector : public ArDetector {

    public:

        explicit FastDetector(int nFeatures);

        cv::Ptr<ArDetectionResult> detect(cv::InputArray &frame) override;

        cv::Ptr<ArDetectionResult> detect(cv::InputArray &frame, const cv::OutputArray &descriptors_object) override;

        void computeKeyPoints(cv::Mat &objImage, vector<cv::KeyPoint> &keyPoints, cv::UMat &descriptors) override;

        void computeKeyPoints(cv::Mat &objImage, vector<cv::KeyPoint> &keyPoints, cv::Mat &descriptors) override;

        ::flann::SearchParams flannSearchParams;

        float ngtSearchEpsilon = 0.1;

        similarity::AnyParams *hnswSearchParams;

        cv::Ptr<cv::flann::IndexParams> indexParams;
        cv::Ptr<cv::flann::SearchParams> searchParams;

    private:
        int matchCount = 0;

        cv::Ptr<engar::ArDetectionResult> result;

        cv::Ptr<cv::Feature2D> detector;

        vector<vector<cv::DMatch> > matches;

//        Ptr<xfeatures2d::SIFT> descriptor;
    };

}
#endif //ENGAR_AKAZEDETECTOR_H
