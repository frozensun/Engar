/*
 * CaptureVideo.h
 *
 *  Created on: Nov 20, 2014
 *      Author: tigershark
 */

#ifndef CAPTUREVIDEO_H_
#define CAPTUREVIDEO_H_

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <string>

class CaptureVideo {
public:
    CaptureVideo();

    virtual ~CaptureVideo();

    cv::Ptr<cv::Mat> getFrame();

private:
    cv::Ptr<cv::VideoCapture> cap;
};

#endif /* CAPTUREVIDEO_H_ */
