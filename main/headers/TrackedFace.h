//
// Created by tigershark on 21/9/19.
//

#ifndef ENGAR_TRACKEDFACE_H
#define ENGAR_TRACKEDFACE_H

#include <iostream>
#include <string>
#include <ctime>
#include <thread>
#include <mutex>
#include <future>
#include <condition_variable>
#include <opencv2/core.hpp>
#include <dlib/matrix.h>
#include <opencv2/tracking/tracker.hpp>
#include "Identity.h"

namespace engar {

    class TrackedFace {
    private:
        std::unique_ptr<std::thread> t;
        std::condition_variable lock;
        bool stop = false;
        std::unique_ptr<std::promise<bool>> resultPromise;
        cv::Ptr<cv::Tracker> tracker = nullptr;
        cv::Mat workFrame;

    public:
        engar::Identity CurrentIdentity;
        cv::Rect2d Bbox;

        std::map<std::string, std::vector<std::pair<std::string, float>>> InferredInfo;

        bool Active = false;

        ulong FoundFrame = 0;
        ulong RecognitionAttemptFrame = 0;
        ulong InferenceAttemptFrame = 0;

        dlib::matrix<dlib::rgb_pixel> DlibFaceChip;
        cv::Mat OpencvFaceChip;

        explicit TrackedFace(cv::Rect2d &bbox);

        TrackedFace() = default;

        TrackedFace(const TrackedFace &) = delete;              // no copying!
        TrackedFace &operator=(const TrackedFace &) = delete; // no copying!

        ~TrackedFace();

        void Init();

        void ResetTracker(cv::InputArray workFrame, cv::Rect2d faceBBox);

        void ResetTracker(cv::Mat workFrame, cv::Rect2d faceBBox);

        std::future<bool> Track(cv::Mat &f);

        void Reset();

    };

}
#endif //ENGAR_TRACKEDFACE_H
