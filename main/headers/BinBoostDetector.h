//
// Created by TigerShark on 9/10/2017.
//

#ifndef ENGAR_BINBOOSTDETECTOR_H
#define ENGAR_BINBOOSTDETECTOR_H

#include "ArDetector.h"
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <vector>

#include "KeypointMatches.h"
#include "binboost/BoostDesc.h"

namespace engar {

    class BinBoostDetector : public ArDetector {

    public:

        BinBoostDetector();

        cv::Ptr<ArDetectionResult> detect(cv::InputArray &frame) override;

        cv::Ptr<ArDetectionResult> detect(cv::InputArray &frame, const cv::OutputArray &descriptors_object) override;

        void computeKeyPoints(Mat &objImage, vector<KeyPoint> &keyPoints, UMat &descriptors) override;

    private:
        int nfeatures = 200;
        float scaleFactor = 1.2f;
        int nlevels = 1;
        int edgeThreshold = 16;
        int firstLevel = 0;
        int WTA_K = 2;
        int patchSize = 16;

        int matchCount = 0;

        UMat tempFrame;

        Mat descriptors_object;

        Ptr<Feature2D> detector;
        unique_ptr<boostDesc::BinBoost> extractor;
        Ptr<DescriptorMatcher> matcher;
        vector<KeyPoint> filteredKps;
    };

}
#endif //ENGAR_ORBDETECTOR_H
