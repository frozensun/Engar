#ifndef ENGAR_RANDOMFRAMEPROVIDER_H
#define ENGAR_RANDOMFRAMEPROVIDER_H

#include <opencv2/videoio.hpp>
#include "VideoFrameProvider.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <random>
#include <opencv2/imgproc.hpp>

namespace engar {

    class RandomFrameProvider : public VideoFrameProvider {

    private:
        int maxTargetId = 0;
        int framesPerTarget = 10;

        std::random_device dev;

        std::mt19937 rng;

        std::uniform_int_distribution<> random;

        const std::vector<std::string> &targetNames;

        cv::Size size;

        int angleCount = 0;
    public:

        int currentTargetId = -1;


        explicit RandomFrameProvider(const std::vector<std::string> &targetNames, int framesPerTarget, const cv::Size &size = cv::Size(640, 480),
                                     bool rotate = false, bool blackInterpolation = false) : targetNames(targetNames) {
            this->size = size;
            maxTargetId = targetNames.size();
            rng = std::mt19937(dev());
            random = std::uniform_int_distribution<>(0, maxTargetId);

            this->framesPerTarget = framesPerTarget;


            this->rotate = rotate;
            this->blackInterpolation = blackInterpolation;

            LoadRandomTarget();
        }

        ~RandomFrameProvider() {
            this->frame.release();
            this->uFrame.release();
        }

        string type2str(int type) {
            string r;

            uchar depth = type & CV_MAT_DEPTH_MASK;
            uchar chans = 1 + (type >> CV_CN_SHIFT);

            switch (depth) {
                case CV_8U:
                    r = "8U";
                    break;
                case CV_8S:
                    r = "8S";
                    break;
                case CV_16U:
                    r = "16U";
                    break;
                case CV_16S:
                    r = "16S";
                    break;
                case CV_32S:
                    r = "32S";
                    break;
                case CV_32F:
                    r = "32F";
                    break;
                case CV_64F:
                    r = "64F";
                    break;
                default:
                    r = "User";
                    break;
            }

            r += "C";
            r += (chans + '0');

            return r;
        }

        void LoadRandomTarget() {
            currentTargetId = random(rng);
            this->frame = cv::Mat::zeros(this->size, CV_8UC3);

            cv::Mat temp = imread(targetNames[currentTargetId], cv::IMREAD_COLOR);
            cout << type2str(temp.type());

//            cv::resize(temp, this->frame, size, 0, 0, cv::INTER_LINEAR_EXACT);
            temp.copyTo(frame(cv::Rect(this->size.width / 2 - temp.cols / 2, this->size.height / 2 - temp.rows / 2, temp.cols, temp.rows)));
//            temp.copyTo(this->frame);
            this->uFrame = frame.getUMat(cv::ACCESS_READ, cv::USAGE_ALLOCATE_HOST_MEMORY);
            angleCount = 0;
        }

        void GrabFrame(cv::UMat &out) override {
            if (frameCount % framesPerTarget == 0) {
                LoadRandomTarget();
            }

            if (blackInterpolation && (frameCount % 100 == 0 || frameCount % 99 == 0 || frameCount % 98 == 0 || frameCount % 97 == 0)) {
                out = cv::UMat(this->uFrame.size(), this->uFrame.type());
                cv::randn(out, mean, sigma);
            } else {
                if (rotate) {
                    ulong angle = angleCount % 360;
                    cv::Mat matRotation = getRotationMatrix2D(cv::Point(frame.cols / 2, frame.rows / 2), angle, 1);
                    warpAffine(uFrame, out, matRotation, frame.size());
                } else {
                    uFrame.copyTo(out);
                }
            }
            frameCount++;
            angleCount++;
        }

        void GrabFrame(cv::Mat &out) override {
            if (frameCount % framesPerTarget == 0) {
                LoadRandomTarget();
            }

            if (blackInterpolation && (frameCount % 50 == 0 || frameCount % 49 == 0 || frameCount % 51 == 0)) {
                out = cv::Mat(this->uFrame.size(), this->uFrame.type());
                cv::randn(out, mean, sigma);
            } else {
                if (rotate) {
                    ulong angle = angleCount % 360;
                    cv::Mat matRotation = getRotationMatrix2D(cv::Point(frame.cols / 2, frame.rows / 2), angle, 1);
                    warpAffine(uFrame, out, matRotation, frame.size());
                } else {
                    uFrame.copyTo(out);
                }
            }
            angleCount++;
            frameCount++;
        }

    private:
        cv::UMat uFrame;
        cv::Mat frame;
        ulong frameCount = 45;
        bool rotate;
        bool blackInterpolation;

        cv::Mat mean = cv::Mat::zeros(1, 1, CV_64FC1);
        cv::Mat sigma = cv::Mat::ones(1, 1, CV_64FC1);
    };


}
#endif //ENGAR_RANDOMFRAMEPROVIDER_H


