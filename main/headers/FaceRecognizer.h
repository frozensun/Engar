//
// Created by tigershark on 13/9/19.
//

#ifndef ENGAR_FACERECOGNIZER_H
#define ENGAR_FACERECOGNIZER_H

#include <chrono>
#include "opencv2/core/types.hpp"
#include <iostream>
#include <dlib/matrix.h>
#include <dlib/pixel.h>

namespace engar {

    /**
     * Interface para reconocedores de rostros.
     */
    class FaceRecognizer {
    public:
        virtual int Recognize(cv::UMat &colorFrame, cv::Rect2d &face) = 0;

        virtual int Recognize(cv::Mat &colorFrame, cv::Rect2d &face) = 0;

        virtual int Recognize(dlib::matrix<dlib::rgb_pixel> face_chip) = 0;

        virtual int AddFace(const cv::UMat &img, cv::Rect roi) = 0;

        virtual cv::Mat DescribeFace(const cv::UMat &img, cv::Rect roi) = 0;

        virtual int AddFace(const cv::Mat &descriptor) = 0;

        virtual void Train() = 0;

        std::chrono::microseconds chipTime = std::chrono::microseconds::zero();
        std::chrono::microseconds descriptorTime = std::chrono::microseconds::zero();
        std::chrono::microseconds matchTime = std::chrono::microseconds::zero();
        ulong callCount = 1;

        ~FaceRecognizer() {
            std::cout << "\n\nFaceRecognizer\n";
            std::cout << std::fixed << "Avg Chip time: " << chipTime.count() / callCount / 1000 << " ms\n";
            std::cout << std::fixed << "Avg Describe time: " << descriptorTime.count() / callCount / 1000 << " ms\n";
            std::cout << std::fixed << "Avg Match time: " << matchTime.count() / callCount / 1000 << " ms\n";
        }

    protected:

        std::vector<std::vector<cv::DMatch> > matches;
    };

}
#endif //ENGAR_FACERECOGNIZER_H
