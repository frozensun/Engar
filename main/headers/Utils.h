/*
 * Utils.h
 *
 *  Created on: Nov 18, 2014
 *      Author: tigershark
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <iostream>
#include <algorithm>
#include <ctime>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include "HomographyHelper.h"
#include "BasicColors.h"

using namespace std;

namespace engar {
//
//
//    auto measure = [](double &accumulator, auto &&function, auto &&... parameters) -> decltype(auto) {
//        const std::chrono::steady_clock::time_point startTimePoint = std::chrono::steady_clock::now();
//
//        decltype(auto) returnValue = std::forward<decltype(function)>(function)(std::forward<decltype(parameters)>(parameters)...);
//
//        accumulator += std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::steady_clock::now() - startTimePoint).count();
//
//        return returnValue;
//    };


    class Utils {
    public:

        static void calculateMaxAndMinDistance(const vector<vector<cv::DMatch> > &, double, double);

        static bool doIntersect(const cv::Point2f &, const cv::Point2f &, const cv::Point2f &, const cv::Point2f &);

        static void PrintTrackingPoints(cv::InputOutputArray &colorFrame, const vector<cv::Point2f> &points);

        static void PrintTrackingPoints(cv::InputOutputArray &colorFrame, const vector<cv::KeyPoint> &points);

        static void PrintTrackingPoints(cv::InputOutputArray &colorFrame, const vector<cv::Point2f> &points, const cv::Scalar &color);

        static void PrintEnclosingLines(const cv::InputOutputArray &colorFrame, const cv::Point2f &offset, const vector<cv::Point2f> &corners);

        static void Print3DAxis(const cv::InputOutputArray &colorFrame, const cv::Point2f &center, const vector<cv::Point2f> &projectedAxes);

        static BasicColors Colors;

//        template<class Obj, typename Func, typename ...Args>
//        void f(Obj &instance, const Func *f, Args &&...args) {
//            (instance).*f(std::forward<Args>(args)...);
//        }
//
//        template<typename F>
//        static double f(F &lambda) {
//            double gf = (double) getTickCount();
//            lambda();
//            return ((double) getTickCount() - gf) * 1000 / getTickFrequency();
//        }
//
//        template<typename F>
//        static double f(F lambda) {
//            double gf = (double) getTickCount();
//            lambda();
//            return ((double) getTickCount() - gf) * 1000 / getTickFrequency();
//        }

//        template<typename F>
//        static double f(F &lambda) {
//            double gf = (double) getTickCount();
//            lambda();
//            return ((double) getTickCount() - gf) * 1000 / getTickFrequency();
//        }



    private:

        static bool onSegment(cv::Point2f, cv::Point2f, cv::Point2f);

        static int orientation(const cv::Point2f &, const cv::Point2f &, const cv::Point2f &);
    };
}
#endif /* UTILS_H_ */
