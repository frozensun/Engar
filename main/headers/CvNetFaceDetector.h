//
// Created by tigershark on 12/9/19.
//

#ifndef ENGAR_CVNETFACEDETECTOR_H
#define ENGAR_CVNETFACEDETECTOR_H

#include <opencv2/dnn.hpp>
#include "FaceDetector.h"

namespace engar {
    class CvNetFaceDetector : public FaceDetector {
    public:
        CvNetFaceDetector(int w, int h, float confidenceThreshold = 0.98);

        void detect(const cv::InputArray &colorFrame, vector<cv::Rect> &faces) override;

        void detect(const cv::UMat &colorFrame, vector<cv::Rect> &faces) override;

    private:
        int width;
        int height;
        float confidenceThreshold = 0.98;

        const std::string caffeConfigFile = "models/deploy.prototxt";
        const std::string caffeWeightFile = "models/opencv_face_detector_fp16.caffemodel";
        const std::string outputName = "detection_out";
        const std::string inputName = "data";
        const cv::Size insize = cv::Size(300, 300);
        const cv::Scalar meanColor = cv::Scalar(104, 117, 123);

        unique_ptr<cv::dnn::Net> net;
    };

}
#endif //ENGAR_CVNETFACEDETECTOR_H
