//
// Created by TigerShark on 9/10/2017.
//

#ifndef ENGAR_ARBITRARYIMAGEENGINE_H
#define ENGAR_ARBITRARYIMAGEENGINE_H

#include "ImageBasedEngine.h"

namespace engar {

    /**
     * Motor de sub-proceso basado en imagenes arbitrarias utilizando keypoints y descriptores.
     */
    class ArbitraryImageEngine : public ImageBasedEngine {

    public:

        ArbitraryImageEngine(VideoFrameProvider *frameProvider, std::shared_ptr<ArDetector> detector, LKTracker *tracker, int targetCols, int targetRows);

        ArbitraryImageEngine(VideoFrameProvider *frameProvider, std::shared_ptr<ArDetector> detector, LKTracker *tracker, int targetCols, int targetRows,
                             cv::Mat &k);

        void ProcessFrame(int &targetId, vector<cv::Point2f> &corners, cv::Mat &homography, cv::Mat &rvec, cv::Mat &tvec) override;

        void ProcessFrame(cv::InputArray frame, int &targetId, vector<cv::Point2f> &corners, cv::Mat &homography, cv::Mat &rvec, cv::Mat &tvec) override;

        void AddTarget(cv::Mat &target) override;

        cv::Mat getRawFrame() const override;

        void Train() override;

        void ShowTargets();

    protected:

        void detectionStep(cv::Mat &homography, cv::Mat &rvec, cv::Mat &tvec, vector<cv::Point2f> &corners) override;

        void trackingStep(cv::Mat &homography, cv::Mat &rvec, cv::Mat &tvec, vector<cv::Point2f> &corners) override;

        vector<cv::Mat> targets;

    private:

        vector<cv::Point2f> coordsCenter = {cv::Point2f(0, 0)};

        vector<cv::Mat> perObjectDescriptors;
        vector<vector<cv::KeyPoint>> perObjectKeypoints;
        vector<vector<cv::Point3f>> baseObj3DPoints;
        vector<vector<cv::Point2f>> baseObj2DPoints;

        std::vector<cv::Point2f> initialCorners;

        int targetCols;
        int targetRows;

        int targetId = -1;

        /// Current frame helper variables
        std::vector<cv::Point2f> cfTargetPoints;
        std::vector<cv::Point2f> cfScenePoints;
        vector<cv::Point3f> cfObj3DPoints;
        vector<cv::Point2f> cfObj2DPoints;
        ushort targetCount = 0;
        vector<cv::Point2f> prevScenePoints;
        vector<cv::Point2f> currentScenePoints;
        vector<cv::Point2f> prevCorners;
        //////////////////////////////////
    };

}
#endif //ENGAR_ARBITRARYIMAGEENGINE_H
