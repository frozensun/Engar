//
// Created by TigerShark on 17/9/2017.
//

#ifndef ENGAR_UNITY3DFRAMEPROVIDER_H
#define ENGAR_UNITY3DFRAMEPROVIDER_H

#include "VideoFrameProvider.h"

using namespace cv;

namespace engar {
    class Unity3DFrameProvider : public VideoFrameProvider {

    public:
        void GrabFrame(UMat &frame) override;
        void GrabFrame(Mat &frame) override;
        void Reset() override;
    };
}
#endif //ENGAR_UNITY3DFRAMEPROVIDER_H
