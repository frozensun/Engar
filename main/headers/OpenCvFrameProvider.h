//
// Created by TigerShark on 17/9/2017.
//

#ifndef ENGAR_OPENCVFRAMEPROVIDER_H
#define ENGAR_OPENCVFRAMEPROVIDER_H

#include <opencv2/videoio.hpp>
#include "VideoFrameProvider.h"

namespace engar {

    class OpenCvFrameProvider : public VideoFrameProvider {

    public:
        OpenCvFrameProvider();

        OpenCvFrameProvider(int deviceId, int w, int h);

        ~OpenCvFrameProvider();

        void GrabFrame(cv::UMat &frame) override;

        void GrabFrame(cv::Mat &frame) override;

        void Reset() override;

    private:
        cv::VideoCapture *videoCapture;
    };


}
#endif //ENGAR_OPENCVFRAMEPROVIDER_H
