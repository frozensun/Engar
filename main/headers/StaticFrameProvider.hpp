#ifndef ENGAR_STATICFRAMEPROVIDER_H
#define ENGAR_STATICFRAMEPROVIDER_H

#include <opencv2/videoio.hpp>
#include "VideoFrameProvider.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

namespace engar {

    class StaticFrameProvider : public VideoFrameProvider {

    public:
        explicit StaticFrameProvider(const std::string &fileName, const cv::Size &size = cv::Size(640, 480), bool rotate = false,
                                     bool blackInterpolation = false) {
            cv::Mat temp = imread(fileName, cv::IMREAD_COLOR);
            cv::resize(temp, this->frame, size, 0, 0, cv::INTER_LINEAR_EXACT);
            this->uFrame = frame.getUMat(cv::ACCESS_READ, cv::USAGE_ALLOCATE_HOST_MEMORY);
            this->rotate = rotate;
            this->blackInterpolation = blackInterpolation;
        }

        ~StaticFrameProvider() {
            this->frame.release();
            this->uFrame.release();
        }

        void GrabFrame(cv::UMat &out) override {
            if (blackInterpolation && (frameCount % 100 == 0 || frameCount % 99 == 0 || frameCount % 98 == 0 || frameCount % 97 == 0)) {
                out = cv::UMat(this->uFrame.size(), this->uFrame.type());
                cv::randn(out, mean, sigma);
            } else {
                if (rotate) {
                    ulong angle = frameCount % 360;
                    cv::Mat matRotation = getRotationMatrix2D(cv::Point(frame.cols / 2, frame.rows / 2), angle, 1);
                    warpAffine(uFrame, out, matRotation, frame.size());
                } else {
                    uFrame.copyTo(out);
                }
            }
            frameCount++;
        }

        void GrabFrame(cv::Mat &out) {
            ulong angle = frameCount % 360;
            cv::Mat matRotation = getRotationMatrix2D(cv::Point(frame.cols / 2, frame.rows / 2), angle, 1);

            // Rotate the image
            warpAffine(frame, out, matRotation, frame.size());
            frameCount++;
        }

    private:
        cv::UMat uFrame;
        cv::Mat frame;
        ulong frameCount = 45;
        bool rotate;
        bool blackInterpolation;

        cv::Mat mean = cv::Mat::zeros(1, 1, CV_64FC1);
        cv::Mat sigma = cv::Mat::ones(1, 1, CV_64FC1);
    };


}
#endif //ENGAR_STATICFRAMEPROVIDER_H


