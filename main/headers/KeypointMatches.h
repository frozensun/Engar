/*
 * KeypointMatches.h
 *
 *  Created on: Nov 20, 2014
 *      Author: tigershark
 */

#ifndef KEYPOINTMATCHES_H_
#define KEYPOINTMATCHES_H_

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"

using namespace std;

class KeypointMatches {
public:

    cv::Ptr<vector<cv::DMatch> > GoodMatches;
    cv::Ptr<vector<cv::KeyPoint> > SceneKeypoints;

    KeypointMatches();

    virtual ~KeypointMatches();
};

#endif /* KEYPOINTMATCHES_H_ */
