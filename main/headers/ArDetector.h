//
// Created by TigerShark on 9/10/2017.
//

#ifndef ENGAR_ARDETECTOR_H
#define ENGAR_ARDETECTOR_H

#include <opencv2/core.hpp>
#include <memory>
#include "ArDetectionResult.h"

namespace engar {

    /**
     * Interface para detectores de AR basados en imagenes.
     */
    class ArDetector {

    public:

        virtual cv::Ptr<ArDetectionResult> detect(cv::InputArray &frame) = 0;

        virtual cv::Ptr<ArDetectionResult> detect(cv::InputArray &frame, const cv::OutputArray &descriptors_object) = 0;

        virtual void computeKeyPoints(cv::Mat &objImage, vector<cv::KeyPoint> &keyPoints, cv::UMat &descriptors) = 0;

        virtual void computeKeyPoints(cv::Mat &objImage, vector<cv::KeyPoint> &keyPoints, cv::Mat &descriptors) = 0;

        cv::Ptr<cv::DescriptorMatcher> matcher;

        std::chrono::microseconds detectTime = std::chrono::microseconds::zero();
        std::chrono::microseconds matchTime = std::chrono::microseconds::zero();

        int nfeatures = 0;
    };

}

#endif //ENGAR_ARDETECTOR_H

