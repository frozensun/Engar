//
// Created by TigerShark on 17/9/2017.
//

#ifndef ENGAR_VIDEOFRAMEPROVIDER_H
#define ENGAR_VIDEOFRAMEPROVIDER_H

#include <opencv2/core.hpp>

using namespace std;

namespace engar {

    /**
     * Interface para proveedores de frames a los diferentes engines. Abstrae las particularidades del uso de hardware o construccion de simulaciones.
     */
    class VideoFrameProvider {

    public:
        virtual void GrabFrame(cv::UMat &frame) = 0;

        virtual void GrabFrame(cv::Mat &frame) = 0;

        cv::Mat lastFrame;

        virtual void Reset() = 0;
    };

}
#endif //ENGAR_VIDEOFRAMEPROVIDER_H
