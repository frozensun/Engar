//
// Created by tigershark on 11/9/19.
//

#ifndef ENGAR_FACEDETECTOR_H
#define ENGAR_FACEDETECTOR_H


#include <vector>
#include <opencv2/core/core.hpp>

using namespace std;
namespace engar {

    /**
     * Interface para detectores de rostros.
     */
    class FaceDetector {

    public:
        virtual void detect(const cv::UMat &colorFrame, vector<cv::Rect> &faces) = 0;
        virtual void detect(cv::InputArray &colorFrame, vector<cv::Rect> &faces) = 0;

    };

}
#endif //ENGAR_FACEDETECTOR_H
