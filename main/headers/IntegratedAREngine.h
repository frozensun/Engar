

#ifndef ENGAR_IntegratedAREngine_H
#define ENGAR_IntegratedAREngine_H

#include <iostream>
#include <filesystem>
#include <csignal>
#include <ratio>
#include <chrono>
#include <map>
#include "TrackedFace.h"
#include "FaceEngine.hpp"
#include "VideoFrameProvider.h"
#include "LKTracker.h"
#include "ArDetector.h"
#include "Identity.h"
#include "ArbitraryImageEngine.h"

#include "OpenCvFrameProvider.h"
#include "Identity.h"
#include "PoolFrameProvider.hpp"
#include "IntegratedAREngine.h"
#include "StaticFrameProvider.hpp"
#include "FaceEngine.hpp"
#include <inferrers/GenderInferrer.h>
#include <inferrers/AgeInferrer.h>
#include <inferrers/FERInferrer.h>
#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"
#include <NGT/Index.h>
#include <omp.h>

namespace engar {

    /**
     * Motor de integracion de AR basada en imagenes y rostros.
     */
    class IntegratedAREngine {

    public:


        IntegratedAREngine(int width, int height, VideoFrameProvider *fp);

        void ProcessFrame(int &targetId, vector<cv::Point2f> &corners, cv::Mat &homography, cv::Mat &rvec, cv::Mat &tvec, bool &facesFound,
                          vector<TrackedFace *> &trackedFaces);

        void LoadFaceEmbeddings(string path);

        void LoadFacesDirectly(std::vector<string> facesPaths);

        void LoadImageDescriptors(string path);

        void LoadImagesDirectly(std::vector<string> imagePaths);

        void Train();

        void AddInferrer(Inferrer *inf);

        void ProjectPoint(std::vector<cv::Point2f> &points);

        void PrintStatistics() const;

        cv::Mat K;

        FaceEngine *faceEngine;

    protected:
        bool stop = false;
        const double f = 621.53903021141821;
        int w = 640, h = 480;

        unsigned long long frameCount = 1;
        int captureWidth = 800;
        int captureHeight = 600;


        VideoFrameProvider *frameProvider;


//// IMAGE SUB-PROCESS ENGINE ////////////////////////////////////////////////////////
//        std::vector<cv::Point2f> projectedAxes;
//        std::vector<cv::Point3f> axis{
//                cv::Point3f(128, 0, 0),
//                cv::Point3f(0, -128, 0),
//                cv::Point3f(0, 0, 128)
//        };

        int poiAmount = 100;
        float trackingPointErrorThreshold = 15;
        ArbitraryImageEngine *imageEngine;


//// FACE SUB-PROCESS ENGINE ////////////////////////////////////////////////////////
        std::map<int, engar::Identity> identities;

        vector<Identity> loadFaceEmbeddings(const string path);

    };

}
#endif //ENGAR_IntegratedAREngine_H
