//
// Created by tigershark on 9/9/19.
//

#ifndef ENGAR_FACEENGINE_H
#define ENGAR_FACEENGINE_H

#include <cstdio>
#include <iostream>
#include <string>
#include <ctime>
#include <vector>

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/face.hpp"
#include "opencv2/dnn.hpp"
#include "opencv2/tracking.hpp"

#include "CvNetFaceDetector.h"
#include "FaceDetector.h"
#include "FaceRecognizer.h"
#include "DlibFaceRecognizer.h"
#include "TrackedFace.h"
#include "OpenCvFrameProvider.h"
#include "Utils.h"
#include <dlib/opencv/cv_image.h>
#include <dlib/dnn.h>
#include <dlib/opencv.h>
#include <opencv2/core/utility.hpp>
#include <inferrers/Inferrer.h>

namespace engar {

    /**
     * Motor del sub-proceso de manejo de rostros.
     */
    class FaceEngine {
    protected:

        int deviceId = 0;
        int width = 800;
        int height = 600;
        cv::Mat colorFrame;

        vector<TrackedFace> trackedFaces;

//        unique_ptr<FaceRecognizer> recognizer;
        map<string, Inferrer *> inferrers;

        shared_ptr<VideoFrameProvider> frameProvider;

        ulong findPeriod = 20;
        ulong recognizePeriod = 20;

        dlib::shape_predictor sp;

        double totalDetectTime = 0;
        double totalTrackTime = 0;
        ulong frameCount = 0;
        ulong detectionFramesCount = 0;
        ulong trackingFramesCount = 0;

    public:

        ushort currentFacesCount = 0;
        unique_ptr<DlibFaceRecognizer> recognizer;
        unique_ptr<FaceDetector> detector;

        FaceEngine(VideoFrameProvider *frameProvider, int w, int h) {
            cout << endl << "Creating Face Engine.......";
            this->width = w;
            this->height = h;
            this->frameProvider.reset(frameProvider);

            // We will also use a face landmarking model to align faces to a standard pose:  (see face_landmark_detection_ex.cpp for an introduction)
            dlib::deserialize("models/dlib/shape_predictor_5_face_landmarks.dat") >> sp;

//            this->detector = std::make_unique<CascadeFaceDetector>();
            this->detector = std::make_unique<CvNetFaceDetector>(w, h);  // ~38ms few false+
//            this->detector = std::make_unique<DlibFaceDetector>();
//            auto detect = std::bind(&CvNetFaceDetector::detect, *this->detector, std::placeholders::_1, std::placeholders::_2);


            this->trackedFaces = vector<TrackedFace>(4);
            for (auto &trackedFace : trackedFaces) {
                trackedFace.Init();
            }

            this->recognizer = make_unique<DlibFaceRecognizer>();

            cout << "done" << endl;
        }

        const cv::Mat &getRawFrame() {
            return this->colorFrame;
        }

        /**
         * Agrega una imagen cruda donde se buscara el rostro principal y agregara al engine.
         * @param face
         * @return
         */
        int AddFaceFromImage(const cv::UMat &face) {
            std::vector<cv::Rect> faces;
            detector->detect(face, faces);

            if (!faces.empty()) {
                int id = recognizer->AddFace(face, faces[0]);
                cout << "Added face with id: " << id << endl;;
                return id;
            }
            return -1;
        }

        /**
         * Agrega un Rostro proporcionando directamente el descriptor precalculado.
         * @param descriptor
         * @return
         */
        int AddFace(const cv::Mat &descriptor) {
            if (!descriptor.empty()) {
                int id = recognizer->AddFace(descriptor);
                cout << "Added face with id: " << id << endl;;
                return id;
            }
            return -1;
        }

        void AddInferrer(string name, Inferrer *inf) {
            this->inferrers[name] = inf;
        }

        void Train() {
            cout << "Training face embeddings matcher." << endl;
            recognizer->Train();
        }

        ~FaceEngine() {
            PrintStatistics();
        }

        void PrintStatistics() const {
            cout << endl << "Face Engine Statistics" << endl;
            cout << "Face detect time:" << endl << "\tAvg = " << (totalDetectTime * 1000 / cv::getTickFrequency()) / detectionFramesCount << " ms" << endl;
            cout << "Face track time:" << endl << "\tAvg = " << (totalTrackTime * 1000 / cv::getTickFrequency()) / trackingFramesCount << " ms" << endl;
        }

        virtual bool ProcessFrame(cv::InputArray frame, std::vector<TrackedFace *> &activeFaces, std::map<int, engar::Identity> &identities) {

            frameCount++;

//            double gf = (double) cv::getTickCount();
//            frameProvider->GrabFrame(colorFrame);
//            cout << "GrabFrame took: " << ((double) cv::getTickCount() - gf) * 1000 / getTickFrequency() << endl;

            colorFrame = frame.getMat();
            activeFaces.clear();

            if (currentFacesCount == 0 || (frameCount % findPeriod) == 0) {
                detectionFramesCount++;
                double t = (double) cv::getTickCount();
                findFaces(colorFrame);
                totalDetectTime += (double) cv::getTickCount() - t;
            }

            if (currentFacesCount == 0) {
                // If no faces found, don't even call the track step
                return false;
            }

            trackingFramesCount++;
            double t = (double) cv::getTickCount();
            bool result = trackFaces(colorFrame, identities);
            totalTrackTime += (double) cv::getTickCount() - t;

            for (auto &trackedFace : trackedFaces) {
                if (!trackedFace.Active) continue;
                activeFaces.push_back(&trackedFace);
            }

            return result;
        }

        void GetFaceChip(cv::Mat &colorFrame, cv::Rect2d &face, int chipSize, dlib::matrix<dlib::rgb_pixel> &face_chip) {
            dlib::cv_image<dlib::bgr_pixel> frame(colorFrame);
            dlib::rectangle faceBbox(face.x, face.y, face.x + face.width, face.y + face.height);

            ///  extract a copy that has been normalized to 150x150 pixels in size and appropriately rotated and centered.
            auto shape = sp(frame, faceBbox);
            extract_image_chip(frame, get_face_chip_details(shape, chipSize, 0.25), face_chip);

            cv::Mat bgrChip;
            cv::cvtColor(dlib::toMat(face_chip), bgrChip, cv::COLOR_RGB2BGR);

            cv::namedWindow("Face chip");
            cv::imshow("Face chip", bgrChip);
        }


    protected:
        bool trackFaces(cv::Mat &workFrame, std::map<int, engar::Identity> &identities) {
            double tt = (double) cv::getTickCount();
            bool ret = false;

            double gf = (double) cv::getTickCount();
//            cout << "Clone took: " << ((double) cv::getTickCount() - gf) * 1000 / getTickFrequency() << endl;

            future<bool> futures[trackedFaces.size()];
            auto trackStart = std::chrono::system_clock::now();
            for (ulong i = 0; i < trackedFaces.size(); i++) {
                auto &trackedFace = trackedFaces[i];
                // Skip inactive objects and ones found in this very frame
                if (!trackedFace.Active || trackedFace.FoundFrame == frameCount) continue;

                futures[i] = trackedFace.Track(workFrame);
            }

            for (ulong i = 0; i < trackedFaces.size(); i++) {
                auto &trackedFace = trackedFaces[i];
                // Skip inactive objects and ones found in this very frame
                if (!trackedFace.Active || trackedFace.FoundFrame == frameCount) continue;

                bool faceLost = futures[i].get();
//                cout << "Tracker took: "
//                     << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - trackStart).count() / 1000 << endl;

                if (faceLost) {
                    cout << "Face lost @" << trackedFace.Bbox << endl;
                    trackedFace.Reset();
                    currentFacesCount--;
                } else {
                    ret = true;

                    /// If not recognized, try to recognize once every recognizePeriod frames
                    if (trackedFace.CurrentIdentity == Identity::Unknown() && (frameCount - trackedFace.RecognitionAttemptFrame) % recognizePeriod == 0) {
                        cout << "Recognizing" << endl;

                        dlib::matrix<dlib::rgb_pixel> face_chip;
                        GetFaceChip(colorFrame, trackedFace.Bbox, 150, face_chip);

                        int id = recognizer->Recognize(face_chip);
                        trackedFace.CurrentIdentity = identities.at(id);
                        trackedFace.RecognitionAttemptFrame = frameCount;
                    }

                    /// Reinfer every recognizePeriod frames
                    if ((frameCount - trackedFace.InferenceAttemptFrame) % recognizePeriod == 0) {
                        cout << "Inferring" << endl;

                        GetFaceChip(colorFrame, trackedFace.Bbox, 224, trackedFace.DlibFaceChip);
                        trackedFace.OpencvFaceChip = dlib::toMat(trackedFace.DlibFaceChip);

                        for (auto inferrer : this->inferrers) {
                            inferrer.second->AsyncInfer(trackedFace.OpencvFaceChip, trackedFace.InferredInfo);
                        }
                        trackedFace.InferenceAttemptFrame = frameCount;
                    }
                }
            }
//            cout << "trackFaces took: " << ((double) cv::getTickCount() - tt) * 1000 / getTickFrequency() << endl;
//            cout << endl << endl;

            return ret;
        }

        bool findFaces(cv::InputArray &workFrame) {
//            cout << "Detecting faces." << endl;
            std::vector<cv::Rect> facesBbox;
            detector->detect(workFrame, facesBbox);
            bool result = false;

            // Check if faces detected or not
            if (!facesBbox.empty()) {
                for (const auto &face : facesBbox) {
                    if (currentFacesCount == trackedFaces.size()) {
                        cout << "Tracked faces limit reached, discarding further detections." << endl;
                        break;
                    }

                    bool skipFace = false;
                    for (auto &trackedFace : trackedFaces) {
                        if (trackedFace.Active) {
                            cv::Point faceCenter = (face.br() + face.tl()) / 2;
                            if (trackedFace.Bbox.contains(faceCenter)) {
                                /// A tracked face is already present at that location, skip detection
                                cout << "Discarding face detected inside a tracked bbox. " << faceCenter << endl;
                                skipFace = true;
                                break;
                            }
                        }
                    }

                    if (skipFace) {
                        continue;
                    }

                    for (auto &trackedFace : trackedFaces) {
                        if (trackedFace.Active) continue;

                        cout << "Found a new face @" << face << endl;
                        currentFacesCount++;
                        result = true;
                        trackedFace.Active = true;
                        trackedFace.Bbox = face;
                        trackedFace.FoundFrame = frameCount;
                        trackedFace.ResetTracker(workFrame, face);
                        break;
                    }
                }
            } else {
//                cout << "No new faces detected." << endl;
            }

            return result;
        }
    };
}
#endif //ENGAR_FACEENGINE_H
