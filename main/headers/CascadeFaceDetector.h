//
// Created by tigershark on 12/8/19.
//

#ifndef ENGAR_CASCADEFACEDETECTOR_H
#define ENGAR_CASCADEFACEDETECTOR_H


#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
#include "FaceDetector.h"

using namespace std;

namespace engar {
    class CascadeFaceDetector : public FaceDetector {

    private:
        string cascadeName;
        string nestedCascadeName;

        cv::UMat image;
        string inputName;
        bool tryflip;
        cv::CascadeClassifier cascade, nestedCascade;
        double scale;

        vector<cv::Rect> faces2;
        const cv::Scalar colors[8] =
                {
                        cv::Scalar(255, 0, 0),
                        cv::Scalar(255, 128, 0),
                        cv::Scalar(255, 255, 0),
                        cv::Scalar(0, 255, 0),
                        cv::Scalar(0, 128, 255),
                        cv::Scalar(0, 255, 255),
                        cv::Scalar(0, 0, 255),
                        cv::Scalar(255, 0, 255)
                };
        cv::UMat smallImg;
        cv::Size minFaceSize;

    public:

        explicit CascadeFaceDetector(double initialScale = 1, const cv::Size &minFaceSize = cv::Size(64, 48));

        void detect(const cv::UMat &colorFrame, vector<cv::Rect> &faces) override;
    };

}
#endif //ENGAR_CASCADEFACEDETECTOR_H
