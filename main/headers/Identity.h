//
// Created by tigershark on 14/9/19.
//

#ifndef ENGAR_IDENTITY_H
#define ENGAR_IDENTITY_H


#include <string>
#include <cstdio>
#include <opencv2/core/mat.hpp>
#include <rapidjson/document.h>
#include "rapidjson/prettywriter.h"

namespace engar {

    /**
     * Identidad de un rostro.
     */
    class Identity {
    public:

        int Id = -1;
        std::string Name = "unknown";
        cv::Mat Descriptor;

        Identity() = default;

        static Identity &Unknown() {
            static Identity unkInstance;
            return unkInstance;
        }

        inline bool operator==(const Identity &other) { return this->Id == other.Id; }

        template<typename Value>
        explicit Identity(Value &val) {
            Deserialize(val);
        }

        template<typename Writer>
        void Serialize(Writer &writer) {
            writer.StartObject();

            writer.String("name");
            writer.String(Name);

            writer.String("descriptor");
            writer.StartArray();
            for (int i = 0; i < Descriptor.size[1]; i++) {
                writer.Double(Descriptor.at<float>(0, i));
            }
            writer.EndArray();

            writer.EndObject();
        }

        template<typename Value>
        void Deserialize(Value &val) {
            if (!val.IsObject()) return;

            this->Name = val["name"].GetString();

            const Value &descr = val["descriptor"];
            std::vector<double> temp;
            for (rapidjson::Value::ConstValueIterator num = descr.Begin(); num != descr.End(); ++num) {
                temp.push_back(num->GetDouble());
            }

            Descriptor.create(1, temp.size(), CV_32F);
            for (ulong i = 0; i < temp.size(); i++) {
                Descriptor.at<float>(0, i) = temp[i];
            }
        }
    };

    inline bool operator==(const Identity &lhs, const Identity &rhs) { return lhs.Id == rhs.Id; }
}
#endif //ENGAR_IDENTITY_H
