//
// Created by tigershark on 4/12/19.
//

#ifndef ENGAR_THREADSAFEQUEUE_H
#define ENGAR_THREADSAFEQUEUE_H

#include <queue>
#include <mutex>

template<typename T>
class ThreadSafeQueue : public std::queue<T> {
public:
    ThreadSafeQueue() : counter(0) {}

    void push(const T &entry) {
        std::lock_guard<std::mutex> lock(mutex);

        std::queue<T>::push(entry);
        counter += 1;
    }

    T get() {
        std::lock_guard<std::mutex> lock(mutex);
        T entry = this->front();
        this->pop();
        return entry;
    }

    void clear() {
        std::lock_guard<std::mutex> lock(mutex);
        while (!this->empty())
            this->pop();
    }

    unsigned int counter;

private:
    std::mutex mutex;
};

#endif //ENGAR_THREADSAFEQUEUE_H
