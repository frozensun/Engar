/*
 * Homography.h
 *
 *  Created on: Nov 18, 2014
 *      Author: tigershark
 */

#ifndef HOMOGRAPHY_H_
#define HOMOGRAPHY_H_

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <vector>

#include "KeypointMatches.h"

using namespace std;

namespace engar {
    class HomographyHelper {
    public:
        HomographyHelper();

        HomographyHelper(cv::Mat &, cv::Mat &, cv::Ptr<KeypointMatches>);

        HomographyHelper(cv::Mat &, vector<cv::KeyPoint> &, cv::Mat &, vector<cv::KeyPoint> &, vector<cv::DMatch> &);

        virtual ~HomographyHelper();

        static bool
        find(vector<cv::Point2f> &initialCorners, vector<cv::KeyPoint> &keypoints_object, cv::Ptr<KeypointMatches> keypointMatches, vector<cv::Point2f> &,
             vector<cv::KeyPoint> &);


        static bool
        find(const vector<cv::Point2f> &objectPoints, const vector<cv::Point2f> &scenePoints, const vector<cv::Point2f> &initialCorners,
             vector<cv::Point2f> &estimatedCorners,
             vector<cv::Point2f> &inliers, cv::Mat &matrix);

        cv::Mat matrix;

        static double computeQuadrilateralArea(const vector<cv::Point2f> &estimatedCorners);

    private:

        void init(const vector<cv::KeyPoint> &keypoints_object, const vector<cv::KeyPoint> &keypoints_scene, const vector<cv::DMatch> &good_matches,
                  cv::Mat &img_object,
                  cv::Mat &img_scene);
    };
}
#endif /* HOMOGRAPHY_H_ */
