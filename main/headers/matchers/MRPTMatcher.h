//
// Created by tigershark on 9/10/19.
//

#ifndef ENGAR_MRPTMATCHER_H
#define ENGAR_MRPTMATCHER_H

#include <vector>
#include "opencv4/opencv2/features2d.hpp"

#include "Mrpt.hpp"


namespace engar {

    class MRPTMatcher : public cv::DescriptorMatcher {


    protected:

        bool needsTrain = false;

        Mrpt* index;

        virtual void knnMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch> > &matches, int k,
                                  cv::InputArrayOfArrays masks = cv::noArray(), bool compactResult = false) CV_OVERRIDE;

        virtual void radiusMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch> > &matches, float maxDistance,
                                     cv::InputArrayOfArrays masks = cv::noArray(), bool compactResult = false) CV_OVERRIDE;

        void appendMat(cv::Mat &d, int imgId = 0);

        std::vector<ushort> imgIdxs;
        ushort imgIdx = 0;
        ushort descIdx = 0;
        ushort descCurrentLimit = 0;
        std::vector<int> descLimits;

        cv::Mat trainData;
        Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> eigen_mat;

    public:

        explicit MRPTMatcher(float targetRecall, int dimensions);

        ~MRPTMatcher() override;

        void add(const cv::_InputArray &descriptors) override;

        void train() override;

        [[nodiscard]] bool isMaskSupported() const override;

        cv::Ptr<DescriptorMatcher> clone(bool emptyTrainData) const override;

        bool empty() const override;

        std::chrono::microseconds knnSearchTime = std::chrono::microseconds::zero();

        std::chrono::microseconds queryBuildTime = std::chrono::microseconds::zero();

        ulong calls = 0;

        ulong descriptorCount = 0;

        double target_recall = 0.9;

        int d;
        int voteThreshold;
        Mrpt_Parameters autotunedParams;
    };

}
#endif //ENGAR_MRPTMATCHER_H
