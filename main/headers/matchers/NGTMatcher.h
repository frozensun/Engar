//
// Created by tigershark on 9/10/19.
//

#ifndef ENGAR_NGTMATCHER_H
#define ENGAR_NGTMATCHER_H

#include <vector>
#include <NGT/Index.h>
#include <opencv4/opencv2/features2d.hpp>

namespace engar {

    class NGTMatcher : public cv::DescriptorMatcher {
    public:

        explicit NGTMatcher(int dimension, bool buildTree = true, float searchEpsilon = 0.1, float searchRadius = FLT_MAX);

        explicit NGTMatcher(NGT::Property &property, float searchEpsilon = 0.1, float searchRadius = FLT_MAX);

        ~NGTMatcher() override;

        void add(const cv::_InputArray &descriptors) override;

        void train() override;

        [[nodiscard]] bool isMaskSupported() const override;

        cv::Ptr<cv::DescriptorMatcher> clone(bool emptyTrainData) const override;

        static NGTMatcher *create(int dimension, bool buildTree = true);

        static NGTMatcher *create(NGT::Property &property);

        std::chrono::microseconds knnSearchTime = std::chrono::microseconds::zero();

        std::vector<ushort> imgIdxs;
        ulong calls = 0;
        float searchEpsilon = 0.1;
        float searchRadius = FLT_MAX;

    protected:

        NGT::Index *index;
        bool needsTrain = false;
        NGT::Property properties;

        virtual void knnMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch> > &matches, int k,
                                  cv::InputArrayOfArrays masks = cv::noArray(), bool compactResult = false) CV_OVERRIDE;

        virtual void radiusMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch> > &matches, float maxDistance,
                                     cv::InputArrayOfArrays masks = cv::noArray(), bool compactResult = false) CV_OVERRIDE;

        void knnMatchFloat(std::vector<std::vector<cv::DMatch>> &allMatches, int k, cv::InputArray &desc) const;

        void knnMatchUChar(std::vector<std::vector<cv::DMatch>> &matches, int k, cv::InputArray &desc);

        ulong descriptorCount = 0;

        std::vector<int> descLimits;

        void appendMat(const cv::Mat &d);

    };

}
#endif //ENGAR_NGTMATCHER_H
