//
// Created by tigershark on 9/10/19.
//

#ifndef ENGAR_ANNOYMATCHER_H
#define ENGAR_ANNOYMATCHER_H

#include <vector>
#include "opencv4/opencv2/features2d.hpp"
#include "annoylib.hpp"
#include "kissrandom.h"

namespace engar {

    template<class T, class D>
    class AnnoyMatcher : public cv::DescriptorMatcher {


    protected:

        bool needsTrain = false;
        annoy::AnnoyIndex<unsigned int, T, D, Kiss32Random> *index = nullptr;
        int trees = 0;
        int idx = 0;

        ulong descriptorCount = 0;

        std::vector<int> descLimits;
        std::vector<ushort> imgIdxs;

        virtual void knnMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch> > &matches, int k,
                                  cv::InputArrayOfArrays masks = cv::noArray(), bool compactResult = false) CV_OVERRIDE;

        virtual void radiusMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch> > &matches, float maxDistance,
                                     cv::InputArrayOfArrays masks = cv::noArray(), bool compactResult = false) CV_OVERRIDE;

        void appendMat(cv::Mat &d);

    public:

        explicit AnnoyMatcher(int dimensions, int trees = 10);

        ~AnnoyMatcher() override;

        void add(const cv::_InputArray &descriptors) override;

        void train() override;

        [[nodiscard]] bool isMaskSupported() const override;

        cv::Ptr<DescriptorMatcher> clone(bool emptyTrainData) const override;

        std::chrono::microseconds knnSearchTime = std::chrono::microseconds::zero();

        std::chrono::microseconds queryBuildTime = std::chrono::microseconds::zero();

        ulong calls = 0;
        int SearchPrecision = 100;
    };

}
#endif //ENGAR_ANNOYMATCHER_H
