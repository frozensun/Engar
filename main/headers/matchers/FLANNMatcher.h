//
// Created by tigershark on 9/10/19.
//

#ifndef ENGAR_FLANNMatcher_H
#define ENGAR_FLANNMatcher_H

#include <vector>
#include <flann/flann.hpp>
#include <opencv4/opencv2/features2d.hpp>

namespace engar {

    template<class ElementType, template<class> class Distance, class ResultType>
    class FLANNMatcher : public cv::DescriptorMatcher {
    public:

        explicit FLANNMatcher(const ::flann::IndexParams &params, const ::flann::SearchParams &searchParams);

        ~FLANNMatcher() override;

        void add(const cv::_InputArray &descriptors) override;

        void train() override;

        [[nodiscard]] bool isMaskSupported() const override;

        cv::Ptr<cv::DescriptorMatcher> clone(bool emptyTrainData) const override;

        std::chrono::microseconds knnSearchTime = std::chrono::microseconds::zero();

        std::vector<ushort> imgIdxs;
        ulong calls = 0;
        float searchEpsilon = 0.1;
        float searchRadius = FLT_MAX;

        ::flann::SearchParams searchParams;
    protected:

        ::flann::Index<Distance<ElementType>> *index;
        ::flann::Matrix<ElementType> data;
        bool needsTrain = false;
        ::flann::IndexParams properties;

        virtual void knnMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch> > &matches, int k,
                                  cv::InputArrayOfArrays masks = cv::noArray(), bool compactResult = false) CV_OVERRIDE;

        virtual void radiusMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch> > &matches, float maxDistance,
                                     cv::InputArrayOfArrays masks = cv::noArray(), bool compactResult = false) CV_OVERRIDE;

        void knnMatchFloat(std::vector<std::vector<cv::DMatch>> &allMatches, int k, cv::InputArray &desc) const;

        void knnMatchUChar(std::vector<std::vector<cv::DMatch>> &matches, int k, cv::InputArray &desc);

        ulong descriptorCount = 0;
        ulong dimensions = 0;

        std::vector<int> descLimits;

        void appendMat(const cv::Mat &d, ::flann::Matrix<ElementType> &data, ulong offset);

    };

}
#endif //ENGAR_FLANNMatcher_H
