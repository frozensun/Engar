//
// Created by tigershark on 9/10/19.
//

#ifndef ENGAR_NMSMATCHER_H
#define ENGAR_NMSMATCHER_H

#include <vector>
#include "opencv4/opencv2/features2d.hpp"
#include "space.h"
#include "index.h"
#include "knnqueue.h"


namespace engar {

    template<class T>
    class NMSMatcher : public cv::DescriptorMatcher {


    protected:

        bool needsTrain = false;
        similarity::Space<T> *space = NULL;
        similarity::Index<T> *index = NULL;
        similarity::ObjectVector dataset;

        virtual void knnMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch> > &matches, int k,
                                  cv::InputArrayOfArrays masks = cv::noArray(), bool compactResult = false) CV_OVERRIDE;

        virtual void radiusMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch> > &matches, float maxDistance,
                                     cv::InputArrayOfArrays masks = cv::noArray(), bool compactResult = false) CV_OVERRIDE;

        void appendMat(cv::Mat &d, int imgId = 0);

    public:

        explicit NMSMatcher(string type, string distance, similarity::AnyParams *iParams, similarity::AnyParams *qParams);

        ~NMSMatcher() override;

        void add(const cv::_InputArray &descriptors) override;

        void train() override;

        [[nodiscard]] bool isMaskSupported() const override;

        cv::Ptr<DescriptorMatcher> clone(bool emptyTrainData) const override;

        std::chrono::microseconds knnSearchTime = std::chrono::microseconds::zero();

        std::chrono::microseconds queryBuildTime = std::chrono::microseconds::zero();

        ulong calls = 0;

        similarity::AnyParams &indexParams;
        similarity::AnyParams &queryParams;
        string indexType;
        string distanceType;

        void SetQueryParams(similarity::AnyParams &qParams);
    };

}
#endif //ENGAR_NMSMATCHER_H
