//
// Created by tigershark on 9/10/19.
//

#ifndef ENGAR_KGraphMatcher_H
#define ENGAR_KGraphMatcher_H

#include <vector>
#include "opencv4/opencv2/features2d.hpp"
#include <kgraph.h>
#include <opencv2/opencv.hpp>
#include <kgraph-data.h> // must follow opencv

namespace engar {

    template<class T, class D>
    class KGraphMatcher : public cv::DescriptorMatcher {


    protected:

        bool needsTrain = false;
        kgraph::KGraph *index = nullptr;
        kgraph::MatrixOracle<T, D> *oracle = nullptr;
        int trees = 0;
        int idx = 0;

        ulong descriptorCount = 0;
        cv::Mat trainData;
        std::vector<int> descLimits;
        std::vector<ushort> imgIdxs;

        virtual void knnMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch> > &matches, int k,
                                  cv::InputArrayOfArrays masks = cv::noArray(), bool compactResult = false) CV_OVERRIDE;

        virtual void radiusMatchImpl(cv::InputArray queryDescriptors, std::vector<std::vector<cv::DMatch> > &matches, float maxDistance,
                                     cv::InputArrayOfArrays masks = cv::noArray(), bool compactResult = false) CV_OVERRIDE;

        void appendMat(cv::Mat &d);

    public:

        explicit KGraphMatcher(int dimensions, kgraph::KGraph::IndexParams &params);

        ~KGraphMatcher() override;

        void add(const cv::_InputArray &descriptors) override;

        void train() override;

        [[nodiscard]] bool isMaskSupported() const override;

        cv::Ptr<DescriptorMatcher> clone(bool emptyTrainData) const override;

        std::chrono::microseconds knnSearchTime = std::chrono::microseconds::zero();

        std::chrono::microseconds queryBuildTime = std::chrono::microseconds::zero();

        ulong calls = 0;

        kgraph::KGraph::IndexParams IndexParams;
        kgraph::KGraph::SearchParams SearchParams;
        kgraph::KGraph::IndexInfo IndexInfo;
    };

}


namespace kgraph {
    namespace metric {
        /// L2 square distance.
        struct hamming {
            template<typename T>
            static inline int cole_popcount(T v) {
                // Note: Only used with MSVC 9, which lacks intrinsics and fails to
                // calculate std::bitset::count for v > 32bit. Uses the generalized
                // approach by Eric Cole.
                // See https://graphics.stanford.edu/~seander/bithacks.html#CountBitsSet64
                v = v - ((v >> 1) & (T) ~(T) 0 / 3);
                v = (v & (T) ~(T) 0 / 15 * 3) + ((v >> 2) & (T) ~(T) 0 / 15 * 3);
                v = (v + (v >> 4)) & (T) ~(T) 0 / 255 * 15;
                return (T) (v * ((T) ~(T) 0 / 255)) >> (sizeof(T) - 1) * 8;
            }

            static inline unsigned BitHamming(const unsigned long long *a, const unsigned long long *b, size_t qty) {
                unsigned res = 0;

                for (size_t i = 0; i < qty; ++i) {
                    //  __builtin_popcount quickly computes the number on 1s
                    res += __builtin_popcount(a[i] ^ b[i]);
                }

                return res;
            }

            template<typename T>
            /// L2 square distance.
            inline static unsigned int apply(T const *a, T const *b, unsigned dim) {
                size_t r = 0;
                for (unsigned i = 0; i < dim; ++i) {
                    r += __builtin_popcountll(a[i] ^ b[i]);
                }
                return r;

//                const unsigned long long *x = reinterpret_cast<const unsigned long long *>(a);
//                const unsigned long long *y = reinterpret_cast<const unsigned long long *>(b);
//                const size_t length = dim / sizeof(unsigned long long); // the last integer is an original number of elements
//
//                return BitHamming(x, y, length);
            }


        };
    }
}
#endif //ENGAR_KGraphMatcher_H
