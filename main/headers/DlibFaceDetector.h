//
// Created by tigershark on 13/9/19.
//

#ifndef ENGAR_DLIBFACEDETECTOR_H
#define ENGAR_DLIBFACEDETECTOR_H

#include "FaceDetector.h"
#include <dlib/image_processing/frontal_face_detector.h>

namespace engar {
    class DlibFaceDetector : public FaceDetector {
    public:
        DlibFaceDetector();

        void detect(const cv::UMat &colorFrame, vector<cv::Rect> &faces) override;

    protected:
        dlib::frontal_face_detector detector;
    };
}

#endif //ENGAR_DLIBFACEDETECTOR_H
