/*
 * PointAndIndex.h
 *
 *  Created on: Nov 22, 2014
 *      Author: tigershark
 */

#ifndef POINTANDINDEX_H_
#define POINTANDINDEX_H_

#include "opencv2/core/core.hpp"

class PointAndIndex {
public:
    PointAndIndex();

    virtual ~PointAndIndex();

    cv::Point2f Point;
    int Index;
};

#endif /* POINTANDINDEX_H_ */
