#ifndef ENGAR_POOLFRAMEPROVIDER_H
#define ENGAR_POOLFRAMEPROVIDER_H

#include <opencv2/videoio.hpp>
#include "VideoFrameProvider.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <random>
#include <opencv2/imgproc.hpp>

namespace engar {

    class PoolFrameProvider : public VideoFrameProvider {

    private:
        int maxTargetId = 0;
        int framesPerTarget = 10;

        int start;
        int end;

        const std::vector<std::string> &targetNames;

        cv::Size size;

        int angleCount = 0;
        int targetId = 0;

        int rotationSpeed;

        cv::Mat affineRotation;

        cv::Mat wall;
        cv::Mat paper;

        cv::UMat uFrame;
        ulong frameCount = 45;
        bool rotate;
        bool blackInterpolation;

        cv::Mat mean = cv::Mat::zeros(1, 1, CV_64FC1);
        cv::Mat sigma = cv::Mat::ones(1, 1, CV_64FC1);

    public:

        int currentTargetId = -1;

        void Reset() override {
            targetId = start - 1;
            LoadNextTarget();
            currentTargetId = targetId;
        }

        explicit PoolFrameProvider(const std::vector<std::string> &targetNames, int framesPerTarget, int startIdx, int endIdx,
                                   const cv::Size &size = cv::Size(640, 480),
                                   bool rotate = false, int rotationSpeed = 1, bool blackInterpolation = false) : targetNames(targetNames) {
            this->size = size;
            maxTargetId = targetNames.size();
            start = startIdx;
            end = endIdx;
            this->rotationSpeed = rotationSpeed;
            this->framesPerTarget = framesPerTarget;


            this->rotate = rotate;
            this->blackInterpolation = blackInterpolation;

            wall = imread("./img/wall.jpg", cv::IMREAD_COLOR);
            paper = imread("./img/paper.jpg", cv::IMREAD_COLOR);

            targetId = startIdx - 1;
            LoadNextTarget();
            currentTargetId = targetId;

            cv::Point2f pts1[] = {cv::Point2f(56, 65), cv::Point2f(368, 52), cv::Point2f(28, 387), cv::Point2f(389, 390)};
            cv::Point2f pts2[] = {cv::Point2f(0, 0), cv::Point2f(300, 0), cv::Point2f(0, 300), cv::Point2f(300, 300)};

            affineRotation = getPerspectiveTransform(pts1, pts2);
        }

        ~PoolFrameProvider() {
            this->lastFrame.release();
            this->uFrame.release();
        }

        void LoadNextTarget() {
            targetId++;
            currentTargetId = targetId;

            paper.copyTo(this->lastFrame);

            if (currentTargetId > end)
                return;

            cv::Mat temp = imread(targetNames[targetId], cv::IMREAD_COLOR);
            temp.copyTo(lastFrame(cv::Rect(lastFrame.cols / 2 - temp.cols / 2, lastFrame.rows / 2 - temp.rows / 2, temp.cols, temp.rows)));

            this->uFrame = lastFrame.getUMat(cv::ACCESS_READ, cv::USAGE_ALLOCATE_HOST_MEMORY);
            angleCount = 0;
        }

        void GrabFrame(cv::UMat &out) override {
            if (frameCount % framesPerTarget == 0) {
                LoadNextTarget();
            }

            currentTargetId = targetId;

            if (blackInterpolation && (frameCount % 100 == 0 || frameCount % 99 == 0 || frameCount % 98 == 0 || frameCount % 97 == 0)) {
                out = cv::UMat(this->uFrame.size(), this->uFrame.type());
                cv::randn(out, mean, sigma);
                currentTargetId = -1;
            } else {
                if (rotate) {
                    ulong angle = angleCount % 360;
                    cv::Mat matRotation = getRotationMatrix2D(cv::Point(lastFrame.cols / 2, lastFrame.rows / 2), angle, 1);
                    cv::Mat temp;
                    warpAffine(lastFrame, temp, matRotation, lastFrame.size());
                    warpPerspective(temp, temp, affineRotation, lastFrame.size());

                    cv::Mat mask(lastFrame.size(), CV_8U, cv::Scalar(1));
                    warpAffine(mask, mask, matRotation, mask.size());
                    warpPerspective(mask, mask, affineRotation, mask.size());

                    wall.copyTo(out);
                    temp.copyTo(out, mask);

                    out.copyTo(lastFrame);
                    GaussianBlur(out, out, cv::Size(5, 5), 0.1, 0.1);

                } else {
                    uFrame.copyTo(out);
                }
            }

            frameCount++;
            angleCount += rotationSpeed;
        }

        void GrabFrame(cv::Mat &out) override {
            if (frameCount % framesPerTarget == 0) {
                LoadNextTarget();
            }

            currentTargetId = targetId;

            if (blackInterpolation && (frameCount % 50 == 0 || frameCount % 49 == 0 || frameCount % 51 == 0)) {
                out = cv::Mat(this->uFrame.size(), this->uFrame.type());
                cv::randn(out, mean, sigma);
                currentTargetId = -1;
            } else {
                if (rotate) {
                    ulong angle = angleCount % 360;
                    cv::Mat matRotation = getRotationMatrix2D(cv::Point(lastFrame.cols / 2, lastFrame.rows / 2), angle, 1);

                    cv::Mat temp;
                    warpAffine(uFrame, temp, matRotation, uFrame.size());
                    warpPerspective(temp, temp, affineRotation, temp.size());

                    cv::Mat mask(lastFrame.size(), CV_8U, cv::Scalar(1));
                    warpAffine(mask, mask, matRotation, mask.size());
                    warpPerspective(mask, mask, affineRotation, mask.size());

                    wall.copyTo(out);
                    temp.copyTo(out, mask);

                    out.copyTo(lastFrame);
                    GaussianBlur(out, out, cv::Size(5, 5), 0.1, 0.1);
                } else {
                    uFrame.copyTo(out);
                }
            }
            angleCount += rotationSpeed;
            frameCount++;
        }


    };


}
#endif //ENGAR_POOLFRAMEPROVIDER_H


