//
// Created by tigershark on 1/12/19.
//

#ifndef ENGAR_CAFFENETABSTRACTINFERRER_H
#define ENGAR_CAFFENETABSTRACTINFERRER_H

#include <iostream>
#include "opencv2/core.hpp"


namespace engar {

    class Inferrer {
    public:

        std::string Name;

        virtual void Infer(cv::Mat &img, std::vector<std::pair<std::string, float>> &re) = 0;

        virtual void Infer(cv::UMat &img, std::vector<std::pair<std::string, float>> &re) = 0;

        virtual void AsyncInfer(cv::Mat &image, std::map<std::string, std::vector<std::pair<std::string, float>>> &result) = 0;

        Inferrer(std::string name) {
            this->Name = name;
        }
    };
}
#endif //ENGAR_CAFFENETABSTRACTINFERRER_H
