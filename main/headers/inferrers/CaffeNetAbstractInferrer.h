//
// Created by tigershark on 1/12/19.
//

#ifndef ENGAR_INFERRER_H
#define ENGAR_INFERRER_H

#include <iostream>
#include <queue>
#include <condition_variable>
#include "opencv2/core.hpp"
#include "opencv2/dnn.hpp"
#include "Inferrer.h"
#include "../ThreadSafeQueue.h"

namespace engar {

    class CaffeNetAbstractInferrer : public engar::Inferrer {
    protected:
        std::string caffeConfigFile;
        std::string caffeWeightFile;

        std::string inputName;
        std::string outputName;

        cv::Size insize;
        cv::Scalar meanColor;

        cv::dnn::Net net{};

        std::vector<std::string> classes;

        ThreadSafeQueue<cv::Mat> pendingChips;

        ThreadSafeQueue<std::map<std::string, std::vector<std::pair<std::string, float>>>*> resultLocations;

        std::condition_variable lock;

    public:
        bool End = false;

        CaffeNetAbstractInferrer(const CaffeNetAbstractInferrer &) = delete;              // no copying!
        CaffeNetAbstractInferrer &operator=(const CaffeNetAbstractInferrer &) = delete; // no copying!

        explicit CaffeNetAbstractInferrer(std::string name);

        void Infer(cv::Mat &image, std::vector<std::pair<std::string, float>> &result) override;

        void Infer(cv::UMat &image, std::vector<std::pair<std::string, float>> &result) override;

        void AsyncInfer(cv::Mat &image, std::map<std::string, std::vector<std::pair<std::string, float>>> &result) override;
    };
}
#endif //ENGAR_INFERRER_H
