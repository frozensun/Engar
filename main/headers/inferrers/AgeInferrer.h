//
// Created by tigershark on 1/12/19.
//

#ifndef ENGAR_AGEINFERRER_H
#define ENGAR_AGEINFERRER_H


#include <random>
#include <iostream>
#include <iomanip>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/dnn.hpp"

#include "Inferrer.h"

namespace engar {

    class AgeInferrer : public CaffeNetAbstractInferrer {

    public:

        explicit AgeInferrer(string name) : CaffeNetAbstractInferrer(name) {
            caffeConfigFile = "models/googlenet_aligned_age.prototxt";
            caffeWeightFile = "models/googlenet_aligned_age.caffemodel";

            inputName = "data";
            outputName = "loss3/loss3";

            insize = cv::Size(224, 224);
            meanColor = cv::Scalar(78.4263377603, 87.7689143744, 114.895847746);

            classes = {"0-2", "4-6", "8-13", "15-20", "25-32", "38-43", "48-53", "60+"};
            net = cv::dnn::readNetFromCaffe(caffeConfigFile, caffeWeightFile);
        }
    };
}
#endif //ENGAR_AGEINFERRER_H
