//
// Created by tigershark on 1/12/19.
//

#ifndef ENGAR_GENDERINFERRER_H
#define ENGAR_GENDERINFERRER_H


#include <random>
#include <iostream>
#include <iomanip>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/dnn.hpp"

#include "Inferrer.h"
#include "CaffeNetAbstractInferrer.h"

namespace engar {

    class GenderInferrer : public CaffeNetAbstractInferrer {


    public:

        explicit GenderInferrer(std::string name) : CaffeNetAbstractInferrer(name) {
            caffeConfigFile = "models/googlenet_aligned_gender.prototxt";
            caffeWeightFile = "models/googlenet_aligned_gender.caffemodel";

            inputName = "data";
            outputName = "loss3/loss3";

            insize = cv::Size(224, 224);
            meanColor = cv::Scalar(78.4263377603, 87.7689143744, 114.895847746);

            classes = {"Masculino", "Femenino"};

            net = cv::dnn::readNetFromCaffe(caffeConfigFile, caffeWeightFile);
            net.setPreferableBackend(cv::dnn::DNN_BACKEND_VKCOM);
            net.setPreferableTarget(cv::dnn::DNN_TARGET_VULKAN);
        }

    };
}
#endif //ENGAR_GENDERINFERRER_H
