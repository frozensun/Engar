//
// Created by tigershark on 1/12/19.
//

#ifndef ENGAR_FERINFERRER_H
#define ENGAR_FERINFERRER_H


#include <random>
#include <iostream>
#include <iomanip>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/dnn.hpp"

#include "Inferrer.h"
#include "CaffeNetAbstractInferrer.h"

namespace engar {

    class FERInferrer : public CaffeNetAbstractInferrer {


    public:

        explicit FERInferrer(std::string name) : CaffeNetAbstractInferrer(name) {
            /// Basadas en inception
//            caffeConfigFile = "models/fer2013_mini_XCEPTION.prototxt";
//            caffeWeightFile = "models/fer2013_mini_XCEPTION.caffemodel";

//            insize = cv::Size(64, 64);
//            inputName = "data";
//            outputName = "predictions";
            //////////////////////////////////////////////////////////////////////////


            /// Basadas en ResNet
//            caffeConfigFile = "models/FER-ResNet.prototxt";
//            caffeWeightFile = "models/FER-ResNet.caffemodel";
            caffeConfigFile = "models/ybch14-github-FER-ResNet.prototxt";
            caffeWeightFile = "models/ybch14-github-FER-ResNet.caffemodel";

            insize = cv::Size(224, 224);
            inputName = "data";
//            outputName = "predictions";
            outputName = "fc8";
            //////////////////////////////////////////////////////////////////////////

            /// Basadas en Alexnet
//            caffeConfigFile = "models/alexnet_FER.prototxt";
//            caffeWeightFile = "models/alexnet_FER_iter_5000.caffemodel";

//            insize = cv::Size(227, 227);
//            inputName = "data";
//            outputName = "fc8_em";
            //////////////////////////////////////////////////////////////////////////

//            meanColor = cv::Scalar(0, 0, 0);

            classes = {"angry", "co", "disgust", "fear", "happy", "neutral", "sad", "surprised"};
//            classes = {"afraid", "angry", "disgusted", "happy", "neutral", "sad", "surprised"};

            net = cv::dnn::readNetFromCaffe(caffeConfigFile, caffeWeightFile);
        }

        void Infer(cv::Mat &image, std::vector<std::pair<std::string, float>> &result) override {
            cv::Mat img;
            cv::cvtColor(image, img, cv::COLOR_BGR2GRAY);
            CaffeNetAbstractInferrer::Infer(img, result);
        }

        void AsyncInfer(cv::Mat &image, std::map<std::string, std::vector<std::pair<std::string, float>>> &result) override {
            cv::Mat img;
            cv::cvtColor(image, img, cv::COLOR_BGR2GRAY);
//            UMat inputBlob = cv::dnn::blobFromImage(img, 1.0, insize, meanColor, true).getUMat(ACCESS_READ, USAGE_ALLOCATE_SHARED_MEMORY);
            cv::Mat inputBlob = cv::dnn::blobFromImage(img, 1.0, insize, meanColor, true);
            resultLocations.push(&result);
            pendingChips.push(inputBlob);
            lock.notify_one();
        }

    };
}
#endif //ENGAR_FERINFERRER_H
