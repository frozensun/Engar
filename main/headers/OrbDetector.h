//
// Created by TigerShark on 9/10/2017.
//

#ifndef ENGAR_ORBDETECTOR_H
#define ENGAR_ORBDETECTOR_H

#include "ArDetector.h"
#include <vector>
#include <params.h>

#include "KeypointMatches.h"
#include <flann/flann.hpp>

namespace engar {

    class OrbDetector : public ArDetector {

    public:

        OrbDetector(int nFeatures);

        ~OrbDetector();

        cv::Ptr<ArDetectionResult> detect(cv::InputArray &frame) override;

        cv::Ptr<ArDetectionResult> detect(cv::InputArray &frame, const cv::OutputArray &descriptors_object) override;

        void computeKeyPoints(cv::Mat &objImage, vector<cv::KeyPoint> &keyPoints, cv::UMat &descriptors) override;

        void computeKeyPoints(cv::Mat &objImage, vector<cv::KeyPoint> &keyPoints, cv::Mat &descriptors) override;

        ::flann::SearchParams flannSearchParams;

        float ngtSearchEpsilon = 0.1;

        similarity::AnyParams *hnswSearchParams;
    private:
        float scaleFactor = 1.2f;
        int nlevels = 8;
        int edgeThreshold = 31;
        int firstLevel = 0;
        int WTA_K = 2;
        cv::ORB::ScoreType scoreType = cv::ORB::HARRIS_SCORE;
        int patchSize = 31;

        int matchCount = 0;

        cv::Ptr<engar::ArDetectionResult> result;

        cv::Ptr<cv::Feature2D> detector;

//        Ptr<KeypointMatches> keysAndMatch = new KeypointMatches();
//        Ptr<vector<KeyPoint> > keypoints_scene = new vector<KeyPoint>;
        vector<vector<cv::DMatch> > matches;
    };

}
#endif //ENGAR_ORBDETECTOR_H
