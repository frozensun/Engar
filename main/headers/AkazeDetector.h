////
//// Created by TigerShark on 9/10/2017.
////

#ifndef ENGAR_AKAZEDETECTOR_H
#define ENGAR_AKAZEDETECTOR_H

#include "ArDetector.h"
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <vector>

#include "KeypointMatches.h"

namespace engar {

    class AkazeDetector : public ArDetector {

    public:

        explicit AkazeDetector();

        cv::Ptr<ArDetectionResult> detect(cv::InputArray &frame) override;

        cv::Ptr<ArDetectionResult> detect(cv::InputArray &frame, const cv::OutputArray &descriptors_object) override;

        void computeKeyPoints(cv::Mat &objImage, vector<cv::KeyPoint> &keyPoints, cv::UMat &descriptors) override;

        void computeKeyPoints(cv::Mat &objImage, vector<cv::KeyPoint> &keyPoints, cv::Mat &descriptors) override;

    private:
        int matchCount = 0;

        cv::Ptr<engar::ArDetectionResult> result;

        cv::Ptr<cv::Feature2D> detector;

        vector<vector<cv::DMatch> > matches;
    };

}
#endif //ENGAR_AKAZEDETECTOR_H
