//
// Created by TigerShark on 30/06/2017.
//

#ifndef ENGAR_ARDETECTIONRESULT_H
#define ENGAR_ARDETECTIONRESULT_H


#include <string>
#include "opencv2/core/types.hpp"
#include "KeypointMatches.h"

namespace engar {
    class ArDetectionResult {
    public:
        bool Detected;

        cv::Ptr<cv::Point2f> CornerA;
        cv::Ptr<cv::Point2f> CornerB;
        cv::Ptr<cv::Point2f> CornerC;
        cv::Ptr<cv::Point2f> CornerD;

        std::vector<cv::Point2f> Points;

        cv::Ptr<KeypointMatches> KPMatches;

        int MinX;
        int MaxX;
        int MinY;
        int MaxY;

        ArDetectionResult();

        ~ArDetectionResult();
    };
}


#endif //ENGAR_ARDETECTIONRESULT_H
