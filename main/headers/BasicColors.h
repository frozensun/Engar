//
// Created by TigerShark on 21/10/2017.
//

#ifndef ENGAR_COLORS_H
#define ENGAR_COLORS_H


#include "opencv2/core/types.hpp"

namespace engar {
    class BasicColors {
    public:

        BasicColors() {
            Red = cv::Scalar(0, 0, 255);
            Green = cv::Scalar(0, 255, 0);
            Blue = cv::Scalar(255, 0, 0);
            Magenta = cv::Scalar(255, 0, 255);
            Cyan = cv::Scalar(255, 255, 0);
            Yellow = cv::Scalar(0, 255, 255);
            Black = cv::Scalar(0, 0, 0);
            White = cv::Scalar(255, 255, 255);
        }

        ~BasicColors() = default;

        cv::Scalar Red;
        cv::Scalar Green;
        cv::Scalar Blue;
        cv::Scalar Magenta;
        cv::Scalar Cyan;
        cv::Scalar Yellow;
        cv::Scalar Black;
        cv::Scalar White;
    };

}
#endif //ENGAR_COLORS_H
