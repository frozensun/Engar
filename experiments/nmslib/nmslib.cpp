//
// Created by tigershark on 11/10/19.
//
#include <exception>
#include <random>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include "../../engar/headers/ArDetector.h"
#include "../../engar/headers/FastDetector.h"
#include "../../engar/headers/OrbDetector.h"
#include <engar/matchers/NMSMatcher.h>
#include "opencv4/opencv2/core/mat.hpp"

#include "init.h"
#include "index.h"
#include "params.h"
#include "knnquery.h"
#include "knnqueue.h"
#include "methodfactory.h"
#include "spacefactory.h"
#include "space.h"

using std::vector;

using similarity::initLibrary;
using similarity::AnyParams;
using similarity::Index;
using similarity::MethodFactoryRegistry;
using similarity::SpaceFactoryRegistry;
using similarity::AnyParams;
using similarity::Space;
using similarity::ObjectVector;
using similarity::Object;
using similarity::KNNQuery;
using similarity::KNNQueue;

template<typename T>
vector<vector<T>> read_record(string fileName, unsigned long maxRows = 1000000) {

    // File pointer
    fstream fin;

    // Open an existing file
    fin.open(fileName, ios::in);

    // Read the Data from the file
    // as String Vector
    string line, word;

    vector<vector<T>> result;
    unsigned long rowCount = 0;
    unsigned long lastRowLength = 0;
    while (getline(fin, line) && rowCount < maxRows) {
        rowCount++;

        vector<T> &row = result.emplace_back();
        row.reserve(lastRowLength);
        lastRowLength = 0;

        // used for breaking words
        stringstream s(line);

        while (!s.eof()) {
            T temp;
            s >> temp;
            row.emplace_back(temp);
            s.ignore();
            lastRowLength++;
        }
    }

    fin.close();

    return result;
}

int main(int argc, char **argv) {
    int samplesAmount = 200;
    int sampleSize = 200;
    typedef float dataType;

    try {
        vector<vector<dataType>> train = read_record<dataType>("../../../../../data/train.csv", samplesAmount * sampleSize);
        int dimensions = train[0].size();
        vector<Mat> samples;
        for (int sampleIdx = 0; sampleIdx < samplesAmount; sampleIdx++) {
            cv::Mat sample;
            sample.create(sampleSize, dimensions, CV_32F);
            for (int elementIdx = 0; elementIdx < sampleSize; elementIdx++) {
                vector<dataType> &t = train[sampleIdx * sampleSize + elementIdx];
                memcpy(sample.ptr(elementIdx), (void *) t.data(), (size_t) t.size() * sizeof(dataType));
            }
            samples.emplace_back(sample);
        }
        train.clear();
        train.shrink_to_fit();

        typedef float indexType;
        similarity::initLibrary();
        ObjectVector dataset;
        Space<indexType> *space = NULL;
        Index<indexType> *index = NULL;

        try {
            initLibrary();
            const char *distanceType = "l2";
            space = SpaceFactoryRegistry<indexType>::Instance().CreateSpace(distanceType, AnyParams());

            for (auto &trainVectors : samples) {
                for (int i = 0; i < trainVectors.rows; i++) {
                    dataType *row = trainVectors.ptr<dataType>(i);
                    dataset.push_back(new Object(i, 17, trainVectors.cols * sizeof(dataType), row));
                }
            }

            index = MethodFactoryRegistry<indexType>::Instance().CreateMethod(true, "hnsw", distanceType, *space, dataset);
            index->CreateIndex(AnyParams({"efConstruction=2000", "indexThreadQty=12"}));
            index->SetQueryTimeParams(AnyParams({"ef=200"}));
//            index->SaveIndex("nmslib_index");
        }
        catch (...) {
            dataset.clear();
            if (index) { delete index; }
            if (space) { delete space; }
        }

        long timeDirect = 0;
        KNNQueue<indexType> *result = NULL;

        int searchesAmount = 100;
        for (int i = 0; i < searchesAmount; i++) {
            auto &queryVectors = samples[i];
            for (int j = 0; j < queryVectors.rows; j++) {
                dataType *row = queryVectors.ptr<dataType>(j);
                KNNQuery<indexType> query(*space, new Object(-1, -1, queryVectors.cols * sizeof(dataType), row), 2);

                auto t1 = std::chrono::high_resolution_clock::now();
                index->Search(&query);
                timeDirect += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1).count();

                result = query.Result()->Clone();

                // Ignore actual results for now, just measuring times

//                int resultSize = result->Size();
//                for (int i = 0; i < resultSize; i++) {
//                    float distance = result->TopDistance();
//                    const Object *obj = result->Pop();
//                    if (distance > 0) {
//                        cout << "Query: " << j << " Id: " << obj->id() << "\tLabel: " << obj->label() << "\tDistance: " << distance << endl;
//                    }
//                }
            }
        }

        engar::NMSMatcher<indexType> *m = new engar::NMSMatcher<indexType>("hnsw", "l2",
                                                                           new AnyParams({"efConstruction=2000", "indexThreadQty=12"}),
                                                                           new similarity::AnyParams({"ef=200"}));
        for (auto &sample : samples) {
            m->add(sample);
        }
        m->train();

        long timeMatcher = 0;

        for (int i = 0; i < searchesAmount; i++) {
            auto &queryVectors = samples[i];

            vector<vector<DMatch>> matches;
            auto t1 = std::chrono::high_resolution_clock::now();
            m->knnMatch(queryVectors, matches, 2);
            timeMatcher += std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1).count();
        }
        cout << endl << "Time direct: " << timeDirect / (samplesAmount * sampleSize) << "μs" << endl;
        cout << "Time though matcher: " << timeMatcher / (samplesAmount * sampleSize) << "μs" << endl;
        cout << "Algorithm time (w/o query and result creation): " << m->knnSearchTime.count() / (samplesAmount * sampleSize) << "μs" << endl;

    } catch (std::exception &e) {
        cout << e.what();
    }


    return 0;
}
