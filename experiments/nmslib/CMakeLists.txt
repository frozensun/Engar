include_directories("../nmslib/similarity_search/")
include_directories("${CMAKE_SOURCE_DIR}/headers/")
include_directories("${CMAKE_SOURCE_DIR}/source/")
include_directories("${OpenCV_INCLUDE_DIRS}")

add_executable(nmslibtest nmslib.cpp)
target_link_libraries(nmslibtest "NonMetricSpaceLib")
target_link_libraries(nmslibtest ${OpenCV_LIBS})
target_link_libraries(nmslibtest engar)