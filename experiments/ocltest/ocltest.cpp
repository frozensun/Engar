//
// Created by tigershark on 11/10/19.
//
#include <iostream>
#include <fstream>

#include <opencv2/core/ocl.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>
#include <opencv2/imgcodecs.hpp>

using namespace std;
using namespace cv;

template<typename T>
vector<vector<T>> read_record(string fileName, unsigned long maxRows = 1000000) {

    // File pointer
    fstream fin;

    // Open an existing file
    fin.open(fileName, ios::in);

    // Read the Data from the file
    // as String Vector
    string line, word;

    vector<vector<T>> result;
    unsigned long rowCount = 0;
    unsigned long lastRowLength = 0;
    while (getline(fin, line) && rowCount < maxRows) {
        rowCount++;

        vector<T> &row = result.emplace_back();
        row.reserve(lastRowLength);
        lastRowLength = 0;

        // used for breaking words
        stringstream s(line);

        while (!s.eof()) {
            T temp;
            s >> temp;
            row.emplace_back(temp);
            s.ignore();
            lastRowLength++;
        }
    }

    fin.close();

    return result;
}

template<>
vector<vector<unsigned char>> read_record(string fileName, unsigned long maxRows) {

    // File pointer
    fstream fin;

    // Open an existing file
    fin.open(fileName, ios::in);

    // Read the Data from the file
    // as String Vector
    string line, word;

    vector<vector<unsigned char>> result;
    unsigned long rowCount = 0;
    unsigned long lastRowLength = 0;
    while (getline(fin, line) && rowCount < maxRows) {
        rowCount++;

        vector<unsigned char> &row = result.emplace_back();
        row.reserve(lastRowLength);
        lastRowLength = 0;

        // used for breaking words
        stringstream s(line);

        while (!s.eof()) {
            float temp;
            s >> temp;
            unsigned char val = static_cast<unsigned char>(temp);
            row.emplace_back(val);
            s.ignore();
            lastRowLength++;
        }
    }

    fin.close();

    return result;
}

void toMatVectorF(int samplesAmount, int sampleSize, vector<vector<float>> &train, vector<UMat> &samples);

void toMatVectorH(int samplesAmount, int sampleSize, vector<vector<unsigned char>> &train, vector<UMat> &samples);

void showOclSupport();

chrono::nanoseconds runMatcher(vector<UMat> &trainSamples, vector<UMat> &testSamples, int k, int searchSize, DescriptorMatcher *matcher);

int main(int argc, char **argv) {

    showOclSupport();

    cv::UMat image;
    cv::ocl::setUseOpenCL(true);
    cv::imread("../../../img/targets/target_1.jpg", cv::IMREAD_GRAYSCALE).copyTo(image);

    cv::Ptr<cv::xfeatures2d::SURF> surf = cv::xfeatures2d::SURF::create();
    std::vector<cv::KeyPoint> keypoints;
    auto t2 = std::chrono::high_resolution_clock::now();
    surf->detect(image, keypoints, cv::noArray());
    cout << "OCL ON Surf took:  " << chrono::duration_cast<chrono::microseconds>(std::chrono::high_resolution_clock::now() - t2).count() << endl;

    cv::ocl::setUseOpenCL(false);
    std::vector<cv::KeyPoint> keypoints_gold;
    t2 = std::chrono::high_resolution_clock::now();
    surf->detect(image, keypoints_gold, cv::noArray());
    cout << "OCL OFF Surf took: " << chrono::duration_cast<chrono::microseconds>(std::chrono::high_resolution_clock::now() - t2).count() << endl;


    int samplesAmount = 50;
    int sampleSize = 200;
    int searchSize = 50;
    int k = 10;

    /// Load train /////////////////////////////////////////////////////////////////////////////////
    vector<UMat> trainSamples;
    vector<vector<float>> train = read_record<float>("../../../../../data/test.csv", samplesAmount * sampleSize);
    toMatVectorF(samplesAmount, sampleSize, train, trainSamples);

    /// Load test /////////////////////////////////////////////////////////////////////////////////
    vector<UMat> testSamples;
    toMatVectorF(samplesAmount, sampleSize, train, testSamples);
    train.clear();
    train.shrink_to_fit();

    /// OCL ON  /////////////////////////////////////////////////////////////////////////////////
    cv::ocl::setUseOpenCL(true);
    DescriptorMatcher *bf = new BFMatcher(NORM_L2, false);
    cout << "OCL ON L2 Matcher took:  " << runMatcher(trainSamples, testSamples, k, searchSize, bf).count() << endl;

    /// OCL OFF /////////////////////////////////////////////////////////////////////////////////
    cv::ocl::setUseOpenCL(false);
    DescriptorMatcher *bf2 = new BFMatcher(NORM_L2, false);
    cout << "OCL OFF L2 Matcher took: " << runMatcher(trainSamples, testSamples, k, searchSize, bf2).count() << endl;




    /// Load train /////////////////////////////////////////////////////////////////////////////////
    vector<UMat> trainSamplesh;
    vector<vector<unsigned char>> trainh = read_record<unsigned char>("../../../../../data/test.csv", samplesAmount * sampleSize);
    toMatVectorH(samplesAmount, sampleSize, trainh, trainSamples);

    /// Load test /////////////////////////////////////////////////////////////////////////////////
    vector<UMat> testSamplesh;
    toMatVectorH(samplesAmount, sampleSize, trainh, testSamples);
    train.clear();
    train.shrink_to_fit();

    /// OCL ON  /////////////////////////////////////////////////////////////////////////////////
    cv::ocl::setUseOpenCL(true);
    DescriptorMatcher *bfh = new BFMatcher(NORM_HAMMING, false);
    cout << "OCL ON HAMMING Matcher took:  " << runMatcher(trainSamplesh, testSamplesh, k, searchSize, bfh).count() << endl;

    /// OCL OFF /////////////////////////////////////////////////////////////////////////////////
    cv::ocl::setUseOpenCL(false);
    DescriptorMatcher *bfh2 = new BFMatcher(NORM_L2, false);
    cout << "OCL OFF HAMMING Matcher took: " << runMatcher(trainSamplesh, testSamplesh, k, searchSize, bfh2).count() << endl;

    return 0;
}


chrono::nanoseconds runMatcher(vector<UMat> &trainSamples, vector<UMat> &testSamples, int k, int searchSize, DescriptorMatcher *matcher) {
    for (const auto &s :  trainSamples) {
        matcher->add(s);
    }

    chrono::nanoseconds knnTime = std::chrono::nanoseconds::zero();

    for (int i = 0; i < searchSize; i++) {
        vector<vector<DMatch> > matches;
        int sampleSize = 200;
        matches.resize(sampleSize);

        auto t2 = std::chrono::high_resolution_clock::now();
        matcher->knnMatch(testSamples[i], matches, k);
        auto time = chrono::duration_cast<chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - t2);
        knnTime += time;
    }

    return knnTime;
}

void toMatVectorF(int samplesAmount, int sampleSize, vector<vector<float>> &train, vector<UMat> &samples) {
    int dimensions = train[0].size();
    for (int sampleIdx = 0; sampleIdx < samplesAmount; sampleIdx++) {
        Mat sample;
        sample.create(sampleSize, dimensions, CV_32FC1);
        for (int elementIdx = 0; elementIdx < sampleSize; elementIdx++) {
            vector<float> &t = train[sampleIdx * sampleSize + elementIdx];
            memcpy(sample.ptr(elementIdx), (void *) t.data(), (size_t) t.size() * sizeof(float));
        }
        UMat uSample;
        sample.copyTo(uSample);
        samples.emplace_back(uSample);
    }
}

void toMatVectorH(int samplesAmount, int sampleSize, vector<vector<unsigned char>> &train, vector<UMat> &samples) {
    int dimensions = train[0].size();
    for (int sampleIdx = 0; sampleIdx < samplesAmount; sampleIdx++) {
        Mat sample;
        sample.create(sampleSize, dimensions, CV_8UC1);
        for (int elementIdx = 0; elementIdx < sampleSize; elementIdx++) {
            vector<unsigned char> &t = train[sampleIdx * sampleSize + elementIdx];
            memcpy(sample.ptr(elementIdx), (void *) t.data(), (size_t) t.size() * sizeof(unsigned char));
        }
        UMat uSample;
        sample.copyTo(uSample);
        samples.emplace_back(uSample);
    }
}


void showOclSupport() {
    if (!cv::ocl::haveOpenCL()) {
        cout << "OpenCL is not available..." << endl;
        return;
    }

    cv::ocl::Context context;
    if (!context.create(cv::ocl::Device::TYPE_GPU)) {
        cout << "Failed creating the context..." << endl;
        return;
    }

    cout << "SVM:               " << context.useSVM() << endl;
    cout << context.ndevices() << " GPU devices are detected." << endl; //This bit provides an overview of the OpenCL devices you have in your computer
    for (ulong i = 0; i < context.ndevices(); i++) {
        const cv::ocl::Device &device = context.device(i);
        cout << "name:              " << device.name() << endl;
        cout << "available:         " << device.available() << endl;
        cout << "imageSupport:      " << device.imageSupport() << endl;
        cout << "OpenCL_C_Version:  " << device.OpenCL_C_Version() << endl;
        cout << endl;
    }
}