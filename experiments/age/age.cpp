//
// Created by tigershark on 11/10/19.
//
#include <exception>
#include <random>
#include <iostream>
#include <opencv2/imgcodecs.hpp>
#include "opencv4/opencv2/core/mat.hpp"
#include "opencv2/dnn.hpp"
#include <complex>
#include <iomanip>


using std::vector;
using namespace std;
using namespace cv;
using namespace cv::dnn;


int main(int argc, char **argv) {

    std::string caffeConfigFile = "../../../models/googlenet_aligned_age.prototxt";
    std::string caffeWeightFile = "../../../models/googlenet_aligned_age.caffemodel";

    string inputName = "data";
    string outputName = "loss3/loss3";

    Size insize = Size(224, 224);
    const Scalar meanColor = Scalar(104, 117, 123);

//    Net net = cv::dnn::readNetFromCaffe(caffeConfigFile, caffeWeightFile);
//    net.setPreferableBackend(DNN_BACKEND_VKCOM);
//    net.setPreferableTarget(DNN_TARGET_VULKAN);


//    caffeConfigFile = "../../../models/fer2013_mini_XCEPTION.prototxt";
//    caffeWeightFile = "../../../models/fer2013_mini_XCEPTION.caffemodel";

    caffeConfigFile = "../../../models/ybch14-github-FER-ResNet.prototxt";
    caffeWeightFile = "../../../models/ybch14-github-FER-ResNet.caffemodel";

    inputName = "data";
//    outputName = "predictions";
//            inputName = "data";
//            outputName = "fc8_em";
    outputName = "fc8";

//    insize = cv::Size(64, 64);
    Net net = cv::dnn::readNetFromCaffe(caffeConfigFile, caffeWeightFile);


    Mat image = imread("../../../img/faces/nahuel.jpg", IMREAD_GRAYSCALE);
    Mat inputBlob = cv::dnn::blobFromImage(image, 1.0, insize, meanColor);//.getUMat(ACCESS_RW, USAGE_ALLOCATE_SHARED_MEMORY);
//    Mat inputBlob = cv::dnn::blobFromImage(image, 1.0, insize, meanColor);

    net.setInput(inputBlob, inputName);

    cv::Mat detection = net.forward(outputName);
    //8 (0-2, 4-6, 8-13, 15-20, 25-32, 38-43, 48-53, 60-)
    cout << fixed << std::setprecision(2);
    cout << "0-2: " << detection.at<float>(0) * 100 << endl
         << "4-6: " << detection.at<float>(1) * 100 << endl
         << "8-13: " << detection.at<float>(2) * 100 << endl
         << "15-20: " << detection.at<float>(3) * 100 << endl
         << "25-32: " << detection.at<float>(4) * 100 << endl
         << "38-43: " << detection.at<float>(5) * 100 << endl
         << "48-53: " << detection.at<float>(6) * 100 << endl
         << "60+: " << detection.at<float>(7) << endl;
    cout << detection;

    return 0;
}

