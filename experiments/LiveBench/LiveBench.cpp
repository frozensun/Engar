
#include <iostream>
#include <filesystem>
#include <omp.h>
#include <csignal>
#include <memory>
#include "OrbDetector.h"
#include "FastDetector.h"
#include "ArbitraryImageEngine.h"
#include "PoolFrameProvider.hpp"
#include "opencv2/core/ocl.hpp"
#include "StaticFrameProvider.hpp"
#include "FaceEngine.hpp"
#include <NGT/Index.h>
#include <matchers/NGTMatcher.h>
#include <matchers/NMSMatcher.h>
#include <matchers/KGraphMatcher.h>
#include <matchers/FLANNMatcher.h>

using namespace std;
using namespace cv;
using namespace engar;

namespace fs = std::filesystem;

bool stop = false;

static void onMouse(int event, int x, int y, int flags, void *param) {
    if (event == EVENT_LBUTTONDOWN) {
        cout << "Stoping..." << endl;

        stop = true;
    }
}

void showOclSupport();

int main(int argc, char **argv) {
    omp_set_num_threads(12);

    try {
        int nap = 0;
        float trackingPointErrorThreshold = 15;
        int captureWidth = 800;
        int captureHeight = 600;

        Mat frame, image, objectMask;
        Mat rvec, tvec, inliers;
        Mat homography;
        vector<cv::Point2f> projectedAxes;
        vector<cv::Point3f> axis{
                cv::Point3f(128, 0, 0),
                cv::Point3f(0, -128, 0),
                cv::Point3f(0, 0, 128)
        };

        namedWindow("AR Engine Demo");
        setMouseCallback("AR Engine Demo", onMouse, nullptr);

        /// Fetch targets
        std::vector<string> targetImages;
        int counter = 0;
        int maxImages = 500;
        std::string targetsDirectoryPath = "../../../../data/test_images_art/";
        for (auto &p : fs::directory_iterator(targetsDirectoryPath)) {
            if (counter >= maxImages) break;
            targetImages.emplace_back(p.path().string());
            counter++;
        }

        std::sort(targetImages.begin(), targetImages.end());
        cout << "Found " << targetImages.size() << " target images." << endl;

        int startId = 0;
        int endId = 199;
        int framesPerTarget = 18;

        /// Construct a frame provider
        PoolFrameProvider *frameProvider = new PoolFrameProvider(targetImages, framesPerTarget, startId, endId, Size(captureWidth, captureHeight), true, 10);
        //////////////////////////////

        /// Construct a tracker
        LKTracker *tracker = new LKTracker(trackingPointErrorThreshold);
        ///////////////////////

        /// Construct a detector
//        shared_ptr<main::FastDetector> detector = shared_ptr<main::FastDetector>(new FastDetector(200));
        shared_ptr<engar::OrbDetector> detector = std::make_shared<engar::OrbDetector>(200);
        ////////////////////////

        /// Construct the engine
        ArbitraryImageEngine engine(frameProvider, detector, tracker, captureWidth, captureHeight);
        ////////////////////////

        /// Add targets
        for (const string &target : targetImages) {
            Mat temp = imread(target, IMREAD_GRAYSCALE);
            engine.AddTarget(temp);
        }

        engine.Train();
        ///////////////

        vector<cv::Point2f> corners;

        int correctCount = 0;
        int incorrectCount = 0;
        int nothingFoundCount = 0;

        float frameCount = 0;

        {
//            similarity::AnyParams params({"ef=12"});
//            ((NMSMatcher<int> *) detector->matcher.get())->SetQueryParams(params);
//            ((KGraphMatcher<float, kgraph::metric::l2> *) detector->matcher.get())->SearchParams.P = 32;
//            ((FLANNMatcher<float, ::flann::L2, float> *) detector->matcher.get())->searchParams.checks = 128;
//            detector->searchParams->setInt("checks", 128);
//            ((NGTMatcher *) detector->matcher.get())->searchEpsilon = 0.1;

            while (!stop) {
                frameCount++;
                int targetId = -1;
                engine.ProcessFrame(targetId, corners, homography, rvec, tvec);
                frame = frameProvider->lastFrame;

                if (targetId != -1) {
                    if (targetId == frameProvider->currentTargetId) {
                        correctCount++;
                    } else {
                        incorrectCount++;
                    }

                    /// Project axes acording to rvec and tvec
                    projectedAxes.clear();
                    projectPoints(axis, rvec, tvec, engine.K, noArray(), projectedAxes);
                    cout << "RVEC: " << rvec << endl << "TVEC: " << tvec << endl << endl;

                    /// Draw tracking points, the lines between the corners and the axes
                    Point2f offset(0, 0);
                    Utils::PrintEnclosingLines(frame, offset, corners);

                    Point2f center = (corners[0] + corners[1] + corners[2] + corners[3]) / 4;
                    Utils::Print3DAxis(frame, center, projectedAxes);
                    ////////////////////////////////////////////////////////////////////
                } else {
                    nothingFoundCount++;
                }

                if (frameCount == framesPerTarget * (endId - startId)) { stop = true; }

                imshow("AR Engine Demo", frame);
                waitKey(std::max(nap, 1));
            }
            frameProvider->Reset();
            cout << "Correct " << correctCount / frameCount << endl;
            cout << "Incorrect " << incorrectCount / frameCount << endl;
            cout << "Nothing " << nothingFoundCount / frameCount << endl;
            engine.PrintStatistics();
            engine.ResetStatistics();
            frameCount = 0;
            correctCount = 0;
            nothingFoundCount = 0;
            incorrectCount = 0;
            stop = false;
        }

        destroyAllWindows();
    } catch (exception &e) {
        cout << "Standard exception: " << e.what() << endl;
    }

    return EXIT_SUCCESS;
}

