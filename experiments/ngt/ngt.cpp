//
// Created by tigershark on 11/10/19.
//
#include <exception>
#include <random>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include "../../engar/headers/ArDetector.h"
#include "../../engar/headers/FastDetector.h"
#include "../../engar/headers/OrbDetector.h"
#include "engar/matchers/NGTMatcher.h"
#include "opencv4/opencv2/core/mat.hpp"


int main(int argc, char **argv) {
    try {
        NGT::Property property;
        property.setDefault();
        property.dimension = 64;
        property.distanceType = NGT::Property::DistanceType::DistanceTypeL2;
        property.indexType = NGT::Property::IndexType::GraphAndTree;
        property.objectType = NGT::Property::ObjectType::Float;
        cv::DescriptorMatcher *matcher = engar::NGTMatcher::create(property);
//        cv::DescriptorMatcher *matcher = new cv::BFMatcher(NORM_L2, false);


        unsigned char asdasd[] = {
                167, 49, 122, 83, 243, 253, 186, 80, 27, 225, 117, 143, 60, 220, 95, 160, 182, 59, 147, 85, 183, 248, 188, 238, 127, 9, 13, 227, 199, 135, 79,
                223,
                3, 216, 23, 155, 116, 135, 160, 102, 94, 57, 82, 82, 180, 24, 52, 96, 232, 57, 50, 17, 83, 188, 81, 245, 12, 48, 154, 66, 28, 8, 177, 140,
                3, 74, 195, 157, 98, 2, 40, 67, 60, 15, 66, 215, 13, 130, 176, 220, 102, 45, 43, 140, 2, 156, 133, 65, 20, 38, 250, 128, 5, 132, 33, 140,
                133, 92, 31, 158, 13, 205, 151, 232, 158, 194, 129, 14, 252, 255, 33, 105, 216, 121, 27, 67, 97, 39, 26, 255, 241, 146, 75, 58, 24, 59, 1, 186,
                1, 200, 23, 155, 116, 141, 160, 192, 78, 25, 82, 98, 180, 146, 168, 228, 232, 57, 58, 73, 3, 184, 67, 245, 4, 32, 186, 64, 20, 11, 49, 169,
//        151,  55, 126,  82, 214, 167, 187, 208, 249, 245, 125,   3, 120, 220, 127,  33, 190, 123,  19, 255, 182, 241,  60, 236,  91,  24,  15, 231, 197,   3,  47, 215,
//        41,  73,  21, 223, 109, 221, 103, 244, 207, 208, 100,  78, 247, 209,  49, 229, 200, 104,  58,  78, 193,  40, 122, 189,  39, 248,  69,  56,  90, 191,  80, 172,
//        43, 209, 113,  73, 101, 221, 241, 119, 205, 117, 252,  41, 139,  28, 150,  52, 204, 152, 214,  64, 207,  58, 116, 213, 103, 217,  16, 115,  87, 156, 214, 254,
//        169, 201,   5, 206,  93, 157, 230, 245, 205,  64, 164,  78, 250, 192,  54, 228, 200,  98, 202,  78, 199,  43,  58, 189,  55,  89,   4,  56,  90, 158, 216, 188,
//        135, 108, 167, 210,  87, 228, 187, 192, 253, 212,  89,   3, 104, 220,  63, 165, 191, 121,  19, 215, 166, 227,  62, 239,  57,  28, 143,  10, 199, 119,  55, 246,
//        158,  18, 151, 154, 111, 163, 176,  72, 159, 138, 209, 247, 204, 194,  97, 205, 247,  41, 175, 147,  36, 199,  10, 194, 148,  50, 203, 155, 158, 109,  49,   7,
//        54, 203,  24, 136,  14, 137, 134, 228, 141,   0, 152,  74, 166, 194, 239, 196, 250, 169,  74,  99,  79,  35,  68, 245, 176,  16, 142,  59,  29,  26,  57, 132,
//        135, 233, 167, 222, 221, 212, 238, 196, 255, 218, 208,  67, 104, 220, 125, 162, 233, 235, 147, 199, 231, 103,  58, 235,  51,  26, 143, 139,  85, 119, 119, 174,
//        255,  98,  98,   4,  79,  19, 206,  72,   1, 166, 209, 207,  67, 200, 223, 159, 124, 196, 199, 239, 204, 119, 204, 136,  18,  72, 201, 189, 111, 191, 122, 196,
//        150, 125, 167, 158,  77, 213, 158, 232, 223, 154, 145,  67, 108, 248,  61, 163, 248, 243, 155, 195, 231,  99,  26, 251,  51,  26, 207, 187,  25, 118,  59, 143,
//        211, 208,  24, 186,  43, 148,  88, 220, 191,  83, 129,  29, 140, 214,  42,  89, 231, 145, 253,  70, 102,   7, 154, 247, 147, 241,  90, 190,  24,  21, 171,  95,
//        77, 124,  24, 184, 234, 164,  94, 219,  58,   2, 104,  29,  29, 215,  43,  88, 183, 139,  19,   6,  70,  22, 154, 115, 215, 239, 122,  46, 152,  81, 171, 154,
//        19, 133, 236, 207,  70,  27, 239,  39,  15,  37, 207, 130, 227,  40,  60, 100, 123, 134,  59, 141,  88, 184, 125, 213, 191, 190, 158,  69,  53, 138, 245, 172,
//        13, 186, 146,  41, 225, 173, 111, 211,  51,  11, 124,  69,  61, 179,  33,  28,  51,  43,  23,  12, 130, 154,  16, 115,  69, 253,  54, 172, 186, 244, 227, 139,
//        19, 189,  83, 136, 242, 194, 115,  83,  63,  73, 124, 100,  60, 179, 162,  52,  54, 169,  22, 144, 179, 154,   0, 220, 190,  60,  34, 162, 190, 230,  35, 159
        };


        cv::Mat frame = imread("img/obj.png", cv::IMREAD_COLOR);
        cv::resize(frame, frame, cv::Size(800, 600), 0, 0, cv::INTER_LINEAR_EXACT);
        cv::UMat uframe = frame.getUMat(cv::ACCESS_READ);


        std::vector<cv::KeyPoint> keypoints_object;
        cv::UMat descriptors_object;
        engar::ArDetector *detector = new engar::FastDetector(100);
//        main::ArDetector *detector = new main::OrbDetector();
        detector->computeKeyPoints(frame, keypoints_object, descriptors_object);
//        Mat ble(descriptors_object.rows, descriptors_object.cols / 4, CV_32F);
//        ble.data = descriptors_object.getMat(cv::ACCESS_READ).data;
        matcher->add(descriptors_object);

//        for (uint i = 0; i < 10; i++) {
//            cv::Mat Descriptor;
//            Descriptor.create(1, 32, CV_8U);
//            for (uint j = 0; j < 32; j++) {
//                Descriptor.at<uchar>(0, j) = (i == j) ? 1 : 0;
//            }
//            matcher->add(Descriptor);
//        }

        matcher->train();

//        cv::Mat query;
//        query.create(10, 32, CV_8U);
//        for (uint i = 0; i < 10; i++) {
//            for (int j = 0; j < 32; j++) {
//                query.at<uchar>(i, j) = (i == j) ? 1 : 0;
//            }
//        }

        cv::Mat query = descriptors_object.getMat(cv::ACCESS_READ);
        query = query.clone();
//        cv::Mat asd = descriptors_object.getMat(cv::ACCESS_READ);
//        cv::Mat query;
//        asd.create(2, 32, CV_8U);
//        query.create(1, descriptors_object.cols, CV_8U);
//        for (int i = 0; i < 5; i++) {
//            for (int j = 0; j < descriptors_object.cols; j++) {
//                query.at<uchar>(i, j) = asdasd[i * 32 + j];
////                asd.at<uchar>(i,j) = query.at<uchar>(i,j);
//            }
//        }
//        query.row(3).copyTo(asd.row(0));
//        query.row(7).copyTo(asd.row(1));
//        query.row(11).copyTo(asd.row(2));
//        query.row(17).copyTo(asd.row(3));
//        query.row(24).copyTo(asd.row(4));


//        cout << query << endl;
        std::vector<std::vector<cv::DMatch> > matches;
        auto t1 = std::chrono::high_resolution_clock::now();
        matcher->knnMatch(query, matches, 2);
        cout << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1).count() << endl;
        cout << endl;


    } catch (std::exception &e) {

    }


    return 0;
}
