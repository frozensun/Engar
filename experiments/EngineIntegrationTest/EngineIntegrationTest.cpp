#define RAPIDJSON_HAS_STDSTRING 1

#include <iostream>
#include <filesystem>
#include <csignal>
#include <omp.h>
#include <chrono>
#include <map>

#include "OpenCvFrameProvider.h"
#include "Identity.h"
#include "PoolFrameProvider.hpp"
#include "IntegratedAREngine.h"
#include "StaticFrameProvider.hpp"
#include "FaceEngine.hpp"
#include <inferrers/GenderInferrer.h>
#include <inferrers/AgeInferrer.h>
#include <inferrers/FERInferrer.h>
#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"
#include "opencv2/core/ocl.hpp"
#include "LKTracker.h"

using namespace std;
using namespace cv;
using namespace engar;
using namespace std::chrono;

namespace fs = std::filesystem;

bool stop = false;

void testCaptureFrame(VideoFrameProvider *frameProvider);

void generateFaceEmbeddings(const string &facesFolder, const string &outputFileName, FaceDetector &faceDetector, engar::FaceRecognizer &faceRecognizer);

static void onMouse(int event, int x, int y, int flags, void *param) {
    if (event == EVENT_LBUTTONDOWN) {
        cout << "Stoping..." << endl;

        stop = true;
    }
}

void showOclSupport();

int main(int argc, char **argv) {
    omp_set_num_threads(12);

    try {
        int nap = 0;

        int deviceId = 0;
        int captureWidth = 800;
        int captureHeight = 600;

        Mat frame, image, objectMask;

        namedWindow("AR Engine Demo");
        setMouseCallback("AR Engine Demo", onMouse, nullptr);

        //// IMAGE SUB-PROCESS ENGINE ////////////////////////////////////////////////////////
        Mat rvec, tvec, inliers;
        int poiAmount = 100;
        float trackingPointErrorThreshold = 15;
        Mat homography;
        vector<cv::Point2f> corners;

        vector<cv::Point2f> projectedAxes;
        vector<cv::Point3f> axis{
                cv::Point3f(128, 0, 0),
                cv::Point3f(0, -128, 0),
                cv::Point3f(0, 0, 128)
        };

        /// Buscamos imagenes objetivo
        std::vector<string> targetImages;
        int counter = 0;
        int maxImages = 1;
        std::string targetsDirectoryPath = "../../../../data/test_images_art/";
        for (auto &p : fs::directory_iterator(targetsDirectoryPath)) {
            if (counter >= maxImages) break;
            targetImages.emplace_back(p.path().string());
            counter++;
        }
        targetImages.emplace_back("img/targets/target_1.jpg");
        targetImages.emplace_back("img/targets/target_2.jpg");
        targetImages.emplace_back("img/targets/target_3.jpg");
        targetImages.emplace_back("img/targets/target_4.jpg");

        std::sort(targetImages.begin(), targetImages.end());
        cout << "Found " << targetImages.size() << " target images." << endl;

        /// Construimos un proveedor de frames
        cout << endl << "Creating FrameProvider" << endl;
//        VideoFrameProvider *frameProvider = new StaticFrameProvider("img/scenepr45.png", Size(captureWidth, captureHeight));
//        VideoFrameProvider *frameProvider = new StaticFrameProvider("img/targets/target_2.jpg", Size(captureWidth, captureHeight), true, true);
//        RandomFrameProvider *frameProvider = new RandomFrameProvider(targetImages, 500, Size(captureWidth, captureHeight), true, true);
//        PoolFrameProvider *frameProvider = new PoolFrameProvider(targetImages, 18, 0, 99, Size(captureWidth, captureHeight), true, 10);
        VideoFrameProvider *frameProvider = new OpenCvFrameProvider(deviceId, captureWidth, captureHeight);
        //////////////////////////////


        /// Creamos el engine integrado
        vector<map<string, vector<pair<string, float >> >> infered;


        bool facesFound = false;
        vector<TrackedFace *> trackedFaces;
        cout << endl << "Init done. Starting main loop..." << endl;
        int imageTargetId = -1;
        IntegratedAREngine engine(captureWidth, captureHeight, frameProvider);

        /// Generamos los descriptores de rostros si hace falta
//        generateFaceEmbeddings("img/faces", "face_embeddings_dlib.json", *engine.faceEngine->detector.get(), *engine.faceEngine->recognizer.get());

        engine.LoadFaceEmbeddings("models/face_embeddings_dlib.json");
        engine.LoadImagesDirectly(targetImages);

        engine.Train();

        /// Agregamos los inferidores de informacion
        string ageInferrerName = "age";
        string genderInferrerName = "gender";
        string ferInferrerName = "fer";

        engine.AddInferrer(new AgeInferrer(ageInferrerName));
        engine.AddInferrer(new GenderInferrer(genderInferrerName));
        engine.AddInferrer(new FERInferrer(ferInferrerName));
        ///////////////

        waitKey(100);
        ///////////////////////////////////////////////////////////////////

        while (!stop) {

            engine.ProcessFrame(imageTargetId, corners, homography, rvec, tvec, facesFound, trackedFaces);

            frame = frameProvider->lastFrame;
            if (imageTargetId > -1) {
                /// Project axes acording to rvec and tvec
                projectedAxes.clear();
                projectPoints(axis, rvec, tvec, engine.K, noArray(), projectedAxes);
//                cout << "RVEC: " << rvec << endl << "TVEC: " << tvec << endl << "Center: " << center << endl << endl;

                /// Draw tracking points, the lines between the corners and the axes
                Point2f offset(0, 0);
                Utils::PrintEnclosingLines(frame, offset, corners);

                Point2f center = (corners[0] + corners[1] + corners[2] + corners[3]) / 4;
                Utils::Print3DAxis(frame, center, projectedAxes);
                ////////////////////////////////////////////////////////////////////
            }

            if (facesFound) {
                /// Tenemos caras, mostrarlas
                for (const auto &trackedFace : trackedFaces) {
                    const TrackedFace &face = *trackedFace;
                    rectangle(frame, face.Bbox, Utils::Colors.Blue, 3);
                    putText(frame, face.CurrentIdentity.Name, face.Bbox.tl(), FONT_HERSHEY_COMPLEX, 1, Utils::Colors.Black);

                    auto ageInference = face.InferredInfo.find(ageInferrerName);
                    if (ageInference != face.InferredInfo.end()) {
                        const vector<pair<string, float>> &ageData = ageInference->second;
                        auto point = face.Bbox.br();
//                        auto point = cv::Point2f (0,0);
                        point.x = face.Bbox.tl().x;
                        for (const auto &dataEntry : ageData) {
                            if (dataEntry.second > 0.00) {
                                point.y += 14;
                                putText(frame, dataEntry.first + ": " + to_string(dataEntry.second), point, FONT_HERSHEY_COMPLEX, 0.5,
                                        Utils::Colors.Green);
                            }
                        }
                    }

                    auto ferInference = face.InferredInfo.find(ferInferrerName);
                    if (ferInference != face.InferredInfo.end()) {
                        const vector<pair<string, float>> &ferData = ferInference->second;
//                        auto point = face.Bbox.br();
//                        point.x = face.Bbox.tl().x;
                        auto point = face.Bbox.tl();
                        point.x = face.Bbox.br().x;
                        point.y += 64;
                        putText(frame, ferData[0].first + " : " + to_string(ferData[0].second), point, FONT_HERSHEY_COMPLEX, 0.5, Utils::Colors.Green);
                        point.y += 16;
                        putText(frame, ferData[1].first + " : " + to_string(ferData[1].second), point, FONT_HERSHEY_COMPLEX, 0.5, Utils::Colors.Green);
                        point.y += 16;
                        putText(frame, ferData[2].first + " : " + to_string(ferData[2].second), point, FONT_HERSHEY_COMPLEX, 0.5, Utils::Colors.Green);
                        point.y += 16;
                        putText(frame, ferData[3].first + " : " + to_string(ferData[3].second), point, FONT_HERSHEY_COMPLEX, 0.5, Utils::Colors.Green);
                        point.y += 16;
                        putText(frame, ferData[4].first + " : " + to_string(ferData[4].second), point, FONT_HERSHEY_COMPLEX, 0.5, Utils::Colors.Green);
                    }

                    auto genderInference = face.InferredInfo.find(genderInferrerName);
                    if (genderInference != face.InferredInfo.end()) {
                        const vector<pair<string, float>> &genderData = genderInference->second;
                        auto point = face.Bbox.tl();
                        point.x = face.Bbox.br().x;
                        point.y -= 14;
                        putText(frame, genderData[0].first + " : " + to_string(genderData[0].second), point, FONT_HERSHEY_COMPLEX, 0.5, Utils::Colors.Green);
                        point.y -= 16;
                        putText(frame, genderData[1].first + " : " + to_string(genderData[1].second), point, FONT_HERSHEY_COMPLEX, 0.5, Utils::Colors.Green);
                    }
                }
            }

            if (frame.rows > 0) {
                imshow("AR Engine Demo", frame);
            } else {
                cout << "WARNING: No video input" << endl;
            }

            waitKey(std::max(nap, 1));
        }

        engine.PrintStatistics();

    } catch (exception &e) {
        cout << "Standard exception: " << e.what() << endl;
    }
    destroyAllWindows();

    return EXIT_SUCCESS;
}

void testCaptureFrame(VideoFrameProvider *frameProvider) {
    UMat temp;
    auto t1 = steady_clock::now();
    double gf = (double) getTickCount();
    frameProvider->GrabFrame(temp);
    cout << "GrabFrame took: " << ((double) getTickCount() - gf) * 1000 / getTickFrequency() << "\t"
         << duration_cast<microseconds>(steady_clock::now() - t1).count() / 1000 << " ms" << endl;
    imshow("AR Engine Demo", temp);
}

void generateFaceEmbeddings(const string &facesFolder, const string &outputFileName, FaceDetector &faceDetector, engar::FaceRecognizer &faceRecognizer) {
    std::vector<Identity> identities;

    auto iterator = std::filesystem::directory_iterator(facesFolder);
    for (auto &p : iterator) {
        if (p.is_directory()) continue; // Skip directories

        std::cout << "Processing: " << p << std::endl;

        const filesystem::path &target = p.path();
        Mat temp = imread(target.generic_string());

        if (temp.cols == 0 || temp.rows == 0) {
            cout << "Invalid face image path: " << p << endl;
            continue;
        }

        UMat uTemp = temp.getUMat(ACCESS_READ, USAGE_ALLOCATE_HOST_MEMORY);
        vector<Rect> faces;

        faceDetector.detect(uTemp, faces);

        if (faces.empty()) continue; // No faces found

        Identity identity;
        identity.Descriptor = faceRecognizer.DescribeFace(uTemp, faces[0]);

        string fileName = target.filename().string();
        identity.Name = fileName.substr(0, fileName.find_last_of('.'));

        identities.emplace_back(move(identity));
    }

    rapidjson::StringBuffer sb;
    rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(sb);
    writer.StartArray();
    for (Identity &id : identities) {
        id.Serialize(writer);
    }
    writer.EndArray();

    ofstream file(outputFileName);
    file << sb.GetString();
    file.close();
}

void showOclSupport() {
    if (!cv::ocl::haveOpenCL()) {
        cout << "OpenCL is not available..." << endl;
        return;
    }

    cout << "OPENCV_OPENCL_ENABLE_MEM_USE_HOST_PTR = " << getenv("OPENCV_OPENCL_ENABLE_MEM_USE_HOST_PTR") << endl;

    cv::ocl::Context context;
    if (!context.create(cv::ocl::Device::TYPE_GPU)) {
        cout << "Failed creating the context..." << endl;
        return;
    }

    cout << context.ndevices() << " GPU devices are detected." << endl; //This bit provides an overview of the OpenCL devices you have in your computer
    for (ulong i = 0; i < context.ndevices(); i++) {
        const cv::ocl::Device &device = context.device(i);
        cout << "name:              " << device.name() << endl;
        cout << "available:         " << device.available() << endl;
        cout << "imageSupport:      " << device.imageSupport() << endl;
        cout << "OpenCL_C_Version:  " << device.OpenCL_C_Version() << endl;
        cout << endl;
    }

    auto backends = cv::dnn::getAvailableBackends();
    for (auto b : backends) {
        cout << "Backend: " << b.first << " Target: " << b.second << endl;
    }
}
