//
// Created by tigershark on 23/8/19.
//
#include "opencv2/face/facemark_train.hpp"
#include <stdio.h>
#include <fstream>
#include <sstream>
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/face.hpp"
#include "opencv2/dnn.hpp"
#include "../engar/headers/OpenCvFrameProvider.h"

#include <iostream>
#include <string>
#include <ctime>

using namespace std;
using namespace cv;
using namespace cv::face;
using namespace cv::dnn;


//bool stop = false;
//
//static void onMouse(int event, int x, int y, int flags, void *param) {
//    if (event == EVENT_LBUTTONDOWN) {
//        cout << "Stoping..." << endl;
//
//        stop = true;
//    }
//}


const String &outputName = "detection_out";
const String &inputName = "data";
const float confidenceThreshold = 0.98;
const Size insize = Size(300, 300);
const Scalar meanColor = Scalar(104, 117, 123);

bool myDetector(InputArray image, OutputArray faces, Net &net) {
//    Mat gray;
//
//    if (image.channels() > 1)
//        cvtColor(image, gray, COLOR_BGR2GRAY);
//    else
//        gray = image.getMat().clone();
//
//    equalizeHist(gray, gray);
//
//    std::vector<Rect> faces_;
//    face_cascade->detectMultiScale(gray, faces_, 1.4, 2, CASCADE_SCALE_IMAGE, Size(30, 30));
//    Mat(faces_).copyTo(faces);

    UMat inputBlob = cv::dnn::blobFromImage(image, 1.0, insize, meanColor).getUMat(ACCESS_RW, USAGE_ALLOCATE_SHARED_MEMORY);

    net.setInput(inputBlob, inputName);

    cv::Mat detection = net.forward(outputName);

    cv::Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());
    vector<Rect> out;

    for (int i = 0; i < detectionMat.rows; i++) {
        float confidence = detectionMat.at<float>(i, 2);

        if (confidence > confidenceThreshold) {
            int x1 = static_cast<int>(detectionMat.at<float>(i, 3) * 640);
            int y1 = static_cast<int>(detectionMat.at<float>(i, 4) * 480);
            int x2 = static_cast<int>(detectionMat.at<float>(i, 5) * 640);
            int y2 = static_cast<int>(detectionMat.at<float>(i, 6) * 480);
            Rect rect = Rect(cv::Point(x1, y1), cv::Point(x2, y2));

            if (rect.area() > 1) {
                out.emplace_back(rect);
            }
        }
    }

    Mat(out).copyTo(faces);
    return true;
}


int main2(int argc, char **argv) {

    const String cascade_path = "models/haarcascades/haarcascade_frontalface_default.xml",
            eyes_cascade_path = "models/haarcascades/haarcascade_eye.xml",
            images_path = "/media/Data/TigerShark/Academico/Doc/Tesis/Soft/data/lfpw/images_train.txt",
            annotations_path = "/media/Data/TigerShark/Academico/Doc/Tesis/Soft/data/lfpw/annotation_train.txt",
            test_images_path = "/media/Data/TigerShark/Academico/Doc/Tesis/Soft/data/lfpw/annotation_test.txt";

    int deviceId = 0;
    int captureWidth = 640;
    int captureHeight = 480;

    const std::string caffeConfigFile = "models/deploy.prototxt";
    const std::string caffeWeightFile = "models/opencv_face_detector_fp16.caffemodel";

    Net net = cv::dnn::readNetFromCaffe(caffeConfigFile, caffeWeightFile);
    net.setPreferableTarget(DNN_TARGET_OPENCL);

    UMat frame, colorFrame, image, objectMask;

    namedWindow("AR Engine Demo", 1);
    setMouseCallback("AR Engine Demo", onMouse, nullptr);

    engar::VideoFrameProvider *frameProvider = new engar::OpenCvFrameProvider(deviceId, captureWidth, captureHeight);

    //pass the face cascade xml file which you want to pass as a detector
//    CascadeClassifier face_cascade;
//    face_cascade.load(cascade_path);

    FacemarkLBF::Params params;
    Ptr<FacemarkLBF> facemark = FacemarkLBF::create(params);
    facemark->setFaceDetector((FN_FaceDetector) myDetector, &net);
    facemark->loadModel("./lbfmodel.yaml");
    cout << "Loaded model" << endl;


    vector<Rect> faces;
    vector<vector<Point2f> > shapes;

    while (!stop) {
        frameProvider->GrabFrame(colorFrame);
        cvtColor(colorFrame, frame, COLOR_BGR2GRAY);

//        resize(img, img, Size(640, 460), 0, 0, INTER_LINEAR_EXACT);
//        faces.clear();
//        facemark->getFaces(colorFrame, faces);

        UMat colorMini;
        resize(colorFrame, colorMini, insize, 0, 0, INTER_LINEAR_EXACT);

        UMat inputBlob = cv::dnn::blobFromImage(colorMini, 1.0, insize, meanColor).getUMat(ACCESS_RW);

        net.setInput(inputBlob, inputName);

        cv::Mat detection = net.forward(outputName);

        cv::Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());

        faces.clear();

        for (int i = 0; i < detectionMat.rows; i++) {
            float confidence = detectionMat.at<float>(i, 2);

            if (confidence > confidenceThreshold) {
                int x1 = static_cast<int>(detectionMat.at<float>(i, 3) * 640);
                int y1 = static_cast<int>(detectionMat.at<float>(i, 4) * 480);
                int x2 = static_cast<int>(detectionMat.at<float>(i, 5) * 640);
                int y2 = static_cast<int>(detectionMat.at<float>(i, 6) * 480);
                Rect rect = Rect(cv::Point(x1, y1), cv::Point(x2, y2));

                if (rect.area() > 1) {
                    faces.emplace_back(rect);
                }
            }
        }

        // Check if faces detected or not
        // Helps in proper exception handling when writing images to the directories.
        if (!faces.empty()) {
            for (const auto &face : faces) {
                cv::rectangle(colorFrame, face, Scalar(255, 0, 0));
            }

//            shapes.clear();
//            if (facemark->fit(colorFrame, faces, shapes)) {
//                for (unsigned long i = 0; i < faces.size(); i++) {
//                    for (auto &k : shapes[i])
//                        cv::circle(colorFrame, k, 5, cv::Scalar(0, 0, 255), FILLED);
//                }
//            }
        } else {
            cout << "Faces not detected." << endl;
        }

        imshow("AR Engine Demo", colorFrame);

        waitKey(1);
    }


}


