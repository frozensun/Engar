//
// Created by tigershark on 11/10/19.
//
#include <exception>
#include <random>
#include <iostream>
#include <complex>

#include "opencv4/opencv2/core/mat.hpp"
#include <opencv2/imgcodecs.hpp>
#include "ArDetector.h"
#include <matchers/NMSMatcher.h>
#include <NGT/Index.h>
#include <matchers/NGTMatcher.h>
#include <matchers/MRPTMatcher.h>
#include <matchers/AnnoyMatcher.h>
#include <matchers/KGraphMatcher.h>
#include <opencv2/core/ocl.hpp>
#include <chrono>
#include <matchers/FLANNMatcher.h>
#include "OrbDetector.h"

#include "init.h"
#include "params.h"
#include "knnquery.h"
#include "methodfactory.h"
#include "spacefactory.h"
#include "space.h"

using std::vector;

using similarity::initLibrary;
using similarity::AnyParams;
using similarity::Index;
using similarity::MethodFactoryRegistry;
using similarity::SpaceFactoryRegistry;
using similarity::AnyParams;
using similarity::Space;
using similarity::ObjectVector;
using similarity::Object;
using similarity::KNNQuery;
using similarity::KNNQueue;


template<typename T>
vector<vector<T>> read_record(string fileName, unsigned long maxRows = 1000000, unsigned long offset = 0);

template<>
vector<vector<unsigned char>> read_record(string fileName, unsigned long maxRows, unsigned long offset);

void computeGolds(vector<cv::Mat> &trainSamples, vector<cv::Mat> &testSamples, vector<vector<int>> &golds, vector<vector<float>> &dists, int k);

void showOclSupport();

class BenchResults {
public:
    BenchResults(string name, int searchSize, int sampleSize, int k) {
        this->k = k;
        this->name = name;
        this->searchSize = searchSize;
        this->sampleSize = sampleSize;
        this->recallCounts.resize(k);
        this->errorRelativeDistanceSums.resize(k);
        this->errorDistanceSums.resize(k);
        this->distanceSums.resize(k);
    }

    string name;
    chrono::microseconds trainTime = std::chrono::microseconds::zero();
    chrono::microseconds knnTime = std::chrono::microseconds::zero();
    int searchSize = 0;
    int sampleSize = 0;
    vector<int> recallCounts;
    int recall2Counts = 0;
    vector<float> errorRelativeDistanceSums;
    vector<float> errorDistanceSums;
    vector<float> distanceSums;
    int errorOffsetSum = 0;
    int offsetSum = 0;
    int k;

    void ResetTestResults() {
        this->knnTime = std::chrono::microseconds::zero();

        this->recallCounts.clear();
        this->recallCounts.resize(k);

        this->errorRelativeDistanceSums.clear();
        this->errorRelativeDistanceSums.resize(k);

        this->errorDistanceSums.clear();
        this->errorDistanceSums.resize(k);

        this->distanceSums.clear();
        this->distanceSums.resize(k);

        this->errorOffsetSum = 0;
        this->offsetSum = 0;
        this->recall2Counts = 0;
    }
};

ostream &operator<<(ostream &os, const BenchResults &dt) {
    os << endl;
    os << fixed << std::setprecision(3);
    os << dt.name << ": " << endl;
    os << "  Total Time: " << chrono::duration_cast<chrono::seconds>(dt.trainTime + dt.knnTime).count() << "s" << endl;
    os << "  Total knn Time: " << chrono::duration_cast<chrono::milliseconds>(dt.knnTime).count() << "ms" << endl;
    os << "  Train Time: " << chrono::duration_cast<chrono::seconds>(dt.trainTime).count() << "s" << endl;
    os << "  Avg Time per Sample: " << chrono::duration_cast<chrono::milliseconds>(dt.knnTime).count() / (float) dt.searchSize << "ms" << endl;
    os << "  Avg Time per Query: " << chrono::duration_cast<chrono::milliseconds>(dt.knnTime).count() / (float) (dt.searchSize * dt.sampleSize) << "ms" << endl;

    os << "          Recall\t";
    for (int i = 0; i < dt.recallCounts.size(); i++) {
        auto const &rec = dt.recallCounts[i];
        os << rec / (float) (dt.searchSize * dt.sampleSize) << "\t";
    }
    os << endl;
    os << "          Recall 2NN\t";
    os << dt.recall2Counts / (float) (dt.searchSize * dt.sampleSize) << "\t";
    os << endl;
    os << "  Avg distance \t";
    for (int i = 0; i < dt.distanceSums.size(); i++) {
        auto const &rec = dt.distanceSums[i];
        os << rec / (float) (dt.searchSize * dt.sampleSize) << "\t";
    }
    os << endl;
    os << "  Error distance %\t";
    for (int i = 0; i < dt.errorRelativeDistanceSums.size(); i++) {
        auto const &rec = dt.errorRelativeDistanceSums[i];
        os << rec / (float) ((dt.searchSize * dt.sampleSize) - dt.recallCounts[i]) << "\t";
    }
    os << endl;
    os << "  Error distance \t";
    for (int i = 0; i < dt.errorDistanceSums.size(); i++) {
        auto const &rec = dt.errorDistanceSums[i];
        os << rec / (float) ((dt.searchSize * dt.sampleSize) - dt.recallCounts[i]) << "\t";
    }
    os << endl;
    os << "  Avg Error Index Offset: " << dt.errorOffsetSum / (float) ((dt.searchSize * dt.sampleSize) - dt.recallCounts[1]);
    os << endl;
    os << "  Avg Index Offset: " << dt.offsetSum / (float) (dt.searchSize * dt.sampleSize);
    os << endl << endl;
    return os;
}

void testMatcher(vector<cv::Mat> &testSamples, int searchSize, cv::DescriptorMatcher *matcher, BenchResults &r, int k, vector<vector<int>> &golds,
                 vector<vector<float>> &dists) {
    omp_set_num_threads(1);
    for (int i = 0; i < searchSize; i++) {
        vector<vector<cv::DMatch> > matches;
        int sampleSize = 200;// testSamples[i].rows;
        matches.resize(sampleSize);

        auto t2 = std::chrono::high_resolution_clock::now();
        matcher->knnMatch(testSamples[i], matches, k);
        r.knnTime += chrono::duration_cast<chrono::microseconds>(std::chrono::high_resolution_clock::now() - t2);

        for (const auto &match : matches) {
            bool st = false;
            bool nd = false;
            for (int nn = 0; nn < match.size(); nn++) {
                auto &m = match[nn];

                // Get the correct nn
                int absoluteQueryIdx = m.queryIdx + i * sampleSize;
                auto &gold = golds[absoluteQueryIdx];

                // Compute the found nn in terms on absolute indexing (trainIdx is in a per img indexing)
                int found = m.trainIdx + m.imgIdx * sampleSize;


                if (gold[nn] == found) {
                    // If nn is correct, increase the recall count
                    r.recallCounts[nn] += 1;

                    if (nn == 0) st = true;
                    if (nn == 1) nd = true;
                } else {
                    // If we made a mistake, compute some statistics
                    if (nn <= 1) {
                        // Compute index distance to the correct nn
                        for (int j = nn + 1; j < gold.size(); j++) {
                            if (gold[j] == found) break;
                            r.errorOffsetSum++;
                        }
                    }
                    float d = dists[absoluteQueryIdx][nn];
                    r.errorRelativeDistanceSums[nn] += abs((m.distance - d) / d);
                    r.errorDistanceSums[nn] += m.distance;

                    if (nn <= 1) {
                        // Compute index distance to the correct nn
                        for (int j = nn + 1; j < gold.size(); j++) {
                            r.offsetSum++;
                            if (gold[j] == found) break;
                        }
                    }
                }
                r.distanceSums[nn] += m.distance;


            }
            r.recall2Counts += (st && nd) ? 1 : 0;
        }
    }

    omp_set_num_threads(12);
}

void trainMatcher(vector<cv::Mat> &trainSamples, cv::DescriptorMatcher *matcher, BenchResults &r) {
    omp_set_num_threads(12);
    for (const auto &s :  trainSamples) {
        matcher->add(s);
    }
    auto t1 = std::chrono::high_resolution_clock::now();
    matcher->train();
    r.trainTime = chrono::duration_cast<chrono::microseconds>(std::chrono::high_resolution_clock::now() - t1);
    omp_set_num_threads(1);
}

typedef unsigned char dType;
#define BYTE_COUNT 32
//typedef unsigned long long dType;
//typedef float dType;

void toMatVector(int samplesAmount, int sampleSize, vector<vector<dType>> &train, vector<cv::Mat> &samples);

void toMatVectorULL(int samplesAmount, int sampleSize, vector<vector<unsigned char>> &train, vector<cv::Mat> &samples);

void toMatVectorULL(int samplesAmount, int sampleSize, vector<vector<float>> &train, vector<cv::Mat> &samples);

//#define HNSW
//#define FLANN
//#define ANNG
//#define KGRAPH
//#define ANNOY
//#define MRPT
#define FLANN_CUSTOM

int main(int argc, char **argv) {
//    showOclSupport();
    cv::ocl::setUseOpenCL(true);
//    cout << cv::ocl::Context::getDefault().ndevices() << endl;
//    cout << cv::ocl::Device::getDefault().maxComputeUnits() << endl;

    int samplesAmount = 5000;
    int sampleSize = 200;
    int searchSize = 50;
    int k = 2;
    float change = (is_same<float, dType>::value) ? 0.05 : 0.15;
    int changeType = 0;

    if (argc != 7) {
        cout << "SamplesAmount SampleSize SearchSize k" << endl;
    } else {
        samplesAmount = atoi(argv[1]);
        sampleSize = atoi(argv[2]);
        searchSize = atoi(argv[3]);
        k = atoi(argv[4]);
        change = atof(argv[5]);
        changeType = atoi(argv[6]);
    }

    // ISO 8601: %Y-%m-%d %H:%M:%S, e.g. 2017-07-31 00:42:00+0200.
    auto time = std::time(nullptr);
    std::cout << std::put_time(std::gmtime(&time), "%F %T%z") << endl;

    cout << "Samples Amount: " << samplesAmount << endl;
    cout << "Sample Size: " << sampleSize << endl;
    cout << "Search Size: " << searchSize << endl;
    cout << "k: " << k << endl;
    cout << "change: " << change << endl;
    cout << "change type " << changeType << endl;
    std::string dist = (is_same<float, dType>::value) ? "L2" : "Hamming";
    cout << "distance: " << dist << endl;


    vector<cv::Mat> trainSamples;
    // SIFT10M_extract_
    vector<vector<dType>> train = read_record<dType>("../../../../../data/train.csv", samplesAmount * sampleSize, 0);
    toMatVector(samplesAmount, sampleSize, train, trainSamples);

    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_real_distribution<> random(0, 1); // distribution in range [1, 6]
    std::uniform_int_distribution<> sign(0, 1); // distribution in range [1, 6]

    float c = change * 255;
    vector<cv::Mat> testSamples;
    for (int q = 0; q < searchSize; q++) {
        auto &s = trainSamples[q];
        cv::Mat sample;
        s.copyTo(sample);
        for (int i = 0; i < sample.rows; i++) {

            if (is_same<float, dType>::value) {
                auto row = sample.ptr<float>(i);
                for (int j = 0; j < sample.cols; j++) {

                    if (changeType == 0) {
                        // All dimensions change a bit
                        float m = (sign(rng) ? 1 : -1) * c;
                        row[j] = max(min(row[j] + m, (float) 255.0), (float) 0.0);
                    } else {
                        // Only "change %" dimension change randomly
                        if (random(rng) < change) {
                            row[j] = random(rng) * 255;
                        }
                    }
                }
            } else {
                auto row = sample.ptr<unsigned char>(i);
                for (int j = 0; j < sample.cols; j++) {
                    std::bitset<8> bits(row[j]);

                    for (int b = 0; b < 8; b++) {
                        if (random(rng) < change) {
                            bits.flip(b);
                        }
                    }

                    row[j] = static_cast<unsigned char>(bits.to_ulong());
                }
            }
        }
        testSamples.emplace_back(sample);
    }

//    vector<cv::Mat> testSamples;
//    vector<vector<dType>> test = read_record<dType>("../../../../../data/SIFT10M_extract_test.csv", samplesAmount * sampleSize);
//    toMatVector(searchSize, sampleSize, test, testSamples); // Only 10.000 elements in test file

//    vector<vector<int>> gold = read_record<int>("../../../../../data/neighbors.csv", searchSize * sampleSize);
//    vector<vector<float>> dists = read_record<float>("../../../../../data/distances.csv", searchSize * sampleSize);

    vector<vector<int>> gold;
    vector<vector<float>> dists;
    computeGolds(trainSamples, testSamples, gold, dists, 100);

    /// Brute Force /////////////////////////////////////////////////////////////////////////////////
//    {
//        BenchResults bResult("Brute Force", searchSize, sampleSize, k);
//        cv::DescriptorMatcher *matcher = new cv::BFMatcher((is_same<float, dType>::value) ? cv::NORM_L2 : cv::NORM_HAMMING, false);
//        trainMatcher(trainSamples, matcher, bResult);
//        testMatcher(testSamples, searchSize, matcher, bResult, k, gold, dists);
//        cout << bResult;
//    }
    /////////////////////////////////////////////////////////////////////////////////////////////////

    /// FLANN_CUSTOM /////////////////////////////////////////////////////////////////////////////////
#ifdef FLANN_CUSTOM
    {
        BenchResults bResult("FLANN", searchSize, sampleSize, k);
        ::flann::IndexParams indexParams;
        ::flann::SearchParams searchParams;
        if (is_same<float, dType>::value) {
            indexParams = ::flann::AutotunedIndexParams(0.8, 0.001, 0, 0.2);
            searchParams = ::flann::SearchParams();

            auto *matcher = new engar::FLANNMatcher<float, ::flann::L2, float>(indexParams, searchParams);

            trainMatcher(trainSamples, matcher, bResult);

            testMatcher(testSamples, searchSize, matcher, bResult, k, gold, dists);
            cout << bResult;
            bResult.ResetTestResults();

            matcher->clear();
            delete matcher;
        } else {
            std::vector<cv::Mat> train64;
            std::vector<cv::Mat> test64;

            if (is_same<float, dType>::value) {
                train64 = trainSamples;
                test64 = testSamples;
            } else {
                train64 = trainSamples;
                test64 = testSamples;

//                toMatVectorULL(samplesAmount, sampleSize, train, train64);
//
//                for (int q = 0; q < searchSize; q++) {
//                    auto &s = testSamples[q];
//                    cv::Mat sample = cv::Mat(cv::Size(BYTE_COUNT / 8, sampleSize), CV_64F);
//                    sample.data = s.data;
//                    test64.emplace_back(sample);
//                }
            }

            //int branching = 32, cv::flann::flann_centers_init_t centers_init = cv::flann::FLANN_CENTERS_RANDOM, int trees = 4, int leaf_size = 100
//            (int branching = 32, flann_centers_init_t centers_init = FLANN_CENTERS_RANDOM, int trees = 4, int leaf_max_size = 100)
            indexParams = ::flann::HierarchicalClusteringIndexParams(32, ::flann::FLANN_CENTERS_RANDOM, 4, 100);

//            indexParams = ::flann::LshIndexParams(3, 8, 2);
//            indexParams = ::flann::LshIndexParams(12, 20, 2);
            searchParams = ::flann::SearchParams();
            searchParams.checks = 8;

            auto *matcher = new engar::FLANNMatcher<unsigned char, ::flann::HammingPopcnt, int>(indexParams, searchParams);
            trainMatcher(train64, matcher, bResult);

            searchParams.cores = 1;

            searchParams.checks = 8;
            testMatcher(test64, searchSize, matcher, bResult, k, gold, dists);
            cout << bResult << endl;
            bResult.ResetTestResults();

            matcher->clear();
            delete matcher;
        }
    }
#endif
    /////////////////////////////////////////////////////////////////////////////////////////////////

    /// FLANN OPENCV ///////////////////////////////////////////////////////////////////////////////////////
#ifdef FLANN
    {
        BenchResults bResult("FLANN", searchSize, sampleSize, k);
        Ptr<::flann::IndexParams> indexParams;
        Ptr<::flann::SearchParams> queryParams;
        if (is_same<float, dType>::value) {
            indexParams = Ptr<::flann::IndexParams>(new ::flann::AutotunedIndexParams(0.8, 0.001, 0, 0.2));
            queryParams = Ptr<::flann::SearchParams>(new ::flann::SearchParams());
        } else {
            //int branching = 32, cv::flann::flann_centers_init_t centers_init = cv::flann::FLANN_CENTERS_RANDOM, int trees = 4, int leaf_size = 100
            //indexParams = Ptr<::flann::IndexParams>(new ::flann::HierarchicalClusteringIndexParams(32, cv::flann::FLANN_CENTERS_RANDOM, 4, 10));
            indexParams = Ptr<::flann::IndexParams>(new ::flann::LshIndexParams(3, 8, 2));
//            indexParams->setInt("cores", 12);
            queryParams = Ptr<::flann::SearchParams>(new ::flann::SearchParams());
        }

        FlannBasedMatcher *matcher = new FlannBasedMatcher(indexParams, queryParams);

        trainMatcher(trainSamples, matcher, bResult);

        testMatcher(testSamples, searchSize, matcher, bResult, k, gold, dists);
        cout << bResult << endl;
        bResult.ResetTestResults();

        matcher->clear();
        delete matcher;
    }
#endif
    /////////////////////////////////////////////////////////////////////////////////////////////////

    /// HNSW ////////////////////////////////////////////////////////////////////////////////////////
#ifdef HNSW
    {
        BenchResults bResult("hnsw", searchSize, sampleSize, k);
        if (is_same<float, dType>::value) {
            main::NMSMatcher<float> *matcher;
            matcher = new main::NMSMatcher<float>("hnsw", "l2",
                                                   new similarity::AnyParams({"efConstruction=2000", "M=10"}),
                                                   new similarity::AnyParams({"ef=10"}));

            trainMatcher(trainSamples, matcher, bResult);

            similarity::AnyParams queryParams({"ef=64"});

            queryParams = similarity::AnyParams({"ef=10"});
            matcher->SetQueryParams(queryParams);
            testMatcher(testSamples, searchSize, matcher, bResult, k, gold, dists);
            cout << bResult << endl;
            bResult.ResetTestResults();

        } else {
            main::NMSMatcher<int> *matcher = new main::NMSMatcher<int>("hnsw", "bit_hamming",
//                                                                         new similarity::AnyParams({"efConstruction=2000", "M=10"}),
                                                                         new similarity::AnyParams({"efConstruction=1600", "M=24", "post=2"}),
                                                                         new similarity::AnyParams({"ef=300"}));

            trainMatcher(trainSamples, matcher, bResult);

            similarity::AnyParams queryParams({"ef=128"});
            matcher->SetQueryParams(queryParams);
            testMatcher(testSamples, searchSize, matcher, bResult, k, gold, dists);
            cout << bResult << endl;
            bResult.ResetTestResults();

            matcher->clear();
            delete matcher;
        }
    }
#endif
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /// NGT ////////////////////////////////////////////////////////////////////////////////////////
#ifdef ANNG
    {
        BenchResults bResult("ANNG", searchSize, sampleSize, k);
        NGT::Property property;
        property.setDefault();
        property.dimension = 128;
        if (is_same<float, dType>::value) {
            property.distanceType = NGT::Property::DistanceType::DistanceTypeL2;
            property.objectType = NGT::Property::ObjectType::Float;
            property.edgeSizeForCreation = 20;
            property.outgoingEdge = 4;
            property.incomingEdge = 8;
        } else {
            property.dimension = BYTE_COUNT;
            property.distanceType = NGT::Property::DistanceType::DistanceTypeHamming;
            property.objectType = NGT::Property::ObjectType::Uint8;
            property.edgeSizeForCreation = 40;
            property.outgoingEdge = 8;
            property.incomingEdge = 20;
        }
        property.indexType = NGT::Property::IndexType::GraphAndTree;
        property.graphType = NGT::Property::GraphType::GraphTypeANNG;
        main::NGTMatcher *matcher = new main::NGTMatcher(property);
        trainMatcher(trainSamples, matcher, bResult);

        matcher->searchEpsilon = 0.1;
        testMatcher(testSamples, searchSize, matcher, bResult, k, gold, dists);
        cout << bResult << endl;
        bResult.ResetTestResults();

        matcher->clear();
        delete matcher;
    }
#endif
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /// MRPT /////////////////////////////////////////////////////////////////////////////////
#ifdef MRPT
    {
        BenchResults bResult("MRPT", searchSize, sampleSize, 2);
        main::MRPTMatcher *matcher = new main::MRPTMatcher(0.6f, 128);
        trainMatcher(trainSamples, matcher, bResult);

        cout << matcher->autotunedParams.votes << endl;
        testMatcher(testSamples, searchSize, matcher, bResult, 2, gold, dists);
        cout << bResult;
        bResult.ResetTestResults();

        matcher->autotunedParams.votes = 8;
        testMatcher(testSamples, searchSize, matcher, bResult, 2, gold, dists);
        cout << bResult;
        bResult.ResetTestResults();

        matcher->clear();
        delete matcher;
    }
#endif
    /////////////////////////////////////////////////////////////////////////////////////////////////

    /// KGraph /////////////////////////////////////////////////////////////////////////////////
#ifdef KGRAPH
    {
        std::vector<cv::Mat> train64 = trainSamples;
        std::vector<cv::Mat> test64 = testSamples;

//        if (is_same<float, dType>::value) {
//            train64 = trainSamples;
//            test64 = testSamples;
//        } else {
//            toMatVectorULL(samplesAmount, sampleSize, train, train64);
//
//            for (int q = 0; q < searchSize; q++) {
//                auto &s = testSamples[q];
//                cv::Mat sample = cv::Mat(cv::Size(BYTE_COUNT / 8, 200), CV_64F);
//                sample.data = s.data;
//                test64.emplace_back(sample);
//            }
//        }

        BenchResults bResult("KGraph", searchSize, sampleSize, k);
        kgraph::KGraph::IndexParams indexParams;
        indexParams.K = 1;
        indexParams.L = 10;
        indexParams.recall = 0.5;
        indexParams.iterations = 32;
        auto *matcher = new main::KGraphMatcher<unsigned char, kgraph::metric::hamming>(BYTE_COUNT, indexParams);
//        auto *matcher = new main::KGraphMatcher<unsigned long long, kgraph::metric::hamming>(BYTE_COUNT / 8, indexParams);
//        auto *matcher = new main::KGraphMatcher<float, kgraph::metric::l2>(128, indexParams);
        trainMatcher(train64, matcher, bResult);

        matcher->SearchParams.P = 32;
        testMatcher(test64, searchSize, matcher, bResult, k, gold, dists);
        cout << bResult;
        bResult.ResetTestResults();

        matcher->clear();
        delete matcher;
    }
#endif
    /////////////////////////////////////////////////////////////////////////////////////////////////

    /// ANNOY /////////////////////////////////////////////////////////////////////////////////
#ifdef ANNOY
    {
        BenchResults bResult("ANNOY", searchSize, sampleSize, k);

        if (is_same<float, dType>::value) {
            main::AnnoyMatcher<float, annoy::Euclidean> *matcher = new main::AnnoyMatcher<float, annoy::Euclidean>(128, 10);
            trainMatcher(trainSamples, matcher, bResult);

            matcher->SearchPrecision = 128;
            testMatcher(testSamples, searchSize, matcher, bResult, k, gold, dists);
            cout << bResult;
            bResult.ResetTestResults();

            matcher->clear();
            delete matcher;
        } else {
            main::AnnoyMatcher<int, annoy::Hamming> *matcher = new main::AnnoyMatcher<int, annoy::Hamming>(128, 48);
            trainMatcher(trainSamples, matcher, bResult);


            matcher->SearchPrecision = 256;
            testMatcher(testSamples, searchSize, matcher, bResult, k, gold, dists);
            cout << bResult;
            bResult.ResetTestResults();

            matcher->clear();
            delete matcher;
        }
    }
#endif
    /////////////////////////////////////////////////////////////////////////////////////////////////
    cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl << endl;
    return 0;
}

void toMatVector(int samplesAmount, int sampleSize, vector<vector<dType>> &train, vector<cv::Mat> &samples) {
    int cvType = is_same<float, dType>::value ? CV_32F : is_same<unsigned long long, dType>::value ? CV_64F : CV_8U;
    int dimensions = train[0].size();
    for (int sampleIdx = 0; sampleIdx < samplesAmount; sampleIdx++) {
        cv::Mat sample;

        sample.create(sampleSize, dimensions, cvType);
        for (int elementIdx = 0; elementIdx < sampleSize; elementIdx++) {
            vector<dType> &t = train[sampleIdx * sampleSize + elementIdx];
            memcpy(sample.ptr(elementIdx), (void *) t.data(), (size_t) t.size() * sizeof(dType));
        }
//        cv::Mat uSample = sample.getUMat(ACCESS_READ, USAGE_ALLOCATE_DEVICE_MEMORY);
        samples.emplace_back(sample);
    }
//    train.clear();
//    train.shrink_to_fit();
}

void toMatVectorULL(int samplesAmount, int sampleSize, vector<vector<unsigned char>> &train, vector<cv::Mat> &samples) {
    int cvType = CV_64F;
    int dimensions = train[0].size();
    for (int sampleIdx = 0; sampleIdx < samplesAmount; sampleIdx++) {
        cv::Mat sample;

        sample.create(sampleSize, 16, cvType);
        for (int elementIdx = 0; elementIdx < sampleSize; elementIdx++) {
            vector<unsigned char> &t = train[sampleIdx * sampleSize + elementIdx];
            memcpy(sample.ptr(elementIdx), (void *) t.data(), (size_t) t.size() * sizeof(unsigned char));
        }
//        cv::Mat uSample = sample.getUMat(ACCESS_READ, USAGE_ALLOCATE_DEVICE_MEMORY);
        samples.emplace_back(sample);
    }
}


void computeGolds(vector<cv::Mat> &trainSamples, vector<cv::Mat> &testSamples, vector<vector<int>> &golds, vector<vector<float>> &dists, int k) {
    cv::Ptr<cv::DescriptorMatcher> bf = cv::DescriptorMatcher::create(
            is_same<float, dType>::value ? cv::DescriptorMatcher::BRUTEFORCE : cv::DescriptorMatcher::BRUTEFORCE_HAMMING);
    for (auto &sample : trainSamples) {
        cv::UMat uSample;
        sample.copyTo(uSample);
        bf->add(uSample);
    }

    float avgDists[k];
    for (int nn = 0; nn < k; nn++) {
        avgDists[nn] = 0;
    }

    chrono::microseconds time = std::chrono::microseconds::zero();
    for (auto &testSample : testSamples) {
        cv::UMat sample;
        testSample.copyTo(sample);
        vector<vector<cv::DMatch>> matches;
        auto t2 = std::chrono::high_resolution_clock::now();
        bf->knnMatch(sample, matches, k);
        time += chrono::duration_cast<chrono::microseconds>(std::chrono::high_resolution_clock::now() - t2);

        for (int j = 0; j < sample.rows; j++) {
            auto &match = matches[j];
            auto &gold = golds.emplace_back();
            auto &dist = dists.emplace_back();

            for (int nn = 0; nn < k; nn++) {
                cv::DMatch &m = match[nn];
                gold.emplace_back(m.trainIdx + m.imgIdx * sample.rows);
                dist.emplace_back(m.distance);
                avgDists[nn] += m.distance;
            }
        }
    }
    cout << fixed << std::setprecision(3);
    cout << "Golds computed in: " << chrono::duration_cast<chrono::seconds>(time).count() << "s" << endl;
//    os << "  Avg Time per Query: " << chrono::duration_cast<chrono::milliseconds>(dt.knnTime).count() / (float) (dt.searchSize * dt.sampleSize) << "ms" << endl;
    cout << "\tAvg query time: " << chrono::duration_cast<chrono::milliseconds>(time).count() / (float) (testSamples.size() * 200) << "ms" << endl;
    cout << "\tAvg distances:\t";
    for (int nn = 0; nn < k; nn++) {
        cout << avgDists[nn] / (float) (testSamples.size() * 200) << "\t";
    }
}

void showOclSupport() {
    if (!cv::ocl::haveOpenCL()) {
        cout << "OpenCL is not available..." << endl;
        return;
    }

    cv::ocl::Context context;
    if (!context.create(cv::ocl::Device::TYPE_GPU)) {
        cout << "Failed creating the context..." << endl;
        return;
    }

    cout << "SVM:               " << context.useSVM() << endl;
    cout << context.ndevices() << " GPU devices are detected." << endl; //This bit provides an overview of the OpenCL devices you have in your computer
    for (ulong i = 0; i < context.ndevices(); i++) {
        const cv::ocl::Device &device = context.device(i);
        cout << "name:              " << device.name() << endl;
        cout << "available:         " << device.available() << endl;
        cout << "imageSupport:      " << device.imageSupport() << endl;
        cout << "OpenCL_C_Version:  " << device.OpenCL_C_Version() << endl;
        cout << endl;
    }
}


template<typename T>
vector<vector<T>> read_record(string fileName, unsigned long maxRows, unsigned long offset) {

    // File pointer
    fstream fin;

    // Open an existing file
    fin.open(fileName, ios::in);

    // Read the Data from the file
    // as String Vector
    string line, word;

    vector<vector<T>> result;
    unsigned long rowCount = 0;
    unsigned long lastRowLength = 0;
    unsigned long offCount = 0;
    while (getline(fin, line) && rowCount < maxRows) {
        offCount++;
        if (offCount < offset) continue;
        rowCount++;

        vector<T> &row = result.emplace_back();
        row.reserve(lastRowLength);
        lastRowLength = 0;

        // used for breaking words
        stringstream s(line);

        while (!s.eof()) {
            T temp;
            s >> temp;
            row.emplace_back(temp);
            s.ignore();
            lastRowLength++;
        }
    }

    fin.close();

    return result;
}

template<>
vector<vector<unsigned char>> read_record(string fileName, unsigned long maxRows, unsigned long offset) {

    // File pointer
    fstream fin;

    // Open an existing file
    fin.open(fileName, ios::in);

    // Read the Data from the file
    // as String Vector
    string line, word;
    unsigned short elemCount;
    vector<vector<unsigned char>> result;
    unsigned long rowCount = 0;
    unsigned long lastRowLength = 0;
    unsigned long offCount = 0;
    while (getline(fin, line) && rowCount < maxRows) {
        offCount++;
        if (offCount < offset) continue;
        rowCount++;

        vector<unsigned char> &row = result.emplace_back();
        row.reserve(lastRowLength);
        lastRowLength = 0;

        // used for breaking words
        stringstream s(line);

        elemCount = 0;
        while (!s.eof() && elemCount < BYTE_COUNT) {
            elemCount++;
            float temp;
            s >> temp;
            unsigned char val = static_cast<unsigned char>(temp);
            row.emplace_back(val);
            s.ignore();
            lastRowLength++;
        }
    }

    fin.close();

    return result;
}