/*
 * ORBMatching.h
 *
 *  Created on: Nov 18, 2014
 *      Author: tigershark
 */

#ifndef ORBMATCHING_H_
#define ORBMATCHING_H_

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <vector>

#include "../../engar/headers/KeypointMatches.h"

class ORBMatching {
public:
	ORBMatching(Mat&);
	Ptr<KeypointMatches> FindMatches(Mat&);
	virtual ~ORBMatching();

private:

	Ptr<vector<KeyPoint> > keypoints_object;
	Mat descriptors_object;

	Ptr<Feature2D> detector;
	//Ptr<FREAK> extractor;
	Ptr<DescriptorMatcher> matcher;
	Ptr<FlannBasedMatcher> matcher2;

	double avgMatchTime;
	double maxMatchTime;
	int matchCount;
};

#endif /* ORBMATCHING_H_ */
