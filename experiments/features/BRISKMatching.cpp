/*
 * BRISKMatching.cpp
 *
 *  Created on: Nov 18, 2014
 *      Author: tigershark
 */

#include <iostream>
#include <algorithm>
#include <ctime>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include "../../engar/headers/HomographyHelper.h"
#include "BRISKMatching.h"
#include "engar/Utils.hpp"

using namespace cv;
using namespace std;

BRISKMatching::BRISKMatching() {
	clock_t begin, end;

	Mat img_object = imread("img/obj.png", CV_LOAD_IMAGE_GRAYSCALE);
	Mat img_scene = imread("img/scenepr45.png", CV_LOAD_IMAGE_GRAYSCALE);

	if (!img_object.data || !img_scene.data) {
		std::cout << " --(!) Error reading images " << std::endl;
		return;
	}

	//-- Step 1: Detect the keypoints using SURF Detector

	/*
	 * BRISK(int thresh=30, int octaves=3, float patternScale=1.0f)
	 Parameters:

	 thresh – FAST/AGAST detection threshold score.
	 octaves – detection octaves. Use 0 to do single scale.
	 patternScale – apply this scale to the pattern used for sampling the neighbourhood of a keypoint.
	 *
	 */
	Ptr<BRISK> detector = BRISK::create(70, 3, 1.0f);

	Ptr<vector<KeyPoint> > keypoints_object = new vector<KeyPoint>;
	Ptr<vector<KeyPoint> > keypoints_scene = new vector<KeyPoint>;
	Mat descriptors_object, descriptors_scene;

	/*
	 operator()(InputArray image, InputArray mask, vector<KeyPoint>& keypoints, OutputArray descriptors, bool useProvidedKeypoints=false ) const
	 Parameters:

	 image – The input 8-bit grayscale image.
	 mask – The operation mask.
	 keypoints – The output vector of keypoints.
	 descriptors – The output descriptors. Pass cv::noArray() if you do not need it.
	 useProvidedKeypoints – If it is true, then the method will use the provided vector of keypoints instead of detecting them.
	 */
	detector->detectAndCompute(img_object, cv::noArray(), *keypoints_object, descriptors_object, false);

	//-- Step 2: Calculate descriptors (feature vectors)
	cout << "BRISK Features: " << endl;
	begin = clock();
	detector->detectAndCompute(img_scene, cv::noArray(), *keypoints_scene, descriptors_scene, false);
	end = clock();
	cout << "\tDetect and Compute time: " << double(end - begin) / CLOCKS_PER_SEC * 1000 << endl;
	cout << "\tAmount: " << keypoints_scene->size() << endl;

	//-- Step 3: Matching descriptor vectors using FLANN matcher

	/*One of NORM_L1, NORM_L2, NORM_HAMMING, NORM_HAMMING2.
	 * L1 and L2 norms are preferable choices for SIFT and SURF descriptors,
	 * NORM_HAMMING should be used with ORB, BRISK and BRIEF,
	 * NORM_HAMMING2 should be used with ORB when WTA_K==3 or 4 (see ORB::ORB constructor description).
	 * */
	Ptr<DescriptorMatcher> matcher = new BFMatcher(NORM_HAMMING, false);
	//std::vector<DMatch> matches;

	vector<vector<DMatch> > matches;

	begin = clock();
	matcher->knnMatch(descriptors_object, descriptors_scene, matches, 2);
	end = clock();
	cout << "\tMatch time: " << double(end - begin) / CLOCKS_PER_SEC * 1000 << endl;

	double max_dist = 0;
	double min_dist = 100;

	//-- Quick calculation of max and min distances between keypoints
	Utils::calculateMaxAndMinDistance(matches, min_dist, max_dist);

	//-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
	Ptr<vector<DMatch> > good_matches = new vector<DMatch>;

	// Apply ratio test
	const float ratio = 0.8f; // As in Lowe's paper
	for (uint i = 0; i < matches.size(); ++i) {
		if (matches[i][0].distance < ratio * matches[i][1].distance) {
			good_matches->push_back(matches[i][0]);
		}
	}
	cout << "\tGood matches: " << good_matches->size() << endl;

	Ptr<KeypointMatches> keysAndMatch = new KeypointMatches();
	keysAndMatch->SceneKeypoints = keypoints_scene;
	keysAndMatch->ObjectKeypoints = keypoints_object;
	keysAndMatch->GoodMatches = good_matches;

	HomographyHelper homography;

	Mat image;
	std::vector<Point2f> oldCorners(4);
	vector<KeyPoint> inliers;
	homography.find(img_object, img_scene, keysAndMatch, image, oldCorners, inliers);
	imshow("LK Demo", image);
	waitKey(0);
}

BRISKMatching::~BRISKMatching() {
	// TODO Auto-generated destructor stub
}

