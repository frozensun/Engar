/*
 * ORBMatching.cpp
 *
 *  Created on: Nov 18, 2014
 *      Author: tigershark
 */

#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <ctime>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include "../../engar/headers/HomographyHelper.h"
#include "engar/Utils.hpp"
#include "../../engar/headers/KeypointMatches.h"
#include "ORBMatching.h"

using namespace cv;
using namespace std;

/*!
 *
 nfeatures 窶� The maximum number of features to retain.
 scaleFactor 窶� Pyramid decimation ratio, greater than 1. scaleFactor==2 means the classical pyramid, where each next level has 4x less pixels than the previous, but such a big scale factor will degrade feature matching scores dramatically. On the other hand, too close to 1 scale factor will mean that to cover certain scale range you will need more pyramid levels and so the speed will suffer.
 nlevels 窶� The number of pyramid levels. The smallest level will have linear size equal to input_image_linear_size/pow(scaleFactor, nlevels).
 edgeThreshold 窶� This is size of the border where the features are not detected. It should roughly match the patchSize parameter.
 firstLevel 窶� It should be 0 in the current implementation.
 WTA_K 窶� The number of points that produce each element of the oriented BRIEF descriptor. The default value 2 means the BRIEF where we take a random point pair and compare their brightnesses, so we get 0/1 response. Other possible values are 3 and 4. For example, 3 means that we take 3 random points (of course, those point coordinates are random, but they are generated from the pre-defined seed, so each element of BRIEF descriptor is computed deterministically from the pixel rectangle), find point of maximum brightness and output index of the winner (0, 1 or 2). Such output will occupy 2 bits, and therefore it will need a special variant of Hamming distance, denoted as NORM_HAMMING2 (2 bits per bin). When WTA_K=4, we take 4 random points to compute each bin (that will also occupy 2 bits with possible values 0, 1, 2 or 3).
 scoreType 窶� The default HARRIS_SCORE means that Harris algorithm is used to rank features (the score is written to KeyPoint::score and is used to retain best nfeatures features); FAST_SCORE is alternative value of the parameter that produces slightly less stable keypoints, but it is a little faster to compute.
 patchSize 窶� size of the patch used by the oriented BRIEF descriptor. Of course, on smaller pyramid layers the perceived image area covered by a feature will be larger.
 *
 */
ORBMatching::ORBMatching(Mat& img_object) {

	if (!img_object.data) {
		std::cout << " --(!) Error reading images " << std::endl;
		return;
	}

	int nfeatures = 700;
	float scaleFactor = 1.2f;
	int nlevels = 8;
	int edgeThreshold = 16;
	int firstLevel = 0;
	int WTA_K = 2;
	int scoreType = ORB::HARRIS_SCORE;
	int patchSize = 16;

	matchCount = 0;
	avgMatchTime = 0;
	maxMatchTime = 0;

	// ORB(int nfeatures=500, float scaleFactor=1.2f, int nlevels=8, int edgeThreshold=31, int firstLevel=0, int WTA_K=2, int scoreType=ORB::HARRIS_SCORE, int patchSize=31)
	this->detector = ORB::create(nfeatures, scaleFactor, nlevels, edgeThreshold,
			firstLevel, WTA_K, scoreType, patchSize);

	// (roation invariance, scale invariance, pattern radius corresponding to SMALLEST_KP_SIZE,
	// number of octaves, optional vector containing the selected pairs)
	// FREAK extractor(true, true, 22, 4, std::vector<int>());
//	this->extractor = new FREAK(true, true, 22, 6);

	/*One of NORM_L1, NORM_L2, NORM_HAMMING, NORM_HAMMING2.
	 * L1 and L2 norms are preferable choices for SIFT and SURF descriptors,
	 * NORM_HAMMING should be used with ORB, BRISK and BRIEF,
	 * NORM_HAMMING2 should be used with ORB when WTA_K==3 or 4 (see ORB::ORB constructor description).
	 * */
	this->matcher = new BFMatcher(NORM_HAMMING, false);

	/*
	 * cv::flann::Index GenFLANNIndex(cv::Mat keys)
	 {
	 switch (keys.type())
	 {
	 case CV_32F:
	 {
	 return  cv::flann::Index(keys,cv::flann::KDTreeIndexParams(4));
	 break;
	 }
	 case CV_8U:
	 {
	 return cv::flann::Index(keys,cv::flann::HierarchicalClusteringIndexParams(),dist_type);
	 break;
	 }
	 default:
	 {
	 return cv::flann::Index(keys,cv::flann::KDTreeIndexParams(4));
	 break;
	 }
	 }

	 }
	 ...
	 cv::flann::Index tree = GenFLANNIndex(descriptors);
	 */
	//this->matcher2 = new FlannBasedMatcher(new flann::LshIndexParams(6, 12, 1));
	//this->matcher2 = new FlannBasedMatcher(new flann::HierarchicalClusteringIndexParams());
	this->keypoints_object = new vector<KeyPoint>;

	detector->detectAndCompute(img_object, noArray(), *keypoints_object,
			descriptors_object);

//	flann::Index idx = flann::Index(descriptors_object, cv::flann::HierarchicalClusteringIndexParams(), cvflann::FLANN_DIST_HAMMING);

//	vector<Mat> train;
//	train.push_back(descriptors_object);
//	matcher2->add(train);
//	matcher2->train();

//	int test_rank = 10;
//	flann::Matrix<float> data((float*) all_data.data, all_data.rows, all_data.cols);
//	flann::Index<cvflann::ChiSquareDistance<float> > index(data, cvflann::LinearIndexParams());
//	index.buildIndex();
//
//	// make cv::Mat-headers for easier access
//	// (you can do the same with the dists-matrix if you need it)
//	cv::Mat1i ind(data.rows, test_rank);
//	CV_Assert(ind.isContinuous());
//	cvflann::Matrix<int> indices((int*) ind.data, ind.rows, ind.cols);
//	cvflann::Matrix<float> dists(new float[data.rows * test_rank], data.rows, test_rank);
//	index.knnSearch(data, indices, dists, test_rank, cvflann::SearchParams(all_data.cols - 1));
}

Ptr<KeypointMatches> ORBMatching::FindMatches(Mat& img_scene) {

	clock_t begin, end;
	Ptr<KeypointMatches> keysAndMatch = new KeypointMatches();

	if (!img_scene.data) {
		std::cout << " --(!) Error reading images " << std::endl;
		return keysAndMatch;
	}

	//-- Detect the keypoints using ORB Detector and Calculate descriptors (feature vectors)
	Ptr<vector<KeyPoint> > keypoints_scene = new vector<KeyPoint>;

	Mat descriptors_scene;

//	cout << "ORB Features: " << endl;
	begin = clock();
	detector->detect(img_scene, *keypoints_scene);
	end = clock();
//	cout << "\tDetect time: " << double(end - begin) / CLOCKS_PER_SEC * 1000 << endl;
//	cout << "\tAmount: " << keypoints_scene->size() << endl;
	begin = clock();
	detector->compute(img_scene, *keypoints_scene, descriptors_scene);
	end = clock();
//	cout << "\tExtract time: " << double(end - begin) / CLOCKS_PER_SEC * 1000 << endl;

	//-- Matching descriptor vectors using FLANN matcher
	vector<vector<DMatch> > matches;

	begin = clock();
	matcher->knnMatch(descriptors_object, descriptors_scene, matches, 2);
//	matcher2->knnMatch(descriptors_scene, matches, 2);
	end = clock();
	double matchTime = double(end - begin) / CLOCKS_PER_SEC * 1000;
	avgMatchTime += matchTime;
	if (matchTime > maxMatchTime) {
		maxMatchTime = matchTime;
	}
	matchCount++;
//	cout << "\tMatch time: " << matchTime << endl;

	double max_dist = 0;
	double min_dist = 100;

	//-- Quick calculation of max and min distances between keypoints
	//Utils::calculateMaxAndMinDistance(matches, min_dist, max_dist);

	//-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
	Ptr<vector<DMatch> > good_matches = new vector<DMatch>;

	// Apply ratio test
	const float ratio = 0.8f;	// As in Lowe's paper
	for (uint i = 0; i < matches.size(); ++i) {
		if (!matches[i].empty() && matches[i][0].distance < 150
				&& matches[i][0].distance < ratio * matches[i][1].distance) {
//			int a = matches[i][0].trainIdx;
//			matches[i][0].trainIdx = matches[i][0].queryIdx;
//			matches[i][0].queryIdx = a;
			good_matches->push_back(matches[i][0]);
		}
	}
//	cout << "\tGood matches: " << good_matches->size() << endl;


	keysAndMatch->SceneKeypoints = keypoints_scene;
	keysAndMatch->ObjectKeypoints = keypoints_object;
	keysAndMatch->GoodMatches = good_matches;

	return keysAndMatch;
}

ORBMatching::~ORBMatching() {
	cout << "\tAvg Match time: " << avgMatchTime / matchCount << endl;
	cout << "\tMax Match time: " << maxMatchTime << endl;
}

