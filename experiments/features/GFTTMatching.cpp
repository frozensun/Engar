/*
 * GFTTMatching.cpp
 *
 *  Created on: Nov 19, 2014
 *      Author: tigershark
 */

#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <ctime>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "../../engar/headers/HomographyHelper.h"
#include "engar/Utils.hpp"
#include "GFTTMatching.h"

using namespace cv;
using namespace std;

GFTTMatching::GFTTMatching() {
	clock_t begin, end;

	Mat img_object = imread("img/obj.png", CV_LOAD_IMAGE_GRAYSCALE);
	Mat img_scene = imread("img/scenepr45.png", CV_LOAD_IMAGE_GRAYSCALE);

	if (!img_object.data || !img_scene.data) {
		std::cout << " --(!) Error reading images " << std::endl;
		return;
	}

	//-- Step 1: Detect the keypoints using SURF Detector

	/*
	 * int maxCorners=1000, double qualityLevel=0.01, double minDistance=1,	 int blockSize=3, bool useHarrisDetector=false, double k=0.04
	 Parameters:

	 image – Input 8-bit or floating-point 32-bit, single-channel image.
	 eig_image – The parameter is ignored.
	 temp_image – The parameter is ignored.
	 corners – Output vector of detected corners.
	 maxCorners – Maximum number of corners to return. If there are more corners than are found, the strongest of them is returned.
	 qualityLevel – Parameter characterizing the minimal accepted quality of image corners. The parameter value is multiplied by the best corner quality measure, which is the minimal eigenvalue (see cornerMinEigenVal() ) or the Harris function response (see cornerHarris() ). The corners with the quality measure less than the product are rejected. For example, if the best corner has the quality measure = 1500, and the qualityLevel=0.01 , then all the corners with the quality measure less than 15 are rejected.
	 minDistance – Minimum possible Euclidean distance between the returned corners.
	 mask – Optional region of interest. If the image is not empty (it needs to have the type CV_8UC1 and the same size as image ), it specifies the region in which the corners are detected.
	 blockSize – Size of an average block for computing a derivative covariation matrix over each pixel neighborhood. See cornerEigenValsAndVecs() .
	 useHarrisDetector – Parameter indicating whether to use a Harris detector (see cornerHarris()) or cornerMinEigenVal().
	 k – Free parameter of the Harris detector.

	 *
	 */
	int maxCorners = 1000;
	double qualityLevel = 0.01;
	double minDistance = 1;
	int blockSize = 3;
	bool useHarrisDetector = false;
	double k = 0.04;
	Mat mask1 = Mat::ones(img_object.rows, img_object.cols, CV_8UC1);
	Mat mask2 = Mat::ones(img_scene.rows, img_scene.cols, CV_8UC1);

	Ptr<vector<KeyPoint> > keypoints_object = new vector<KeyPoint>;
	Ptr<vector<KeyPoint> > keypoints_scene = new vector<KeyPoint>;
	Mat descriptors_object, descriptors_scene;

	goodFeaturesToTrack(img_object, *keypoints_object, maxCorners, qualityLevel, minDistance, mask1, blockSize, useHarrisDetector, k);

	//-- Step 2: Calculate descriptors (feature vectors)
	cout << "GFTT Features: " << endl;
	begin = clock();
	goodFeaturesToTrack(img_scene, *keypoints_object, maxCorners, qualityLevel, minDistance, mask2, blockSize, useHarrisDetector, k);
	end = clock();
	cout << "\tDetect and Compute time: " << double(end - begin) / CLOCKS_PER_SEC * 1000 << endl;
	cout << "\tAmount: " << keypoints_scene->size() << endl;

	//-- Step 3: Matching descriptor vectors using FLANN matcher

	/*One of NORM_L1, NORM_L2, NORM_HAMMING, NORM_HAMMING2.
	 * L1 and L2 norms are preferable choices for SIFT and SURF descriptors,
	 * NORM_HAMMING should be used with ORB, BRISK and BRIEF,
	 * NORM_HAMMING2 should be used with ORB when WTA_K==3 or 4 (see ORB::ORB constructor description).
	 * */
	Ptr<DescriptorMatcher> matcher = new BFMatcher(NORM_HAMMING, false);
	//std::vector<DMatch> matches;

	vector<vector<DMatch> > matches;

	begin = clock();
	matcher->knnMatch(descriptors_object, descriptors_scene, matches, 2);
	end = clock();
	cout << "\tMatch time: " << double(end - begin) / CLOCKS_PER_SEC * 1000 << endl;

	double max_dist = 0;
	double min_dist = 100;

	//-- Quick calculation of max and min distances between keypoints
	Utils::calculateMaxAndMinDistance(matches, min_dist, max_dist);

	//-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
	Ptr<vector<DMatch> > good_matches = new vector<DMatch>;

	// Apply ratio test
	const float ratio = 0.8f; // As in Lowe's paper
	for (uint i = 0; i < matches.size(); ++i) {
		if (matches[i][0].distance < ratio * matches[i][1].distance) {
			good_matches->push_back(matches[i][0]);
		}
	}
	cout << "\tGood matches: " << good_matches->size() << endl;

	Ptr<KeypointMatches> keysAndMatch = new KeypointMatches();
	keysAndMatch->SceneKeypoints = keypoints_scene;
	keysAndMatch->ObjectKeypoints = keypoints_object;
	keysAndMatch->GoodMatches = good_matches;

	HomographyHelper homography;

	Mat image;
	std::vector<Point2f> oldCorners(4);
	vector<KeyPoint> inliers;
	homography.find(img_object, img_scene, keysAndMatch, image, oldCorners, inliers);
	imshow("LK Demo", image);
	waitKey(0);
}

GFTTMatching::~GFTTMatching() {
	// TODO Auto-generated destructor stub
}

