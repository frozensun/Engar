/*
 * FASTMatching.cpp
 *
 *  Created on: Nov 18, 2014
 *      Author: tigershark
 */

#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <ctime>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include "../../engar/headers/HomographyHelper.h"
#include "engar/Utils.hpp"
#include "FASTMatching.h"

using namespace cv;
using namespace std;

FASTMatching::FASTMatching() {
	clock_t begin, end;

	Mat img_object = imread("img/obj.png", CV_LOAD_IMAGE_GRAYSCALE);
	Mat img_scene = imread("img/scenepr45.png", CV_LOAD_IMAGE_GRAYSCALE);

	if (!img_object.data || !img_scene.data) {
		std::cout << " --(!) Error reading images " << std::endl;
		return;
	}

	//-- Step 1: Detect the keypoints using SURF Detector

	Ptr<FeatureDetector> detector = FastFeatureDetector::create(60, false);
	//Ptr<FeatureDetector> detector = new SurfFeatureDetector(200, 4, 2, true, false);

	// (roation invariance, scale invariance, pattern radius corresponding to SMALLEST_KP_SIZE,
	// number of octaves, optional vector containing the selected pairs)
	// FREAK extractor(true, true, 22, 4, std::vector<int>());
	//FREAK extractor(true, true, 22, 4);

	Ptr<vector<KeyPoint> > keypoints_object = new vector<KeyPoint>;
	Ptr<vector<KeyPoint> > keypoints_scene = new vector<KeyPoint>;

	detector->detect(img_object, *keypoints_object);

	//-- Step 2: Calculate descriptors (feature vectors)

	Mat descriptors_object, descriptors_scene;

	detector->compute(img_object, *keypoints_object, descriptors_object);

	//detector = new FastFeatureDetector(100, false);

	cout << "FAST Features: " << endl;
	begin = clock();
	detector->detect(img_scene, *keypoints_scene);
	end = clock();
	cout << "\tDetect time: " << double(end - begin) / CLOCKS_PER_SEC * 1000 << endl;
	cout << "\tAmount: " << keypoints_scene->size() << endl;
	begin = clock();
	detector->compute(img_scene, *keypoints_scene, descriptors_scene);
	end = clock();
	cout << "\tCompute time: " << double(end - begin) / CLOCKS_PER_SEC * 1000 << endl;

	//-- Step 3: Matching descriptor vectors using FLANN matcher

	/*One of NORM_L1, NORM_L2, NORM_HAMMING, NORM_HAMMING2.
	 * L1 and L2 norms are preferable choices for SIFT and SURF descriptors,
	 * NORM_HAMMING should be used with ORB, BRISK and BRIEF,
	 * NORM_HAMMING2 should be used with ORB when WTA_K==3 or 4 (see ORB::ORB constructor description).
	 * */
	Ptr<DescriptorMatcher> matcher = new BFMatcher(NORM_HAMMING, false);
	//std::vector<DMatch> matches;

	vector<vector<DMatch> > matches;

	begin = clock();
	matcher->knnMatch(descriptors_object, descriptors_scene, matches, 2);
	end = clock();
	cout << "\tMatch time: " << double(end - begin) / CLOCKS_PER_SEC * 1000 << endl;

	double max_dist = 0;
	double min_dist = 100;

	//-- Quick calculation of max and min distances between keypoints
	//Utils::calculateMaxAndMinDistance(matches, min_dist, max_dist);
	for (uint i = 0; i < matches.size(); i++) {
		double dist = matches[i][0].distance;
		if (dist < min_dist)
			min_dist = dist;

		if (dist > max_dist)
			max_dist = dist;
	}
	printf("\t-- Max dist : %f \n", max_dist);
	printf("\t-- Min dist : %f \n", min_dist);

	//-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
	Ptr<vector<DMatch> > good_matches = new vector<DMatch>;
	// Apply ratio test
	const float ratio = 0.8f; // As in Lowe's paper
	for (uint i = 0; i < matches.size(); ++i) {
		if (matches[i][0].distance < ratio * matches[i][1].distance) {
			good_matches->push_back(matches[i][0]);
		}
	}
	cout << "\tGood matches: " << good_matches->size() << endl;

	Ptr<vector<DMatch> > nearMatches = new vector<DMatch>;
	float distanceThreshold = 2.0f;
	for (uint i = 0; i < good_matches->size(); ++i) {
		if ((*good_matches)[i].distance < distanceThreshold * max(min_dist, 20.0)) {
			nearMatches->push_back((*good_matches)[i]);
		}
	}
	cout << "\tGood and Near matches: " << nearMatches->size() << endl;

	Ptr<KeypointMatches> keysAndMatch = new KeypointMatches();
	keysAndMatch->SceneKeypoints = keypoints_scene;
	keysAndMatch->ObjectKeypoints = keypoints_object;
	keysAndMatch->GoodMatches = nearMatches;

	HomographyHelper homography;

	Mat image;
	std::vector<Point2f> oldCorners(4);
	vector<KeyPoint> inliers;
	homography.find(img_object, img_scene, keysAndMatch, image, oldCorners, inliers);
	imshow("LK Demo", image);
	waitKey(0);
}

FASTMatching::~FASTMatching() {
	// TODO Auto-generated destructor stub
}

