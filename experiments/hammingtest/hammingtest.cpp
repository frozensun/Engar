//
// Created by tigershark on 11/10/19.
//
#include <iostream>
#include <fstream>

#include <kgraph.h>
#include <opencv2/opencv.hpp>
#include <kgraph-data.h> // must follow opencv
#include <opencv2/xfeatures2d/nonfree.hpp>
#include <opencv2/imgcodecs.hpp>
#include <engar/matchers/KGraphMatcher.h>

using namespace std;
using namespace cv;

template<typename T>
vector<vector<T>> read_record(string fileName, unsigned long maxRows = 1000000) {

    // File pointer
    fstream fin;

    // Open an existing file
    fin.open(fileName, ios::in);

    // Read the Data from the file
    // as String Vector
    string line, word;

    vector<vector<T>> result;
    unsigned long rowCount = 0;
    unsigned long lastRowLength = 0;
    while (getline(fin, line) && rowCount < maxRows) {
        rowCount++;

        vector<T> &row = result.emplace_back();
        row.reserve(lastRowLength);
        lastRowLength = 0;

        // used for breaking words
        stringstream s(line);

        while (!s.eof()) {
            T temp;
            s >> temp;
            row.emplace_back(temp);
            s.ignore();
            lastRowLength++;
        }
    }

    fin.close();

    return result;
}

template<>
vector<vector<unsigned char>> read_record(string fileName, unsigned long maxRows) {

    // File pointer
    fstream fin;

    // Open an existing file
    fin.open(fileName, ios::in);

    // Read the Data from the file
    // as String Vector
    string line, word;

    vector<vector<unsigned char>> result;
    unsigned long rowCount = 0;
    unsigned long lastRowLength = 0;
    while (getline(fin, line) && rowCount < maxRows) {
        rowCount++;

        vector<unsigned char> &row = result.emplace_back();
        row.reserve(lastRowLength);
        lastRowLength = 0;

        // used for breaking words
        stringstream s(line);

        while (!s.eof()) {
            float temp;
            s >> temp;
            unsigned char val = static_cast<unsigned char>(temp);
            row.emplace_back(val);
            s.ignore();
            lastRowLength++;
        }
    }

    fin.close();

    return result;
}

void toMatVectorF(int samplesAmount, int sampleSize, vector<vector<float>> &train, vector<Mat> &samples);

void toMatVectorH(int samplesAmount, int sampleSize, vector<vector<unsigned char>> &train, vector<Mat> &samples);

void showOclSupport();

chrono::nanoseconds runMatcher(vector<UMat> &trainSamples, vector<UMat> &testSamples, int k, int searchSize, DescriptorMatcher *matcher);

int main(int argc, char **argv) {

    unsigned long long a[16];
    unsigned long long b[16];

    for (int i = 0; i < 16; i++) {
        a[i] = 0;
        b[i] = 0;
    }

    a[10] = 1;
    b[2] = 3;

    cout << kgraph::metric::hamming::apply(a, b, 128) << endl;

    int samplesAmount = 50;
    int sampleSize = 200;
    int searchSize = 50;
    int k = 10;

    /// Load train /////////////////////////////////////////////////////////////////////////////////
    vector<Mat> trainSamples;
    vector<vector<unsigned char>> train = read_record<unsigned char>("../../../../../data/test.csv", samplesAmount * sampleSize);
    toMatVectorH(samplesAmount, sampleSize, train, trainSamples);

    /// Load test /////////////////////////////////////////////////////////////////////////////////
    vector<Mat> testSamples;
    toMatVectorH(samplesAmount, sampleSize, train, testSamples);
    train.clear();
    train.shrink_to_fit();

    Mat trainData;

    for (auto &d : trainSamples) {
        trainData.push_back(d);
    }

    kgraph::MatrixProxy<unsigned char> proxy(trainData);

    for (int i = 0; i < trainSamples.size(); i++) {
        cout << kgraph::metric::hamming::apply(proxy[i], proxy[i], 128) << endl;
    }

    kgraph::KGraph::IndexParams indexParams;
    indexParams.K = 2;
    indexParams.L = 10;
    indexParams.recall = 0.5;
    indexParams.iterations = 32;
    engar::KGraphMatcher<unsigned long long, kgraph::metric::hamming> *matcher = new engar::KGraphMatcher<unsigned long long, kgraph::metric::hamming>(16, indexParams);

    for (const auto &s :  trainSamples) {
        matcher->add(s);
    }
    matcher->train();
}

void toMatVectorF(int samplesAmount, int sampleSize, vector<vector<float>> &train, vector<Mat> &samples) {
    int dimensions = train[0].size();
    for (int sampleIdx = 0; sampleIdx < samplesAmount; sampleIdx++) {
        Mat sample;
        sample.create(sampleSize, dimensions, CV_32FC1);
        for (int elementIdx = 0; elementIdx < sampleSize; elementIdx++) {
            vector<float> &t = train[sampleIdx * sampleSize + elementIdx];
            memcpy(sample.ptr(elementIdx), (void *) t.data(), (size_t) t.size() * sizeof(float));
        }
        samples.emplace_back(sample);
    }
}

void toMatVectorH(int samplesAmount, int sampleSize, vector<vector<unsigned char>> &train, vector<Mat> &samples) {
    int dimensions = train[0].size();
    for (int sampleIdx = 0; sampleIdx < samplesAmount; sampleIdx++) {
        Mat sample;
        sample.create(sampleSize, dimensions, CV_64F);
        for (int elementIdx = 0; elementIdx < sampleSize; elementIdx++) {
            vector<unsigned char> &t = train[sampleIdx * sampleSize + elementIdx];
            memcpy(sample.ptr(elementIdx), (void *) t.data(), (size_t) t.size() * sizeof(unsigned char));
        }
        samples.emplace_back(sample);
    }
}

