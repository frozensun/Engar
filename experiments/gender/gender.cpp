//
// Created by tigershark on 11/10/19.
//
#include <exception>
#include <random>
#include <iostream>
#include <opencv2/imgcodecs.hpp>
#include "opencv4/opencv2/core/mat.hpp"
#include "opencv2/dnn.hpp"
#include <complex>
#include <iomanip>


using std::vector;
using namespace std;
using namespace cv;
using namespace cv::dnn;


int main(int argc, char **argv) {

    const std::string caffeConfigFile = "../../../models/googlenet_aligned_gender.prototxt";
    const std::string caffeWeightFile = "../../../models/googlenet_aligned_gender.caffemodel";

    const string inputName = "data";
    const string outputName = "loss3/loss3";

    const Size insize = Size(224, 224);
    const Scalar meanColor = Scalar(104, 117, 123);

    Net net = cv::dnn::readNetFromCaffe(caffeConfigFile, caffeWeightFile);
    net.setPreferableTarget(DNN_TARGET_OPENCL);


    Mat image = imread("../../../img/faces/nahuel.jpg", IMREAD_COLOR);
    UMat inputBlob = cv::dnn::blobFromImage(image, 1.0, insize, meanColor).getUMat(ACCESS_RW, USAGE_ALLOCATE_SHARED_MEMORY);

    net.setInput(inputBlob, inputName);

    cv::Mat detection = net.forward(outputName);
    cout << fixed << std::setprecision(2);
    cout << "Hombre: " << detection.at<float>(0) * 100 << endl
         << "Mujer: " << detection.at<float>(1) * 100 << endl;
    cout << detection;

    return 0;
}

